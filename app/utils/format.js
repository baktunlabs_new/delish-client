
export const formatPrice = (price) => {
    const formated = price % 1 === 0 ? `${price}.00` : price
    return formated
}
