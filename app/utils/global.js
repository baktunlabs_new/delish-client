import {
  Dimensions,
} from 'react-native'

const { height, width } = Dimensions.get('window')
export const isX = Platform.OS === "ios" && (height > 800 || width > 800) ? true : false
export const APIKEY = 'AIzaSyBkuwuqAQd0na7bYlaPkZ2U095-kfZ5xSg'
export const earthRadiusInKM = 6371;
export const radiusInKM = 0.5;
export const aspectRatio = 0.5;
/*funciones para obtener el radio de la tierra*/
export function deg2rad (angle) {
  return angle * 0.017453292519943295 // (angle / 180) * Math.PI;
}

export function rad2deg (angle) {
  return angle * 57.29577951308232 // angle / Math.PI * 180
}

export function debounced(delay, fn) {
  let timerId;
  return function (...args) {
    if (timerId) {
      clearTimeout(timerId);
    }
    timerId = setTimeout(() => {
      fn(...args);
      timerId = null;
    }, delay);
  }
}

export function throttled(delay, fn) {
  let lastCall = 0;
  return function (...args) {
    const now = (new Date).getTime();
    if (now - lastCall < delay) {
      return;
    }
    lastCall = now;
    return fn(...args);
  }
}