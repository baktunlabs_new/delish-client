import { Platform } from 'react-native'

export const validateRegister = ({
    name,
    lastName,
    phone,
    email,
    password,
    country,
    state
}) => {
    return name.trim() !== ''
    && lastName.trim() !== ''
    && phone.trim() !== ''
    && email.trim() !== ''
    && password.trim() !== ''
    && country
    && state.trim() !== ''
}
export const validateRegisterIOS = ({
    name,
    lastName,
    phone,
    email,
    password,
}) => {
    return name.trim() !== ''
    && lastName.trim() !== ''
    && phone.trim() !== ''
    && email.trim() !== ''
    && password.trim() !== ''
}

export const validateEmail = ({
    email
}) => {
    const reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    return reg.test(String(email.toLowerCase()))
}

export const validatePassword = ({
    password
}) => {
    return password.trim() !== '' && password.length >= 6
}

export const validateInfo = ({
    address,
    city,
    state,
    curp,
    ine,
    ineBack,
    driverLicence
}) => {
    return address.trim() !== '' &&
        city.trim() !== '' &&
        state.trim() !== '' &&
        curp.trim() !== '' &&
        ine &&
        ineBack &&
        driverLicence
}

export const validatePersonalInfo = ({
    name,
    lastName,
    phone
}) => {
    return name.trim() !== '' &&
        lastName.trim() !== '' &&
        phone.trim() !== ''
}
