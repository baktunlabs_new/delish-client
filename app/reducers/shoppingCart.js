const UPDATE_CART_DATA = 'UPDATE_CART_DATA'
const RESET_CART_DATA = 'RESET_CART_DATA'
const UPDATE_STORE = 'UPDATE_STORE'
const RESET_STORE_DATA = 'RESET_STORE_DATA'

const initialState = {
  cart: [],
  store: null
}

export default(state = initialState, action) => {
  switch (action.type) {
    case UPDATE_CART_DATA:
      return {
        ...state,
        cart: action.payload
      }
    case UPDATE_STORE:
      return {
        ...state,
        store: action.payload
      }
    case RESET_CART_DATA:
      return {
        ...state,
        cart:[],
        store: null 
      }
    case RESET_STORE_DATA:
      return initialState
    default:
      return state
  }
}

export const updateCartData = payload => ({ type: UPDATE_CART_DATA, payload })

export const updateStoreData = payload => ({ type: UPDATE_STORE, payload })

export const cleanStoreData = payload => ({ type: RESET_STORE_DATA, payload })

export const resetCartData = () => ({ type: RESET_CART_DATA })