const UPDATE_PAYMENT_METHOD = 'UPDATE_PAYMENT_METHOD'
const RESET_PAYMENT_METHOD = 'RESET_CPAYMENT_METHOD'


const initialState = {
    paymentMethod:{type:'cash',selectedCard:{}},
//   store: null
}

export default(state = initialState, action) => {
  switch (action.type) {
    case UPDATE_PAYMENT_METHOD:
      return {
        ...state,
        paymentMethod: action.payload
      }
    case RESET_PAYMENT_METHOD:
      return initialState
    default:
      return state
  }
}

export const updatePaymentMethod = payload => ({ type: UPDATE_PAYMENT_METHOD, payload })

export const resetPaymentMethod = () => ({ type: RESET_PAYMENT_METHOD })