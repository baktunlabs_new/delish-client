const UPDATE_LOCACION_DATA = 'UPDATE_LOCACION_DATA'
const RESET_DATA_LOCATION = 'RESET_DATA_LOCATION'

const initialState = {
  location: null
}

export default(state = initialState, action) => {
  switch (action.type) {
    case UPDATE_LOCACION_DATA:
      return {
        ...state,
        location: action.payload
      }
    case RESET_DATA_LOCATION:
      return initialState
    default:
      return state
  }
}

export const udpateLocationData = payload => ({ type: UPDATE_LOCACION_DATA, payload })

export const resetDataLocation = () => ({ type: RESET_DATA_LOCATION })