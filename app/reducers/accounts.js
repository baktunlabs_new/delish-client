const UPTADE_ACCOUNT_DATA = 'UPTADE_SESSION_DATA'
const UPDATE_TOKEN_SESSION = 'UPDATE_TOKEN_SESSION'
const LOGOUT_USER = 'LOGOUT_USER'
const INITIAL_RUTA = 'INITIAL_RUTA'
const UPDATE_RUTA_DATA = 'UPDATE_RUTA_DATA'

const initialState = {
    token: null,
    account: null,
}



export default(state = initialState, action) => {
    switch (action.type) {
        case UPTADE_ACCOUNT_DATA:
            return {
                ...state,
                account: action.payload
            }

        case UPDATE_TOKEN_SESSION:
            return {
                ...state,
                token: action.payload
            }

        case LOGOUT_USER:
            return initialState

       

        default:
            return state
    }
}

export const updateAccountData = payload => ({type: UPTADE_ACCOUNT_DATA, payload})

export const updateTokenSession = payload => ({type: UPDATE_TOKEN_SESSION, payload})

export const logOutUser = () => ({type: LOGOUT_USER})