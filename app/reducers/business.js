const SET_BUSINESSID = 'SET_BUSINESSID'
const CLEAN_BUSINESSID = 'CLEAN_BUSINESSID'


const initialState = {
    businessId: null,
}



export default(state = initialState, action) => {
    switch (action.type) {
        case SET_BUSINESSID:
            return {
                ...state,
                businessId: action.payload
            }

        case CLEAN_BUSINESSID:
            return initialState

        default:
            return state
    }
}

export const setBusinessId = payload => ({type: SET_BUSINESSID, payload})

export const cleanBusinessId = () => ({type: CLEAN_BUSINESSID})