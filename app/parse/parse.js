import Parse from 'parse/react-native'
import AsyncStorage from '@react-native-community/async-storage'
import OneSignal from 'react-native-onesignal'

Parse.setAsyncStorage(AsyncStorage);
Parse.initialize('8YBorQZJH8gvbSnr8QnA');
Parse.serverURL = 'https://chevedelivery.herokuapp.com/parse';

export const checkSession = async () => {
    try {
        const session = await Parse.Session.current()
        if (!session) return null
        return session.get('sessionToken')
    } catch (err) {
        return null
    }
}

export const getCurrentUser = async () => {
    try {
        var currentUser =  await Parse.User.currentAsync();
        await currentUser.fetch()
        if (currentUser) {
            return currentUser;
        } else {
            return false
        }
    } catch (err) {
        return false
    }
}

export const userLogIn = async({email, password}) => {
    try {

        var emailLower = email.toLowerCase()
        var currUser = await Parse.User.logIn(emailLower.trim(), password).catch((err)=>{
            return err
        });
        if (currUser.id) {
            return {
                code: 200, 
                data:currUser
            }

        } else {
            return {
                code: 101, 
                data: currUser
                }
        }
        // console.log("currentUser", currUser)
       
        
        

    } catch (e) {
        console.log('error',e)
        if (e === "Error: Invalid username/password.") {
          
            return {
                code: 500, 
                data: "Favor de contactarse con soporte"
                }
        } 

    }
}
export const singUpUser = async ({ name, lastName, email, phone, password, }) =>{
    
    const user = new Parse.User();
    user.set('username', email);
    user.set('email', email)
    user.set('password', password);
    user.set('name', name);
    user.set('lastName', lastName);
    user.set('phone', phone);
    const createdUser = await user.signUp().catch((err)=>{
        console.log('signin Err',err)
        return false;
    });

    // var currentUser = Parse.User.current();
    // console.log('currentUser',currentUser);
    return createdUser;
}

export const logUserOutParse = async() => {
    try {
        await Parse.User.logOut()
        OneSignal.deleteTag('email')
        return true
    } catch (e) {
        throw e
    }
}


export const updateUser = async ({ name, lastName,phone, birthday, loadedImg, gender, userId, state, nationality }) => {
    try {
        const respuesta = await Parse.Cloud.run("updateUser", {})
        return respuesta
    } catch (error) {
        console.log('Error', error)
        return {}
    }
}


export const resetPassword = async ({ email }) => {
    try {
        console.log('entro', email)
        const respuesta = await Parse.Cloud.run("resetPassword", {
            email:email
        })
        console.log('resp',respuesta)
        return respuesta
    } catch (error) {
        console.log('Error', error)
        return {}
    }
}

export const autoComplete = async ({ value }) => {
    try {

        const respuesta = await Parse.Cloud.run("autoComplete", {
            value
        })

        console.log('autoComplete',respuesta)
        return respuesta
    } catch (error) {
        console.log(' autoComplete Error', error)
        return {}
    }
}

export const getPlaceDetails = async ({ placeId }) => {
    try {
        
        const respuesta = await Parse.Cloud.run("getPlaceDetails", {
            placeId
        })
        console.log('getPlaceDetails',respuesta)
        return respuesta
    } catch (error) {
        console.log('getPlaceDetails Error', error)
        return {}
    }
}

export const saveFavoriteLocation = async ({ data }) => {
    try {
        const {
            position,
            latitude,
            longitude,
            editableAddress,
            placeReference,
            placeTitle,
            placeId
        } = data

        const currentUser =  await Parse.User.currentAsync();

        const respuesta = await Parse.Cloud.run("saveFavLocation", {
            userId: currentUser.id,
            position,
            latitude,
            longitude,
            editableAddress,
            placeReference,
            placeTitle,
            placeId
        })
        console.log('saveFavoriteLocation',respuesta)
        return respuesta
    } catch (error) {
        console.log('saveFavoriteLocation Error', error)
        return {}
    }
}

export const getFavLocation = async () => {
    try {
        const currentUser =  await Parse.User.currentAsync();
        const respuesta = await Parse.Cloud.run("getFavLocation", {
            userId: currentUser.id,
        })
        console.log('getFavLocation',respuesta)
        return respuesta
    } catch (error) {
        console.log('getFavLocation Error', error)
        return {}
    }
}

export const updateDefaultLocation = async ({favLocationId}) => {
    try {
        const currentUser =  await Parse.User.currentAsync();
        const respuesta = await Parse.Cloud.run("updateDefaultLocation", {
            userId: currentUser.id,
            favLocationId
        })
        console.log('updateDefaultLocation',respuesta)
        return respuesta
    } catch (error) {
        console.log('updateDefaultLocation Error', error)
        return {}
    }
}

export const getDefaultLocation = async () => {
    try {
        const currentUser =  await Parse.User.currentAsync();
        const location = currentUser.get('defaultLocation');
        await location.fetch();
        const respuesta = {
            id: location.id,
            placeId: location.get('placeId'),
            latitude: location.get('latitude'),
            longitude: location.get('longitude'),
            position: location.get('address'),
            editableAddress: location.get('editableAddress'),
            placeReference: location.get('placeReference'),
            placeTitle: location.get('placeTitle'),
        }
        console.log('updateDefaultLocation',respuesta)
        return respuesta
    } catch (error) {
        console.log('updateDefaultLocation Error', error)
        return {}
    }
}

export const nearBusiness = async ({
    latitude,
    longitude
}) => {
    try {
        const response = await Parse.Cloud.run("nearBusiness", {
            latitude,
            longitude
        })
        if (response.code !== 200) return []
        return response.data
    } catch (error) {
        return []
    }
}

export const storeData = async ({
    storeId
}) => {
    try {
        const response = await Parse.Cloud.run("storeData", {
            storeId
        })
        return response.data
    } catch (error) {
        return {}
    } 
}

export const createOrder = async ({
    storeId,
    total,
    address,
    location,
    products,
    paymentMethod
}) => {
    try {
        const currentUser =  await Parse.User.currentAsync();
        const response = await Parse.Cloud.run("createOrder", {
            userId: currentUser.id,
            storeId,
            total,
            address,
            location,
            products,
            paymentMethod
        })
        return response
    } catch (error) {
        return false
    } 
}

export const subscribeOrder = async ({orderId}) => {
    const query = new Parse.Query('Order')
    query.include(['store', 'deliveryMan.user'])
    query.equalTo('objectId', orderId)
    return await query.subscribe()
}

export const getOrder = async ({orderId}) => {
    const response = await Parse.Cloud.run("getOrder", { orderId })
    return response
}

export const rateOrder = async ({
    orderId,
    rate
}) => {
    const response = await Parse.Cloud.run('rateOrder', { orderId, rate })
    return response
}

export const getOrdersHistory = async () => {
    try {
        const currentUser =  await Parse.User.currentAsync()
        const response = await Parse.Cloud.run("getOrdersHistory", {
            userId: currentUser.id
        })
        return response
    } catch (error) {
        return false
    } 
}

export const getUserData = async () => {
    try {
        const currentUser =  await Parse.User.currentAsync()
        const response = await Parse.Cloud.run("getUserData", {
            userId: currentUser.id
        })
        return response
    } catch (error) {
        return false
    } 
}

export const updateUserProfile = async ({
    name,
    lastName,
    phone,
    email,
    profilePicture
}) => {
    try {
        const currentUser =  await Parse.User.currentAsync()
        const response = await Parse.Cloud.run("updateUserProfile", {
            userId: currentUser.id,
            name,
            lastName,
            phone,
            email,
            profilePicture
        })
        return response
    } catch (error) {
        return false
    }
}

export const getCurrentOrder = async () => {
    try {
        const currentUser =  await Parse.User.currentAsync()
        const response = await Parse.Cloud.run("getCurrentOrder", {
            userId: currentUser.id
        })
        return response
    } catch (error) {
        return false
    } 
}

export const cancelOrder = async ({orderId}) => {
    try {
        const response = await Parse.Cloud.run('cancelOrder', { orderId })
        return response
    } catch (error) {
        return false
    }
}

export const productData = async ({ productId }) => {
    try {
        console.log('params', productId)
        const response = await Parse.Cloud.run('productData', { productId })
        return response
    } catch (error) {
        return false
    }
}

export const getBusinessByUser = async () => {
    try {
        const currentUser =  await Parse.User.currentAsync()
        const response = await Parse.Cloud.run('getBusinessByUser', { userId: currentUser.id })
        return response
    } catch (error) {
        return false
    }
}

export const getOrdersByBusiness = async ({storeId, status}) => {
    try {
        const response = await Parse.Cloud.run('getOrdersByBusinessMobile', { businessId: storeId, status })
        return response
    } catch (error) {
        return false
    }
}

export const subscribeListOrders = async ({ storeId }) => {
    const businessQ = new Parse.Query('Business')
    const business = await businessQ.get(storeId)
    const query = new Parse.Query('Order')
    query.include(['store', 'user', 'deliveryMan'])
    query.equalTo('store', business)
    return await query.subscribe()
}

export const acceptOrder = async ({ orderId, estimatedTime  }) => {
    try {
        const response = await Parse.Cloud.run('acceptOrder', { orderId, estimatedTime })
        return response
    } catch (error) {
        return false
    }
}

export const toDeliver = async ({ orderId }) => {
    try {
        const response = await Parse.Cloud.run('toDeliver', { orderId })
        return response
    } catch (error) {
        return false
    }
}

export const finishOrder = async ({ orderId }) => {
    try {
        const response = await Parse.Cloud.run('finishOrder', { orderId })
        return response
    } catch (error) {
        return false
    }
}

export const getAvailableDeliveries = async ({ businessId }) => {
    try {
        const response = await Parse.Cloud.run('getAvailableDeliveries', { businessId })
        return response
    } catch (error) {
        return false
    }
}

export const setDeliveryMan = async ({ orderId, deliveryManId }) => {
    try {
        const response = await Parse.Cloud.run('setDeliveryMan', { orderId, deliveryManId })
        return response
    } catch (error) {
        return false
    }
}




