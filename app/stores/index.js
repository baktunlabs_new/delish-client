import React from 'react'
import { createStore } from 'redux'
import { persistStore, persistCombineReducers } from 'redux-persist'
// import storage from 'redux-persist/lib/storage'
import AsyncStorage from '@react-native-community/async-storage'

import { accounts, location, shoppingCart, paymentMethod, business } from '../reducers';

const reducers = persistCombineReducers({
    storage: AsyncStorage,
    key: 'root',
    whitelist: ['accounts', 'business']
}, {
    accounts,
    location,
    shoppingCart,
    paymentMethod,
    business

})

export const store = createStore(reducers)

export const persistor = persistStore(store)