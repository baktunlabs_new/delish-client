import React from 'react'
import {
  FlatList,
  TouchableOpacity,
  ImageBackground,
  StyleSheet,
  Text,
  Image,
  View, 
  Linking
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { primaryColor, secondaryColor } from '../utils/colors'

const gradientColors = ['rgba(255,255,255,0)', primaryColor]
const gradientLocations = [0.10, 0.90]

const StoreList = ({
  data,
  onPress,
  showFooter,
  sendMsg
}) => (
  <FlatList
    data={data}
    showsVerticalScrollIndicator={false}
    keyExtractor={item => item.id}
    contentContainerStyle={{
      padding: 10
    }}
    renderItem={({ item }) => {
      console.log('item', item)
      return <TouchableOpacity
        onPress={() => onPress(item.id)}
        style={styles.itemStore}>
        <Image
          source={item.cover? {uri:item.cover.location }: require('../assets/placeholderStore.png')}
          style={{height:110, width:'30%', borderRadius:10}}

        />
        <View style={{flex:1, justifyContent:'flex-start'}}>
          <Text allowFontScaling={false} style={styles.titleStore}>
            {item.name}
          </Text>
          <Text allowFontScaling={false} numberOfLine={4} ellipsizeMode='tail' style={styles.storeDescription} >
            {item.description}
          </Text>
        </View>
        {/* <ImageBackground
          source={item.image? item.image : require('../assets/placeholderStore.png')}
          style={styles.imgStore}
          borderRadius={10}
          resizeMode='cover'>
          <LinearGradient
            locations={gradientLocations}
            colors={gradientColors}
            style={styles.gradient}>
            <Text style={styles.titleStore}>
              {item.name}
            </Text>
          </LinearGradient>
        </ImageBackground> */}
      </TouchableOpacity>
    }}
    ListFooterComponent={ () => {
      
      return showFooter ?<View style={{ alignSelf:'flex-end', backgroundColor:'white', width:'100%', height: 150}}>
      <Image
        source={require('../assets/logo_01.png')}
        style={{ width: 50,height:50, alignSelf:'center', borderRadius:10, marginBottom:20}}
      />
      <Text style={{textAlign:'center', color:'black'}}>{`AGREGA TU TIENDA\nSI VENDES COMIDA CON SERVICIO A DOMICILIO Y TIENES TUS PROPIOS REPARTIDORES ABRE TU TIENDA AQUI PIDE INFORMES A NUESTRO WHATSAPP `}
      <Text style={{textAlign:'center', color:'black', fontWeight:'bold'}} onPress={sendMsg}>#9993422523</Text></Text>
      </View>:null
    }}
  />
)

const styles = StyleSheet.create({
  itemStore: {
    // height: 120,
    backgroundColor: 'white',
    borderRadius: 10,
    marginBottom: 15,
    // justifyContent: 'center',
    alignItems: 'center',
    flexDirection:'row', 
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
    paddingVertical:2,
    paddingRight:15,
    overflow: 'hidden'
  },
  imgStore: {
    height: 200,
    width: '100%'
  },
  gradient: {
    width:"100%", 
    height:"100%", 
    position:"absolute",
    borderRadius: 10,
    marginBottom: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleStore: {
    color: 'black',
    // position: 'absolute',
    left: 10,
    bottom: 15,
    // textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {
      width: -1,
      height: 1
    },
    textShadowRadius: 10,
    fontSize: 18,
    fontWeight:'bold'
  },
  storeDescription: {
    color: 'gray',
    // position: 'absolute',
    left: 10,
    bottom: 15,
    // textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {
      width: -1,
      height: 1
    },
    textShadowRadius: 10,
    fontSize: 14,
    // fontWeight:'bold'
  }
})

export default StoreList
