import React from 'react'
import {
  View,
  Modal,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  Dimensions,
  Platform
} from 'react-native'
import Button from './Button'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { ScrollView, TextInput } from 'react-native-gesture-handler'
import { lightpurple, pinkish } from '../utils/colors'

const { width, height } = Dimensions.get('window')

const placeHolder = require('../assets/placeholderStore.png')
const preloadedImage = Image.resolveAssetSource(placeHolder).uri


const ModalProduct = ({
  modalVisible,
  onRequestClose,
  product,
  onPress,
  onAdd,
  changeText,
  comment,
  renderCheck,
  onPressAddon,
}) => {
  return <Modal
    animationType='slide'
    transparent={false}
    visible={modalVisible}
    onRequestClose={onRequestClose}>
      <View style={styles.centeredView}>
      <ScrollView
          showsVerticalScrollIndicator={true}
          >
          <TouchableOpacity
            onPress={onRequestClose}
            style={styles.closeModalButton}>
              <Icon
                name='close'
                color='black'
                size={25}
              />
          </TouchableOpacity>
          <View style={styles.imgContainer}>
            <Image 
              source={{uri:product.image? product.image : preloadedImage}}
              style={{flex:1, }}
            />
          </View>
          <View style={styles.body}>
          
            <Text style={styles.title}>{product.name}</Text>
            <Text style={styles.description}>{product.description}</Text>
            <Text style={styles.price}>{`$${product.price.toFixed(2)}`}</Text>
            {
              product.addOns && product.addOns.categories.length > 0? product.addOns.categories.map((addon, catIndex) => {
                return <View key={addon.id} >
                  <View style={styles.addons}>
                  <Text style={{fontWeight:'bold', color:'black', fontSize:20}}>{addon.description}</Text>
                  {addon.subtitle ?  <Text style={{color:'gray'}}>{addon.subtitle}</Text>:null}
                  </View>
                  {
                    addon.toppings.length > 0 ? addon.toppings.map((topping, topIndex) => {
                      return <View key={topIndex} style={{paddingHorizontal:10, paddingVertical:3 , backgroundColor: '#fcfcfc', flexDirection:"row",marginBottom:5, justifyContent:'space-between'}}>
                        <Text style={{ color:'black' }}>{`${topping.description}\n`}{topping.price > 0 ?<Text style={{ color: 'gray'}}>{`$${topping.quantity > 0?(topping.quantity * topping.price).toFixed(2): topping.price.toFixed(2)}`}</Text>:null}</Text>
                        { 
                          topping.max_limit === 1 ? renderCheck(topping.quantity, addon.topping_type_id, catIndex, topIndex): 
                          <View style={styles.addOnBottomContainer}>
                            <View style={styles.addOnFlexHorizontal}>
                              { topping.quantity > 0 ?<Button
                                onPress={() => onPressAddon('-', catIndex,topIndex)}
                                style={styles.addOnRemoveButton}>
                                <Icon
                                  name='remove'
                                  color='white'
                                  size={25}
                                />
                              </Button>:null}
                             {
                               topping.quantity > 0 ?
                                <View style={styles.addOnAmountContainer}>
                                  <Text style={styles.addOnAmount}>{topping.quantity}</Text>
                                </View>:null
                              }
                              <Button
                                onPress={() => onPressAddon('+', catIndex, topIndex)}
                                style={topping.quantity > 0 ? styles.addOnAddButton : styles.addOnAddButtonDisabled}>
                                <Icon
                                  name='add'
                                  color='white'
                                  size={25}
                                />
                              </Button>
                            </View>
                          </View>
                        }
                      </View>
                    }) : null
                  }
                </View>
              }):
              null
            }
            <Text style={styles.addTitle}>Comentarios</Text>
              <TextInput
                  placeholder="Escribe tus comentarios aquí..."
                  placeholderTextColor="black"
                  autoCapitalize={"none"}
                  onChangeText={changeText}
                  style={styles.textComment}
                  value={comment}
                />
            <Text style={styles.addTitle}>Agregar cantidad</Text>
            <View style={styles.bottomContainer}>
              <View style={styles.  flexHorizontal}>
                <Button
                  onPress={() => onPress('-')}
                  style={styles.removeButton}>
                  <Icon
                    name='remove'
                    color='white'
                    size={25}
                  />
                </Button>
                <View style={styles.amountContainer}>
                  <Text style={styles.amount}>{product.quantity}</Text>
                </View>
                <Button
                  onPress={() => onPress('+')}
                  style={styles.addButton}>
                  <Icon
                    name='add'
                    color='white'
                    size={25}
                  />
                </Button>
              </View>
              <Text style={styles.total}>{`$${(product.extraTotal?(product.price * product.quantity) + (product.extraTotal * product.quantity):(product.price * product.quantity) ).toFixed(2)}`}</Text>
              <Button
                onPress={onAdd}
                style={styles.aceptButton}>
                <Text style={styles.aceptButtonText}>Agregar a canastilla</Text>
              </Button>
            </View>
          </View>
          </ScrollView>
      </View>
  </Modal>
}

const styles = StyleSheet.create({
  centeredView: {
    flex:1,
    // justifyContent: "center",
    // alignItems: "center",
    backgroundColor: 'white',
  },
  modalView: {
    width: width,
    margin: 20,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  closeModalButton: {
    position: 'absolute',
    top: 10,
    right: 10,
    width: 30,
    height: 30,
    borderRadius: 30 / 2,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
        width: 0,
        height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
    zIndex: 2,
    marginTop: Platform.OS === 'ios'? 30:0
  },
  body: {
    padding: 10
  },
  imgContainer: {
    height: 150,
    width: '100%',
    backgroundColor: 'grey',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    color: pinkish
  },
  description: {
    fontSize: 15,
    color: 'black',
    marginTop: 10
  },
  price: {
    fontSize: 13,
    color: 'black',
    fontWeight: 'bold',
    marginVertical: 15,
    paddingHorizontal: 5
  },
  addTitle: {
    fontSize: 18,
    color: 'black',
    fontWeight: 'bold'
  },
  bottomContainer: {
    paddingVertical: 10,
    alignItems: 'center',
    marginTop: 10
  },
  flexHorizontal: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15
  },
  removeButton: {
    padding: 10,
    backgroundColor: lightpurple,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8
  },
  amountContainer: {
    paddingVertical: 10,
    paddingHorizontal: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 2,
    borderBottomWidth: 2,
    borderColor: lightpurple
  },
  amount: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'black'
  },
  addButton: {
    padding: 10,
    backgroundColor: lightpurple,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8
  },
  total: {
    fontSize: 13,
    color: 'black',
    fontWeight: 'bold',
    marginBottom: 15
  },
  aceptButton: {
    padding: 10,
    backgroundColor: lightpurple,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8
  },
  aceptButtonText: { color: 'white', fontWeight: 'bold' },
  textComment: {
    //fontSize: 16,
    //marginLeft: 25,
    borderBottomColor: pinkish,
    borderBottomWidth: 1,
    width: "80%",
    height: 50,
    color: 'black',
    fontWeight: "bold",
    backgroundColor: 'transparent',
    borderRadius: 6,
    paddingHorizontal: 10,
    marginBottom:20

  },
  addons: { 
    borderWidth: StyleSheet.hairlineWidth, 
    borderRadius: 5, 
    height: 50, 
    justifyContent:'center', 
    padding:10, 
    marginBottom:10,
    
  },
  addOnBottomContainer: {
    paddingVertical: 5,
    alignItems: 'center',
    // marginTop: 10
  },
  addOnFlexHorizontal: {
    flexDirection: 'row',
    alignItems: 'center',
    
  },
  addOnRemoveButton: {
    padding: 5,
    backgroundColor: lightpurple,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8
  },
  addOnAmountContainer: {
    paddingVertical: 5,
    paddingHorizontal: 8,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 2,
    borderBottomWidth: 2,
    borderColor: lightpurple
  },
  addOnAmount: {
    fontSize: 14,
    fontWeight: 'bold',
    color: 'black'
  },
  addOnAddButton: {
    padding: 5,
    backgroundColor: lightpurple,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8
  },
  addOnAddButtonDisabled : {
    padding: 5,
    backgroundColor: lightpurple,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius:8
  }
})

export default ModalProduct
