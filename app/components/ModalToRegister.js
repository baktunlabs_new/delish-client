import React, { Component } from "react";
import { 
    StyleSheet, 
    View, 
    StatusBar, 
    Modal, 
    Dimensions, 
    Image,
    Text
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { lightpurple } from "../utils/colors";
const { width, height } = Dimensions.get('window')
const ModalToRegister = ({
    modalVisible,
    onRequestClose,
    onLogin
}) => {
  return (
    <Modal
    animationType='slide'
    transparent={false}
    visible={modalVisible}
    onRequestClose={onRequestClose}>
    <View style={styles.container}>
      <View style={styles.imgContainer}>
          <Image
            source={require('../assets/ico_error.png')}
            style={{
                width: width * .70,
                alignSelf:'center'}}
            resizeMode='contain'
          />
      </View>
      <View style={styles.body}>
      <Text allowFontScaling={false} style={styles.txtTitle}>Registrate o inicia sesión en Delish</Text>
      <Text allowFontScaling={false} style={styles.txtInfo}>Recuerda que es necesario tener una sesión iniciada o registrarse en la
        app para poder ingresar artículos al carrito y hacer solicitudes.</Text>

        <View style={styles.btnContainer}>
            <TouchableOpacity onPress={onRequestClose} style={styles.btn}>
                <Text style={styles.txtButton}>REGRESAR</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={onLogin} style={styles.btn}>
                <Text style={styles.txtButton}>REGISTARSE O INICIAR SESIÓN</Text>
            </TouchableOpacity>
        </View>
      </View>
    </View>
    </Modal>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 34,
  },
  imgContainer: {
    width: width,
    height: height * .35,
    justifyContent: 'center',
  },
  body:{
    flex:1,
    paddingHorizontal: 20
  },
  txtTitle: {
    fontWeight: 'bold', 
    fontSize:20
  },
  txtInfo: {
      fontSize: 18,
      marginTop: 20
  },
  btnContainer: {
      flexDirection:'row',
      marginTop: height * .10,
      justifyContent:'space-between'
  },
  btn:{
      padding: 15,
      alignSelf:'center',
      maxWidth: width * .4
  },
  txtButton: {
      fontWeight:'bold',
      color: lightpurple
  }
 
});

export default ModalToRegister;