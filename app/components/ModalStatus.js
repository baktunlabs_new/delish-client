import React from 'react'
import {
  View,
  Modal,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  Dimensions,
  SafeAreaView,
  TextInput
} from 'react-native'
import Button from './Button'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { lightpurple } from '../utils/colors'
import { FlatList } from 'react-native-gesture-handler'

const { height,width } = Dimensions.get('window')


const status = [
    {value:'created', label: 'Pendiente de confirmación'},
    {value:'accepted', label: 'En preparación'},
    {value:'inDelivery', label: 'En Camino'},
    {value:'arrived', label: 'Entregado'},
    {value:'finished', label: 'Finalizado'},
    {value:'cancelled', label: 'Cancelada'},
    {value: null, label: 'Todos'},
]
const ModalStatusPicker = ({
  modalVisible,
  onRequestClose,
  onSelect
}) => {
  return <Modal
    animationType='fade'
    transparent
    visible={modalVisible}
    onRequestClose={onRequestClose}>
    <SafeAreaView style={styles.safeContainer}>
      <View style={styles.centeredView}>
       <FlatList
           data={status}
           showsVerticalScrollIndicator={false}
           keyExtractor={item => item.value}
           renderItem={({ item }) => {
            console.log('item', item)
            return <TouchableOpacity
                onPress={() => onSelect(item)}
                style={styles.itemStore}>
                <View style={styles.itemcontainer}>
                    <Text style={{ color: 'black',}}>{item.label}</Text>
                </View>
            </TouchableOpacity>
            }}
       />
      </View>
      </SafeAreaView>
  </Modal>
}

const styles = StyleSheet.create({
    safeContainer: {
        flex:1,
        backgroundColor: 'rgba(0, 0, 0, 0.20)',
        justifyContent:'center'
    },
    centeredView: {
        width: '80%',
        // height: '50%',
        backgroundColor: 'white',
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        borderRadius: 10,
        padding: 20,
        justifyContent: 'center'
    },
    btnCall: { 
        backgroundColor: lightpurple, 
        height:50, 
        justifyContent:'center', 
        borderRadius:30, 
        marginHorizontal:20,
        marginTop: 50
    },
    btnAccept: {
        backgroundColor: '#00B61C', 
        paddingVertical:10,
        justifyContent:'center', 
        borderRadius:30, 
        width: width * 0.30
    },
    btnReject: {
        backgroundColor: '#EB110E', 
        paddingVertical:10,
        justifyContent:'center', 
        borderRadius:30, 
        width: width * 0.30
    },
    footer: {
        justifyContent: 'space-between',
        flexDirection:'row',
        marginBottom: 20
    },
    txtInput: {
        borderBottomColor:'gray',
        borderBottomWidth:1,
        paddingHorizontal:5
    },
    itemcontainer: {
        flex: 1, 
        borderBottomColor: lightpurple, 
        borderBottomWidth: 1 ,
        paddingVertical: 10,
        justifyContent: 'center',
        paddingHorizontal: 3
    }

})

export default ModalStatusPicker
