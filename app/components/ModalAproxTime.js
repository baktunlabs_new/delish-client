import React from 'react'
import {
  View,
  Modal,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  Dimensions,
  SafeAreaView,
  TextInput
} from 'react-native'
import Button from './Button'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { lightpurple } from '../utils/colors'

const { height,width } = Dimensions.get('window')

const ModalAproxTime = ({
  modalVisible,
  onRequestClose,
  onChangeText,
  onAccept
}) => {
  return <Modal
    animationType='fade'
    transparent
    visible={modalVisible}
    onRequestClose={onRequestClose}>
    <SafeAreaView style={styles.safeContainer}>
      <View style={styles.centeredView}>
        <View style={{ height:'70%', }}>
            <Text style={{marginBottom:20,  alignSelf:'center'}}>Por favor ingrese el tiempo de entrega aproximado en minutos.</Text>
            <TextInput
                placeholder="Tiempo Aproximado en Minutos"
                placeholderTextColor="gray"
                autoCapitalize={"none"}
                keyboardType='numeric'
                onChangeText={onChangeText}
                style={styles.txtInput}
            />
        </View>
        
        <View>
        <View style={styles.footer}>
            <TouchableOpacity onPress={onAccept} style={styles.btnAccept}>
                <Text style={{color:'white', alignSelf:'center', fontWeight:'bold'}}>Aceptar</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={onRequestClose} style={styles.btnReject}>
                <Text style={{color:'white', alignSelf:'center', fontWeight:'bold'}}>Cancelar</Text>
            </TouchableOpacity>
            
        </View>
        </View>
      </View>
      </SafeAreaView>
  </Modal>
}

const styles = StyleSheet.create({
    safeContainer: {
        flex:1,
        backgroundColor: 'rgba(0, 0, 0, 0.20)',
        justifyContent:'center'
    },
    centeredView: {
        width: '80%',
        height: '30%',
        backgroundColor: 'white',
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        borderRadius: 10,
        padding: 20,
    },
    btnCall: { 
        backgroundColor: lightpurple, 
        height:50, 
        justifyContent:'center', 
        borderRadius:30, 
        marginHorizontal:20,
        marginTop: 50
    },
    btnAccept: {
        backgroundColor: '#00B61C', 
        paddingVertical:10,
        justifyContent:'center', 
        borderRadius:30, 
        width: width * 0.30
    },
    btnReject: {
        backgroundColor: '#EB110E', 
        paddingVertical:10,
        justifyContent:'center', 
        borderRadius:30, 
        width: width * 0.30
    },
    footer: {
        justifyContent: 'space-between',
        flexDirection:'row',
        marginBottom: 20
    },
    txtInput: {
        borderBottomColor:'gray',
        borderBottomWidth:1,
        paddingHorizontal:5
    }

})

export default ModalAproxTime
