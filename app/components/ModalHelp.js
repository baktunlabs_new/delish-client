import React from 'react'
import {
  View,
  Modal,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  Dimensions,
  SafeAreaView
} from 'react-native'
import Button from './Button'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { lightpurple } from '../utils/colors'

const { height,width } = Dimensions.get('window')

const ModalHelp = ({
  modalVisible,
  onRequestClose,
  onCall
}) => {
  return <Modal
    animationType='slide'
    // transparent={false}
    visible={modalVisible}
    onRequestClose={onRequestClose}>
    <SafeAreaView style={styles.safeContainer}>
      <View style={styles.centeredView}>
      <Button
            style={{borderBottomWidth: StyleSheet.hairlineWidth}}
            onPress={onRequestClose}>
            <Icon
                name='chevron-left'
                color={lightpurple}
                size={50}
            />
        </Button>
        <View style={{flex:1, justifyContent:'center'}}>
        <Image
            source={require('../assets/dudas.png')}
            style={{
                width:width * 0.80,
                height: height * 0.50,
                alignSelf:'center',
                
            }}
            resizeMode='contain'
        />
        <View style={{paddingHorizontal:20, marginTop:20}}>
            <Text style={{fontSize:25, color:'black', textAlign:'center'}}>Comunicate con tu restaurante para resolver cualquier duda sobre tu pedido </Text>
        </View>
        <TouchableOpacity onPress={onCall} style={styles.btnCall}>
            <Text style={{fontSize:20, alignSelf: 'center', color: 'white'}}>Llamar al restaurante</Text>
        </TouchableOpacity>
        </View>
      </View>
      </SafeAreaView>
  </Modal>
}

const styles = StyleSheet.create({
  safeContainer: {
    flex:1,
    backgroundColor: 'white'
  },
  centeredView: {
    flex: 1,
    backgroundColor: 'white'
  },
  btnCall: { 
    backgroundColor: lightpurple, 
    height:50, 
    justifyContent:'center', 
    borderRadius:30, 
    marginHorizontal:20,
    marginTop: 50
}
 
})

export default ModalHelp
