import React from 'react'
import {
  View,
  Modal,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  Dimensions
} from 'react-native'
import Button from './Button'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { lightpurple, secondaryColor } from '../utils/colors'

const { height,width } = Dimensions.get('window')

const ModalDelivered = ({
  modalVisible,
  onRequestClose,
  onRate
}) => {
  return <Modal
    animationType='slide'
    transparent={true}
    visible={modalVisible}
    onRequestClose={onRequestClose}>
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <TouchableOpacity
            onPress={onRequestClose}
            style={styles.closeModalButton}>
              <Icon
                name='close'
                color='black'
                size={25}
              />
          </TouchableOpacity>
          <View style={styles.body}>
          <Image
              resizeMode='stretch'
              style={{
                backgroundColor: 'transparent',
                width:200,
                height:200,
                justifyContent: 'center',
                alignContent: 'center',
                alignSelf:'center'
              }}
              source={require('../assets/ico_finished.png')}/>
              {/* <Image
              resizeMode='stretch'
              style={{
                backgroundColor: 'transparent',
                marginTop:10,
                width:280,
                height:20,
                justifyContent: 'center',
                alignContent: 'center',
                alignSelf:'center'
              }}
              source={require('../assets/entregado.png')}/> */}
              <Text style={{fontSize: 30, color:'black', textAlign:'center'}}>¡Pedido entregado!</Text>
              <Text style={{fontSize:20, marginTop:10, color:'black', textAlign:'center'}}>¿Qué tal estuvo?</Text>
          </View>
          <View style={styles.footer}>
            <Button onPress={() => onRate(5)} style={styles.modalBtn}>
              <Text style={styles.payText}>Excelente</Text>
            </Button>
            <Button onPress={() => onRate(0)} style={styles.modalBtn}>
              <Text style={styles.payText}>No muy bien</Text>
            </Button>
          </View>
        </View>
      </View>
  </Modal>
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: 'rgba(0, 0, 0, 0.70)'
  },
  modalView: {
    width: width * 0.90,
    margin: 20,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5, 
    height:height/1.4
  },
  closeModalButton: {
    position: 'absolute',
    top: 10,
    right: 10,
    width: 30,
    height: 30,
    borderRadius: 30 / 2,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
        width: 0,
        height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
    zIndex: 2
  },
  body: {
    padding: 10,
    marginTop:30,
    justifyContent:'center',
    paddingHorizontal:30
  },
  imgContainer: {
    height: 150,
    width: '100%',
    backgroundColor: 'grey',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#05CD2F'
  },
  description: {
    fontSize: 15,
    color: 'black',
    marginTop: 10
  },
  price: {
    fontSize: 13,
    color: 'black',
    fontWeight: 'bold',
    marginVertical: 15,
    paddingHorizontal: 5
  },
  addTitle: {
    fontSize: 18,
    color: 'black',
    fontWeight: 'bold'
  },
  bottomContainer: {
    paddingVertical: 10,
    alignItems: 'center',
    marginTop: 10
  },
  flexHorizontal: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15
  },
  removeButton: {
    padding: 10,
    backgroundColor: '#05CD2F',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8
  },
  amountContainer: {
    paddingVertical: 10,
    paddingHorizontal: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 2,
    borderBottomWidth: 2,
    borderColor: '#05CD2F'
  },
  amount: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'black'
  },
  addButton: {
    padding: 10,
    backgroundColor: '#05CD2F',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8
  },
  total: {
    fontSize: 13,
    color: 'black',
    fontWeight: 'bold',
    marginBottom: 15
  },
  aceptButton: {
    padding: 10,
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8
  },
  aceptButtonText: { color: 'white', fontWeight: 'bold' },
  listContainer:{
    height:height/4,
    backgroundColor:'transparent',
    // marginTop:10
  },
  cartItem:{
    height:50,
    backgroundColor:'gainsboro',
    // borderBottomWidth: StyleSheet.hairlineWidth,
    flexDirection:'row',
    paddingVertical: 5,
    elevation:1,
    // borderRadius:3,
    // borderWidth:0.5,
    marginBottom:1,
    paddingHorizontal:10,
    justifyContent: 'space-between'

  },
  itemName:{
    color:"#05CD2F",
    alignSelf:'flex-start',
    fontWeight:'bold'
  },
  itemPrice:{
    alignSelf:'center',
  },
  quantityContainer:{
    justifyContent:'center',
    width: width/3.1
  },
  itemTextContainer:{
    justifyContent:'center',
    paddingHorizontal:5
  },
  footer: {
    backgroundColor: 'green',
    height:80,
    flexDirection:'row'
  },
  modalBtn:{
    height:"100%",
    width: "50%",
    backgroundColor: lightpurple, 
    elevation:5, 
    borderRightColor: StyleSheet.hairlineWidth,
    borderWidth:0.5,
    justifyContent: 'center',
    borderColor: secondaryColor
  },
  payText:{
    color:'white',
    fontWeight:'bold',
    fontSize:18, 
    alignSelf:'center'
  },

  locationContainer: {
    height: height*0.08, 
    backgroundColor:'transparent',
    paddingHorizontal:10,
    justifyContent:'center',
    marginTop:5
  },
  locationTitle: {
    fontSize:12
  },
  locationText: {
    fontSize: 18,
    fontWeight:'bold'
  },
  paymentMethodContainer: {
    flexDirection:'row',
    justifyContent:'flex-start',
    paddingHorizontal:10
  },
  paymentMethodBtn: { 
    backgroundColor:"#05CD2F", 
    height:50,
    justifyContent:'center'
  },
  storeTitle: {
    fontSize: 18,
    color: 'black'
  },
  totalContainer:{
    flexDirection:'row',
    justifyContent:'center',
    marginTop:15
  },
  totalTitle:{
    fontSize:30,
    fontWeight:'bold',
    alignSelf:'center'
  },
  totalValue:{
    fontSize:30,
    fontWeight:'bold',
    alignSelf:'center'
  },
  totalText: {
    backgroundColor:'transparent', 
    width:width/2.3, 
    justifyContent:'center'
  }
})

export default ModalDelivered
