import React from 'react'
import {
  FlatList,
  TouchableOpacity,
  ImageBackground,
  StyleSheet,
  Text,
  Image,
  View
} from 'react-native'

import { primaryColor, secondaryColor, lightpurple } from '../utils/colors'

const DeliveryManList = ({
  data,
  onPress
}) => (
  <FlatList
    data={data}
    showsVerticalScrollIndicator={false}
    keyExtractor={item => item.id}
    contentContainerStyle={{
      padding: 10
    }}
    renderItem={({ item }) => {
      console.log('item', item)
      return <TouchableOpacity
        onPress={() => onPress(item.id)}
        style={styles.itemStore}>
        <View style={{flexDirection:'row', justifyContent: 'space-between'}}>
            <Image
                defaultSource={require('../assets/icon_name.png')}
                source={item && item.user.img? {uri:item.user.img }: require('../assets/icon_name.png')}
                style={{
                    width:50,
                    height:50,
                    backgroundColor:lightpurple,
                    borderRadius:50/2,
                    borderWidth:1,
                    borderColor:lightpurple,
                    marginRight:10
                }}
            />
            <View>
                <Text style={[styles.nameTxt, {alignSelf:'center'}]}>{item.user? item.user.name:''}</Text>
                {/* <Text style={[styles.nameTxt, {alignSelf:'center'}]}>{item.user? item.user.name:''}</Text> */}
            </View>
            
            
        </View>
      </TouchableOpacity>
    }}
  />
)

const styles = StyleSheet.create({
  itemStore: {
    height: 90,
    backgroundColor: 'white',
    borderRadius: 10,
    marginBottom: 15,
    // justifyContent: 'center',
    alignItems: 'center',
    flexDirection:'row', 
    // shadowColor: "#000",
    // shadowOffset: {
    //   width: 0,
    //   height: 1,
    // },
    // shadowOpacity: 0.20,
    // shadowRadius: 1.41,
    // elevation: 2,
    paddingVertical:2,
    paddingHorizontal: 10,
    borderBottomColor: 'gray',
    borderBottomWidth: 1
  },
  imgStore: {
    height: 200,
    width: '100%'
  },
  gradient: {
    width:"100%", 
    height:"100%", 
    position:"absolute",
    borderRadius: 10,
    marginBottom: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleStore: {
    color: 'black',
    // position: 'absolute',
    left: 10,
    bottom: 15,
    // textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {
      width: -1,
      height: 1
    },
    textShadowRadius: 10,
    fontSize: 18,
    fontWeight:'bold'
  },
  storeDescription: {
    color: 'gray',
    // position: 'absolute',
    left: 10,
    bottom: 15,
    // textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {
      width: -1,
      height: 1
    },
    textShadowRadius: 10,
    fontSize: 14,
    // fontWeight:'bold'
  },
  nameTxt: {
      color: lightpurple,
      fontSize: 15
  }
})

export default DeliveryManList
