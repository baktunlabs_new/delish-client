import React from 'react'
import { connect } from 'react-redux'
import { NavigationActions, StackActions } from 'react-navigation'
import { 
    View, 
    Image, 
    Dimensions, 
    TouchableOpacity, 
    Text,
    StyleSheet,
    Alert,
} from 'react-native'
import { primaryColor,secondaryColor, pinkish, lightpurple } from '../utils/colors'

const { height, width } = Dimensions.get('window');

function onPressHandler (goTo, navigation) { 
    // const storeId = navigation.getParam('storeId')
    console.log(goTo,navigation )
    const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: goTo })],
    })

    navigation.dispatch(resetAction);

}

function onPressSoon() {
    Alert.alert('', '¡Proximamente!')
} 

const Menu = props => {
    const {
      current, 
      navigation
    } = props;
        return <View style={styles.menuContainer}>
        {/* 
            <TouchableOpacity 
            onPress={() => onPressSoon()}
            style={styles.btnContainer}>
                <Image
                    source={ current === 'Inicio' ? require('../assets/cat_inicio_activo.png') : require('../assets/cat_inicio_inactivo.png')}
                    style={styles.img}
                    resizeMode='contain'
                />
                <Text style={current === 'Inicio' ? styles.textSelected: styles.text}>{'Inicio'}</Text>
            </TouchableOpacity> 
        */}
        <TouchableOpacity
        onPress={() => onPressHandler('RestaurantOrders', navigation)}
        style={styles.btnContainer}>
        
            {/* <Image
                source={current === 'Favoritos' ? require('../assets/cat_fav_activo.png') : require('../assets/cat_fav_inactivo.png')}
                style={styles.img}
                resizeMode='contain'
            /> */}
            <Text style={current === 'Orders' ? styles.textSelected: styles.text}>{'Órdenes'}</Text>
        </TouchableOpacity>
        {/* 
        <TouchableOpacity 
        onPress={() => onPressSoon()}
        style={styles.btnContainer}>
            <Image
                source={current === 'Promo' ? require('../assets/cat_promo_activo.png') : require('../assets/cat_promo_inactivo.png')}
                style={styles.img}
                resizeMode='contain'
            />
            <Text style={current === 'Menu' ? styles.textSelected: styles.text}>{'Menú'}</Text>
        </TouchableOpacity> 
        */}
        <TouchableOpacity 
        onPress={() => onPressHandler('RestaurantSettings', navigation)}
        style={styles.btnContainer}>
            {/* <Image
                source={ current === 'Ajustes' ? require('../assets/cat_ajustes_activo.png') : require('../assets/cat_ajustes_inactivo.png')}
                style={styles.img}
                resizeMode='contain'
            /> */}
            <Text style={current === 'Opciones' ? styles.textSelected: styles.text}>{'Opciones'}</Text>
        </TouchableOpacity>
        </View>
}
const styles = StyleSheet.create({
    menuContainer: { 
        width:'100%', 
        height:height * 0.13, 
        backgroundColor:'#F3F3F3', 
        flexDirection:'row', 
        justifyContent:'space-between',
    },
    btnContainer: { 
        height:'100%', 
        width: width/2, 
        justifyContent:'center', 
        padding:5, 
    },
    img: {
        height: 50,
        width: 50,
        alignSelf:'center'
    },
    textSelected: {
        alignSelf:'center', 
        color: lightpurple,
        fontWeight:'bold'
    },
    text: {
        alignSelf:'center', 
        color: 'gray'
    }
})

export default Menu