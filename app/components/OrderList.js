import React from 'react'
import {
  FlatList,
  TouchableOpacity,
  ImageBackground,
  StyleSheet,
  Text,
  Image,
  View
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { lightpurple, pinkish, primaryColor, secondaryColor } from '../utils/colors'
import moment from 'moment'
import { linear } from 'react-native/Libraries/Animated/src/Easing'
const gradientColors = ['rgba(255,255,255,0)', primaryColor]
const gradientLocations = [0.10, 0.90]
function getStatus ( status ) {
    switch (status) {
        case 'created' : 
            return 'Pendiente de confirmación'
        case 'accepted':
            return 'En preparación'
        case 'cancelled':
            return 'Cancelada por cliente'
        case 'inDelivery': 
            return 'En Camino'
        case 'arrived':
            return 'Entregado'
        case 'finished':
            return 'Finalizado'
    } 
}

function statusColor ( status ) {
    switch (status) {
        case 'created' : 
            return '#FF8F00'
        case 'accepted':
            return '#009AFF'
        case 'cancelled':
            return '#FF000F'
        case 'inDelivery': 
            return pinkish
        case 'arrived':
            return '#24B122'
        case 'finished':
            return '#5B00FF'
    } 
}
const OrderList = ({
  data,
  onPress
}) => (
  <FlatList
    data={data}
    showsVerticalScrollIndicator={false}
    keyExtractor={item => item.id}
    // contentContainerStyle={{
    //   paddingTop: 15
    // }}
    renderItem={({ item }) => {
      console.log('item', item)
      return <TouchableOpacity
        onPress={() => onPress(item.id)}
        style={styles.itemStore}>
        <View style={{flex:1, justifyContent:'space-between', flexDirection:'row'}}>
          <View style={styles.statusContainer}>
            <Text style={{ fontSize:10, color: statusColor(item.status), textAlign:'center', }}>{getStatus(item.status)}</Text>
            <Text style={{fontSize:12, textAlign:'center', color:'black'}}>{moment(item.createdAt).format('DD/MM/YY[\n]HH:mm')}</Text>
          </View>
          <View style={styles.clientContainer}>
            <Text style={{ fontSize:13, color: lightpurple }}>{item.client}</Text>
            <Text style={{fontSize:12,  color:'gray'}}>{item.address}</Text>
          </View>
          <View style={styles.totalContainer}>
            <Text style={{ fontSize:13, color: 'black', fontWeight:'bold' }}>{`$${item.total.toFixed(2)}`}</Text>
          </View>
        </View>
      </TouchableOpacity>
    }}
    ListFooterComponent={ () => {
      
      return data.length === 0 ?<View style={{ alignSelf:'flex-end', backgroundColor:'white', width:'100%', height: 200}}>
      <Image
        source={require('../assets/nopedidos.png')}
        style={{ width: 200,height:200, alignSelf:'center', borderRadius:10, marginBottom:20}}
      />
      </View>:null
    }}
  />
)

const styles = StyleSheet.create({
  itemStore: {
    height: 90,
    backgroundColor: 'white',
    marginBottom: 15,
    flexDirection:'row', 
   
  },
  statusContainer: {
    // justifyContent:'center',
    width: '25%',
    height:'100%'
  }, 
  clientContainer: {
    // justifyContent:'center',
    width: '48%',
    height:'100%'
  },
  totalContainer: {
    justifyContent:'center',
    width: '25%',
    height:'100%' 
  } 
})

export default OrderList
