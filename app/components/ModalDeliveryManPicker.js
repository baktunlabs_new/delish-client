import React from 'react'
import {
  View,
  Modal,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  Dimensions,
  SafeAreaView,
  TextInput
} from 'react-native'
import Button from './Button'
import Icon from 'react-native-vector-icons/MaterialIcons'
import { lightpurple } from '../utils/colors'
import  DeliveryManList  from "./DeliveryManList";
const { height,width } = Dimensions.get('window')

const ModalDeliveryManPicker = ({
  modalVisible,
  onRequestClose,
  onSelect,
  data
}) => {
  return <Modal
    animationType='fade'
    visible={modalVisible}
    onRequestClose={onRequestClose}>
    <SafeAreaView style={styles.safeContainer}>
        <View style={styles.headerContainer}>
            <TouchableOpacity style={styles.btnContainer} onPress={onRequestClose}>
                <Image
                    source={require('../assets/flecha_back_white.png')}
                    style={{
                        width:30, 
                        height:30,
                        alignSelf:'center'
                    }}
                />
            </TouchableOpacity>
        </View>
        <View style={styles.centeredView}>
            <Text style={{fontSize:15, fontWeight:'bold', color:'black'}}>Seleccione a un conductor</Text>
            <DeliveryManList
                data={data}
                onPress={onSelect}
            />
                    
        </View>
        </SafeAreaView>
  </Modal>
}

const styles = StyleSheet.create({
    safeContainer: {
        flex:1,
        backgroundColor: 'white',
        paddingHorizontal:10,
       
    },
    centeredView: {
        flex:1,
        width:'90%',
        backgroundColor: 'white',
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        borderRadius: 10,
        padding: 20,
        marginTop: Platform.OS === 'ios' ? 30 : 15, 
    },
    headerContainer: {
        width: width, 
        position: 'absolute', 
        flexDirection: 'row', 
        backgroundColor: 'transparent', 
        paddingTop: Platform.OS === 'ios' ? 40 : 15, 
        paddingHorizontal: 20, 
        justifyContent: 'space-between', 
        zIndex:1
    }, 
    btnContainer: { 
        width: 50, 
        height: 50, 
        backgroundColor: lightpurple,
        borderRadius:25,
        justifyContent:'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.18,
        shadowRadius: 1.00,
        elevation: 1,
    },
    
})

export default ModalDeliveryManPicker
