import { createAppContainer, createBottomTabNavigator, createNavigator,  } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'

// SCREENS
import SplashScreen from '../screen/splash'
import MainScreen from '../screen/main'
import LoginScreen from '../screen/login'
import Register from '../screen/register'
import Map from '../screen/Map'
import ShoppingCart from '../screen/shoppingCart'
import Checkout from '../screen/checkout'
import Store from '../screen/Store'
import Profile from '../screen/Profile'
import PaymentMethods from '../screen/PaymentMethods'
import AddPaymentMethod from '../screen/AddPaymentMethod'
import ServiceHistory from '../screen/ServiceHistory'
import SuccessfulOrder from '../screen/SuccessfulOrder'
import OrderTracking from '../screen/OrderTracking'
import Location from '../screen/Location'
//RESTAURANT VIEWS// 
import RestaurantMainScreen from '../screen/RestaurantScreens/RestaurantMain'
import RestaurantOrdersScreen from '../screen/RestaurantScreens/RestaurantOrders'
import RestaurantSettingsScreen from '../screen/RestaurantScreens/RestaurantSettings'
import SelectRestaurantScreen from '../screen/RestaurantScreens/SelectRestaurant'
import RestaurantOrderDetailScreen from '../screen/RestaurantScreens/RestaurantOrderDetail'

const StackNavigatorConfig = {
  headerMode: 'none',
  defaultNavigationOptions: {
    gesturesEnabled: false,
    headerShown: false,
    // animationEnabled: false
  }
}
const restuarantNav = {
  gesturesEnabled: false,
  headerShown: false,
  animationEnabled: false
} 
const AppStackNavigation = createStackNavigator({
  Splash: { screen: SplashScreen },
  Login: { screen: LoginScreen },
  Main: { screen: MainScreen },
  Register: { screen: Register },
  ShoppingCart: { screen: ShoppingCart },
  Checkout: { screen: Checkout },
  PaymentMethods: { screen: PaymentMethods },
  AddPaymentMethod: { screen: AddPaymentMethod },
  ServiceHistory: { screen: ServiceHistory },
  App: { screen: MainScreen },
  Map: { screen: Map },
  Store: { screen: Store },
  Profile: { screen: Profile },
  SuccessfulOrder: { screen: SuccessfulOrder },
  OrderTracking: { screen: OrderTracking },
  Location: { screen: Location },
  RestaurantMain: { screen: RestaurantMainScreen, navigationOptions: restuarantNav },
  RestaurantOrders: { screen: RestaurantOrdersScreen, navigationOptions:  restuarantNav  },
  RestaurantSettings: { screen: RestaurantSettingsScreen , navigationOptions: restuarantNav },
  RestaurantOrderDetail: { screen: RestaurantOrderDetailScreen },
  SelectRestaurant: { screen: SelectRestaurantScreen },
  
}, StackNavigatorConfig)

const App = createAppContainer(AppStackNavigation)

export default App