import React from 'react'
import {
  Image,
  StyleSheet,
  Dimensions,
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Platform,
  Alert,
  TextInput
} from 'react-native'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/MaterialIcons'
import IconIon from 'react-native-vector-icons/Ionicons'
import Button from '../components/Button'
import { autoComplete, getFavLocation, updateDefaultLocation } from '../parse/parse'
import { udpateLocationData } from '../reducers/location'
import { lightpurple, pinkish } from '../utils/colors'

const locations = [
    { id: '1', name: 'Ubicacion 1', address: 'calle 21, 123, col. falsa' },
    { id: '2', name: 'Ubicacion 2', address: 'calle 21, 123, col. falsa' },
    { id: '3', name: 'Ubicacion 3', address: 'calle 21, 123, col. falsa' },
    { id: '4', name: 'Ubicacion 4', address: 'calle 21, 123, col. falsa' },
    { id: '5', name: 'Ubicacion 5', address: 'calle 21, 123, col. falsa' },
    { id: '6', name: 'Ubicacion 6', address: 'calle 21, 123, col. falsa' }
  ]
const locations2 = [
    { id: '1', name: 'Ubicacion 1', address: 'calle 21, 123, col. prueba' },
    { id: '2', name: 'Ubicacion 2', address: 'calle 21, 123, col. prueba' },
    { id: '3', name: 'Ubicacion 3', address: 'calle 21, 123, col. prueba' },
    { id: '4', name: 'Ubicacion 4', address: 'calle 21, 123, col. prueba' },
    { id: '5', name: 'Ubicacion 5', address: 'calle 21, 123, col. prueba' },
    { id: '6', name: 'Ubicacion 6', address: 'calle 21, 123, col. prueba' }
  ]

const { height, width } = Dimensions.get('window')

class Location extends React.Component {

    state = {
      favLocations: [],
      responseLocations:[],
      slectedLocation: null,
      searchAddress: '',  
      typing:false
    }
  
    async componentDidMount() {
     this.getFavlocations()
    }
    getFavlocations = async () => {
        const response = await getFavLocation()
        if (response.code === 200 ) {
            this.setState({favLocations:response.data})
        }
    }
    goBack = () => {
        const { navigation } =this.props
        navigation.goBack()
    } 

    goToMap = () => {
        const { navigation } = this.props
        navigation.navigate('Map')
    }

    onSearchInput = (value) => {
        if (value.length >= 4) {
           

            if (this.state.typingTimeout) {
            clearTimeout(this.state.typingTimeout);
            }
        
            this.setState({
                searchAddress: value,
                typing: false,
                typingTimeout: setTimeout(() => {
                    this.searchLocation(value)
                }, 2000)
            });
        } else {
            this.setState({searchAddress: value})
        }
    }
    searchLocation = async (value) => {
        
        const response = await autoComplete({value})
        if (response.code === 200){
            this.setState({ responseLocations: response.data})
        }
    }

    clearSearch = () => {
        // const { searchAddress, responseLocations } = this.state
        this.setState({searchAddress: '', responseLocations:[]})
    }

    onPressAutoComplete = (place) => {
        const { navigation } = this.props
        navigation.navigate('Map', { placeId: place.placeId })
    }

    selectFavLocation = async (item) => {
        const { navigation, udpateLocationData } = this.props
        const { 
            position,
            latitude,
            longitude,
            editableAddress,
            placeReference,
            placeTitle,
            placeId,
            id
        } = item
        const data = {
            position,
            latitude,
            longitude,
            editableAddress,
            placeReference,
            placeTitle,
            placeId,
            id
        } 
        const updatedDefaultLocation = await updateDefaultLocation({favLocationId:id})
        console.log('updated', updatedDefaultLocation);
        udpateLocationData(data)
        navigation.navigate('App')
    }
   
    render () {
        const {searchAddress, responseLocations, favLocations, typing} = this.state
        return (
            <SafeAreaView style={styles.container} >
                <View style={styles.header}>
                <TouchableOpacity
                onPress={this.goBack}>
                <Icon
                    name='chevron-left'
                    color={lightpurple}
                    size={50}
                />
                </TouchableOpacity>
                <Text style={styles.title}>Agrega o escoge una dirección</Text>
                </View>
                <View style={styles.body} >
                    <View style={styles.searchInputContainer}>
                        <IconIon
                        name='ios-search'
                        color={lightpurple}
                        size={25}
                        style={{alignSelf:'center'}}
                        />
                        <TextInput
                        value={searchAddress}
                        placeholder='Ingresa una dirección de entrega' 
                        placeholderTextColor='gray'
                        style={styles.searchInput}
                        onChangeText={(value) => {this.onSearchInput(value)}}
                        />
                        {
                            searchAddress.length > 0 ? 
                            <Button onPress={this.clearSearch} style={styles.cancelBtn}>
                                <Icon
                                    name='cancel'
                                    color={pinkish}
                                    size={25}
                                    style={{alignSelf:'center'}}
                                />
                            </Button>:null
                        }
                    </View>
                    <View style={styles.listContainer}>
                        {
                        responseLocations.length > 0?
                        <FlatList
                        data={responseLocations}
                        // refreshing={typing}
                        showsVerticalScrollIndicator={true}
                        keyExtractor={item => item.id}
                        ListHeaderComponent={() => {
                            return <Button onPress={this.goToMap} style={styles.listItem}>
                                <View style={styles.listHeaderIconContainer}>
                                    <Icon
                                        name='location-on'
                                        color='white'
                                        size={40}
                                        style={{alignSelf:'center'}}
                                    />
                                </View>
                                <Text style={styles.listItemHeaderTitle}>Ubicación actual</Text>
                            </Button>
                        }}
                        renderItem={({ item, index }) => {
                            return <Button onPress={ () =>{this.onPressAutoComplete(item)}} style={styles.listItem}>
                                <Icon
                                    name='location-on'
                                    color='gray'
                                    size={20}
                                    style={{alignSelf:'center'}}
                                />
                                <View style={styles.itemTextContainer}>
                                <Text style={styles.listItemTitle}>{item.title}</Text>
                                </View>
                            </Button>
                        }}
                        />
                        :<FlatList
                        data={this.state.favLocations}
                        showsVerticalScrollIndicator={true}
                        keyExtractor={item => item.id}
                        ListHeaderComponent={() => {
                            return <Button onPress={this.goToMap} style={styles.listItem}>
                                <View style={styles.listHeaderIconContainer}>
                                    <Icon
                                        name='location-on'
                                        color='white'
                                        size={40}
                                        style={{alignSelf:'center'}}
                                    />
                                </View>
                                <Text style={styles.listItemHeaderTitle}>Ubicación actual</Text>
                            </Button>
                        }}
                        renderItem={({ item, index }) => {
                            return <Button onPress={()=> {this.selectFavLocation(item)}} style={styles.listItem}>
                                <IconIon
                                    name='ios-star'
                                    color='gray'
                                    size={20}
                                    style={{alignSelf:'center'}}
                                />
                                <View style={styles.itemTextContainer}>
                                <Text style={styles.listItemTitle}>{item.placeTitle}</Text>
                                <Text style={styles.listItemSubTitle}>{item.editableAddress}</Text>
                                </View>
                            </Button>
                        }}

                        />
                        }
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: 'white',
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: 'grey',
        marginBottom: 10
    },
    buttonHeaderContianer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonHeader: {
        height: 35,
        width: 35,
        borderRadius: 35 / 2,
        elevation: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    title: {
        fontSize: 18,
        marginTop:10,
        marginLeft: 10
    },
    body:{
        backgroundColor:'transparent',
        flex:1,
        padding:20,
        
    },
    listContainer:{
        flex:1,
        marginTop:20
    },
    searchInputContainer: {
        backgroundColor: 'white',
        paddingHorizontal:10, 
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.36,
        shadowRadius: 6.68,
        elevation: 11,
        borderRadius: 20,
        flexDirection:"row",
        justifyContent:'flex-end'
    },
    searchInput:{
        height: 50,
        marginLeft: 10,
        width: width*0.75,  
    },
    listItem: {
        backgroundColor: 'transparent',
        height: 80,
        // justifyContent: 'center',
        flexDirection:"row",
        padding:10,
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: 'grey',
    },
    listHeaderIconContainer: {
        backgroundColor: lightpurple,
        justifyContent:'center',
        borderRadius:60/2,
        height: 60,
        width:60,
        marginRight:10
    },
    listItemHeaderTitle: {
        fontWeight:'bold',
        fontSize:15,
        alignSelf:'center'
    },
    listItemTitle: {
        fontWeight:'bold',
        fontSize:15,
    },
    listItemSubTitle: {
        fontWeight:'bold',
        fontSize:12,
        color:'gray'
    },
    itemTextContainer: {
        justifyContent: 'center',
        paddingHorizontal:10, 
    },
    cancelBtn: { 
        position:'absolute', 
        alignSelf:'center',
    }
  
    
});
  
const mapStateToProps = state => ({
})

const bindActions = dispatch => ({
    updateCartData: data => dispatch(updateCartData(data)),
    udpateLocationData: data => dispatch(udpateLocationData(data))

})

export default connect(mapStateToProps, bindActions)(Location)