import React, { Component } from 'react'
import { connect } from 'react-redux'
import { StyleSheet, View, Image, Text, Platform, BackHandler, StatusBar, ActivityIndicator, TouchableOpacity, Dimensions, TextInput, Alert } from 'react-native'
import Geolocation from 'react-native-geolocation-service'
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import Icon from 'react-native-vector-icons/MaterialIcons'
import { aspectRatio, radiusInKM, earthRadiusInKM, rad2deg, deg2rad, isX, debounced, APIKEY } from '../utils/global'
import { udpateLocationData } from '../reducers/location'
import { getCurrentUser, getPlaceDetails, saveFavoriteLocation, updateDefaultLocation } from '../parse/parse'
import { lightpurple } from '../utils/colors';

const { height, width } = Dimensions.get('window')

const initialRegion = {
  latitude: 20.9667,
  longitude: -89.6167,
  latitudeDelta: 0.0922,
  longitudeDelta: 0.0421
}

class Map extends Component {
  _mounted = false
  map = React.createRef()

  constructor(props) {
    super(props)
    this.state = {
      region: {
        latitude: 20.9667,
        longitude: -89.6167,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421
      },
      position: '',
      ready: true,
      loading: true,
      editableAddress:'',
      placeTitle:'',
      placeReference:''
    }

    this.handleRegionChange = debounced(1000, this.handleRegionChange)
  }

  componentDidMount () {

    // const { placeId } = this.props.navigation.state.params
    const placeId = this.props.navigation.state.params?this.props.navigation.state.params.placeId: null
    if (placeId) {
      this._mounted = true
      this.fetchPlaceDetails(placeId)
    } else {
      this._mounted = true
      this.getCurrentPosition()
    }
    
    
    if (Platform.OS == 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.handleGoBack);
    }
  }

  componentWillUnmount() {
    this._mounted = false
    BackHandler.removeEventListener('hardwareBackPress', this.handleGoBack);
  }

  fetchPlaceDetails = async (placeId) => {
    const response = await getPlaceDetails({placeId})
    if (response.code === 200 ) {
      const region = {
        latitude: response.data.location.lat,
        longitude: response.data.location.lng,
        latitudeDelta: 0.0016699317454111906,
        longitudeDelta: 0.0010058283805847168
      }
      this.setState({
        region,
        position: response.data.title,
        editableAddress: response.data.title,
        placeId: response.data.placeId,
        loading: false
      })
      this.setRegion(region)

    }
  }
  
  getCurrentPosition = async () => {
    Geolocation.getCurrentPosition(async position => {
      const { coords: { latitude, longitude } } = position
      const radiusInRad = radiusInKM / earthRadiusInKM
      const longitudeDelta = rad2deg(radiusInRad / Math.cos(deg2rad(parseFloat(latitude))))
      const latitudeDelta = aspectRatio * rad2deg(radiusInRad)
      const response = await fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${APIKEY}&sensor=true`)
      const result = await response.json()
      if (result.status !== 'OK') {
        this.setState({ position: 'Actualizando ubicación...' })
        return
      }
      const { results } = result
      const { formatted_address, place_id } = results[0]
      const region = {
        latitude,
        longitude,
        longitudeDelta,
        latitudeDelta
      }
      this.setState({
        region,
        position: formatted_address,
        editableAddress: formatted_address,
        loading: false,
        placeId: place_id
      })
      this.setRegion(region)
    })
  }

  handleGoBack = () => {
    const { navigation } = this.props
    navigation.goBack()
    return true
  }

  setRegion (region) {
    if(this._mounted) {
      this.map.current.animateToRegion(region, 10)
    }
  }

  handleRegionChange = async region => {
    this.setState({ loading: true })
    const { latitude, longitude } = region
    const response = await fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${APIKEY}&sensor=true`)
    const result = await response.json()
    if (result.status !== 'OK') {
      this.setState({ position: 'Actualizando ubicación...' })
      return
    }
    const { results } = result
    console.log('results',results[0])
    const { formatted_address, place_id } = results[0]
    this.setState({
      region,
      position: formatted_address,
      editableAddress: formatted_address,
      loading: false,
      placeId: place_id
    })
  }

  handleChange = region => {
    this.setState({ position: 'Actualizando ubicación...', loading: true })
  }

  handleConfirmLocationByMap = async () => {
    const { navigation, udpateLocationData } = this.props
    const { position, region,placeReference,placeTitle, editableAddress, placeId } = this.state
    const { latitude, longitude } = region
    if (placeReference === '' || placeTitle === '' || editableAddress === '') {
      Alert.alert('Error al guardar la ubicación', 'favor de llenar todos los datos.')
      return 
    }
    const data = {
      position,
      latitude,
      longitude,
      editableAddress,
      placeReference,
      placeTitle,
      placeId
    }
    const loggedIn = await getCurrentUser()
    if (!loggedIn) {
      udpateLocationData(data)
      navigation.navigate('App')
      return
    }
    const response = await saveFavoriteLocation({data})
    if (response.code === 200) {
      // const updatedDefaultLocation = await updateDefaultLocation({favLocationId:id})
      udpateLocationData(response.data)
      navigation.navigate('App')
      return
    } else {
      Alert.alert('Error al guardar la ubicación', 'favor de verificar que los datos esten correctos.')
    }
  }

  render() {
    const { position, loading } = this.state
    console.log('state', this.state)
    return <View style={styles.container}>
      <StatusBar backgroundColor='white' barStyle='dark-content'/>
      <MapView
        ref={this.map}
        provider={PROVIDER_GOOGLE}
        // showsUserLocation
        style={styles.map}
        onRegionChangeComplete={this.handleRegionChange}
        onRegionChange={this.handleChange}
        initialRegion={initialRegion}
      />
      <View style={styles.header}>
        <TouchableOpacity
          onPress={this.handleGoBack}
          style={styles.buttonBack}>
          <Icon
            name='arrow-back'
            color={lightpurple} size={30} />
        </TouchableOpacity>
        <View style={{ flex: 1, marginLeft: 10 }}>
          <Text style={{ color: 'black', fontWeight: 'bold' }}>Ubicacion actual</Text>
          <Text
            numberOfLines={1}
            ellipsizeMode='tail'
            style={{ color: 'black' }}>
            { position }
          </Text>
        </View>
      </View>
      <View
        pointerEvents='none'
        style={styles.markerContainer}>
        <Image
          style={styles.marker}
          resizeMode='contain'
          source={require('../assets/marker.png')}
        />
      </View>
      <View style={styles.bottom}>
        <TouchableOpacity
          onPress={this.getCurrentPosition}
          style={styles.buttonCurrentLocation}>
          <Icon
            name='my-location'
            color='black'
            size={30} 
          />
        </TouchableOpacity>
        <View style={styles.containerEditable}>
          <Text style={styles.label}>Editar Dirección</Text>
          <TextInput
            value={this.state.editableAddress}
            placeholder='Dirección editable'
            onChangeText={(editableAddress) => {this.setState({editableAddress})}}
          />
          <Text style={styles.label}>Referencias</Text>
          <TextInput
            value={this.state.placeReference}
            placeholder='Alguna referencia para facilitar ubicar el lugar'
            onChangeText={(placeReference) => {this.setState({placeReference})}}
          />
          <Text style={styles.label}>Titulo</Text>
          <TextInput
            value={this.state.placeTitle}
            placeholder='Nombre con el que se guardara esta ubicación'
            onChangeText={(placeTitle) => {this.setState({placeTitle})}}
          />
        
        </View>
        <TouchableOpacity
          disabled={loading}
          onPress={this.handleConfirmLocationByMap}
          style={styles.buttonOrder}>
          {
            loading
              ? <ActivityIndicator
                size='small'
                color='white'
              />
              : <Text style={{ color: 'white' }}>Confirmar</Text>
          }
        </TouchableOpacity>
      </View>
    </View>
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  map: {
    ...StyleSheet.absoluteFillObject

  },
  header: {
    position: 'absolute',
    top: Platform.OS === 'android' ? 10 : isX ? 45 : 30,
    height: 50,
    width: '97%',
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    marginHorizontal: 5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4
  },
  buttonBack: {
    flex: 0.1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  markerContainer: {
    left: '50%',
    marginLeft: -15,
    position: 'absolute',
    top: '50%',
    zIndex: 2,
    height: 30,
    width: 30
  },
  buttonOrder: {
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: lightpurple,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.27,
    shadowRadius: 4.65,
    elevation: 6
  },
  bottom: {
    position: 'absolute',
    bottom: Platform.OS === 'android' ? 10 : isX ? 40 : 10,
    left: 5,
    right: 5
  },
  buttonCurrentLocation: {
    alignSelf: 'flex-end',
    marginBottom: 15
  },
  marker: {
    width: 30,
    height: 30,
    marginTop: Platform.OS === 'android' ? -30 : isX ? -22 : -17
  },
  containerEditable: {
    backgroundColor: 'white',
    paddingHorizontal:10, 
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 5,
    },
    shadowOpacity: 0.36,
    shadowRadius: 6.68,
    elevation: 11,
    backgroundColor: 'white', 
    height:height/4.5, 
    marginBottom:10, 
    borderRadius:5, 
    padding:5, 
    justifyContent:'space-evenly'
  },
  label: {
    fontWeight:'bold'
  }
})

const mapStateToProps = state => ({
  location: state.location.location
})

const bindActions = dispatch => ({
  udpateLocationData: data => dispatch(udpateLocationData(data))
})

export default connect(mapStateToProps, bindActions)(Map)
