import React from 'react'
import {
  Image,
  StyleSheet,
  Dimensions,
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Platform,
  Alert,
} from 'react-native'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/MaterialIcons'
// import IconIon from 'react-native-vector-icons/Ionicons'
import QuantityInput from '../components/quantityInput'
import {formatPrice} from '../utils/format'
import {updateCartData, resetCartData, cleanStoreData } from '../reducers/shoppingCart'
import { storeData,  } from '../parse/parse'
import { lightpurple, pinkish, secondaryColor } from '../utils/colors'


const { height, width } = Dimensions.get('window')

class ShoppingCart extends React.Component {

    state = {
        cartItems: [],
        total:0.00,
    }
  
    async componentDidMount() {
        const { cartItems } = this.state
        const { cart, store } = this.props
        const data = await storeData({ storeId:store })

        console.log('store', data)
        if (cartItems !== cart) {
            const total = this.updateTotal(cart)
            this.setState({
                cartItems: cart,
                total: data.deliveryPrice ? total + data.deliveryPrice : total,
                deliveryPrice: data.deliveryPrice || null,
                storeName: data.name
            })
        }
    }

    updateTotal = (items) => {
        var total = 0
        items.map( item => {
            const cost = item.extraTotal ? (item.price * item.quantity) + (item.extraTotal * item.quantity) : (item.price * item.quantity);
            total = total + cost;
        })
        return total
    }

    goBack = () => {
        const { navigation } =this.props
        navigation.goBack()
    }

    handleOnChangeQuantity = ({value, index}) => {
        const { updateCartData } = this.props;
        var list = this.state.cartItems
        list[index].quantity = value;
        const total = this.updateTotal(list);
        updateCartData(list);
        this.setState({cartItems:list, total});
    }

    goToCheckout = async () => {
        const { total } = this.state
        const { navigation , store } = this.props
        const data = await storeData({ storeId:store })
        console.log(data)
        console.log(total)
        if (total >= data.minimumOrder ) {
            navigation.navigate('Checkout')
        } else {
            Alert.alert('Monto minimo',`${data.name} tiene una cantidad minima para realizar el pedido de $${data.minimumOrder}`)
        }
        
    }

    validateCartContent = () =>{
        const { cartItems } = this.state
        if (cartItems.length === 0) {
            return false
        }
        return true 
    }

    handleConfirmDeleteItem = index => {
        Alert.alert(
            "Mi Canastita",
            "Esta seguro que desea eliminar este articulo de la canastita?",
            [
                {
                    text: "Cancelar",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { 
                    text: "Eliminar",
                    onPress: () => this.handleDeleteItem(index)
                }
            ], { cancelable: false }
        )
    }

    handleDeleteItem = index => {
        const { updateCartData } = this.props
        let list = this.state.cartItems
        list.splice(index, 1)
        updateCartData(list)
        const total = this.updateTotal(list)
        this.setState({ cartItems: list, total })
    }
    
    emptyShoppingCart = async  () => {
        Alert.alert(
            "Mi carrito",
            "Esta seguro que desea eliminar los articulos del carrito?",
            [
                {
                    text: "Cancelar",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { 
                    text: "Vaciar",
                    onPress: async () => {
                        

                        this.props.resetCartData();
                        this.props.cleanStoreData();
                        console.log('this.props', this.props)
                        
                        const { cartItems } = this.state
                        const { cart, store } = this.props
                        const data = await storeData({ storeId:store })

                        console.log('store', data)
                        if (cartItems !== cart) {
                            const total = this.updateTotal(cart)
                            this.setState({
                                cartItems: cart,
                                total: data.deliveryPrice ? total + data.deliveryPrice : total,
                                deliveryPrice: data.deliveryPrice || null,
                                storeName: data.name
                            })
                        }
                    }
                }
            ], { cancelable: false }
        )
        
    }

    handleNavigateStore = storeId => {
        const { navigation } = this.props
        navigation.navigate('Store', { storeId })
    }

    render() {
        console.log(this.props)
        const valid = this.validateCartContent()
        const { store } = this.props
        const { cartItems, storeName } = this.state

        return (
            <SafeAreaView style={styles.container}>
                {/* <View style={styles.header}>
                    <TouchableOpacity
                        onPress={this.goBack}>
                        <Icon
                            name='chevron-left'
                            color={lightpurple}
                            size={50}
                        />
                    </TouchableOpacity>
                    <Text style={styles.storeTitle}>Mi canastita</Text>
                </View> */}
                <View style={styles.headerContainer}>
                    <TouchableOpacity style={styles.btnContainer} onPress={this.goBack}>
                        <Image
                            source={require('../assets/flecha_back_white.png')}
                            style={{
                                width:30, 
                                height:30,
                                alignSelf:'center'
                            }}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.body} >
                    <View style={styles.cartContainer} >
                    <View style={{ flexDirection: 'row', justifyContent: valid?'space-between':'center', paddingHorizontal: 5}}>
                        <Text style={styles.title}>
                            { valid ? 'Tu Carrito' : 'La canastita esta vacia' }
                        </Text>
                        { valid?
                            <Text style={{ color:'gray', alignSelf:'flex-end'}}>{ storeName }</Text>
                            :null
                        }
                    </View>
                    
                    <View style={styles.listContainer}>
                        <FlatList
                            data={cartItems}
                            showsVerticalScrollIndicator={true}
                            keyExtractor={item => item.id}
                            ListHeaderComponent={() => {
                                return cartItems.length > 0?
                                <TouchableOpacity onPress={() => this.handleNavigateStore(store)} style={styles.btnList}>
                                    <Text style={{ color: lightpurple, alignSelf:'center'}}>+ Agregar más productos</Text>
                                </TouchableOpacity> : null
                            }}
                            renderItem={({ item, index }) => (
                                <View style={styles.cartItem}>
                                    <View style={{flexDirection:'row'}}>
                                        <View style={styles.itemTextContainer}> 
                                            <Text style={styles.itemName}>{item.name}</Text>
                                        </View>
                                        <View style={styles.itemTextContainer}> 
                                            <Text style={styles.itemPrice}>{`$${item.extraTotal?(item.price + item.extraTotal).toFixed(2) :item.price.toFixed(2) }`}</Text>
                                        </View>
                                        <View style={styles.quantityContainer}>
                                            <QuantityInput
                                                value={item.quantity}
                                                onChange={(value) => this.handleOnChangeQuantity({value, index})}
                                                totalWidth={100}
                                                totalHeight={25}
                                                rounded={true}
                                                minValue={1}
                                                maxValue={100}
                                                onLimitReached={(isMAx, msg) =>{ 
                                                    if (msg === 'Reached Minimum Value!') this.handleConfirmDeleteItem(index) 
                                                }}
                                                step={1}
                                                iconStyle={{ fontSize: 15, color: 'white' }}
                                                inputStyle={{ fontSize: 18, color: '#434A5E' }}
                                                valueType='real'
                                                borderColor={lightpurple}
                                                rightButtonBackgroundColor={lightpurple}
                                                leftButtonBackgroundColor={lightpurple}
                                                textColor='white'
                                                editable={false}
                                            />
                                        </View>
                                    </View>
                                    <View style={{paddingHorizontal: 10}}>
                                        <Text style={{color:'gray'}}>{item.description}</Text>
                                    </View>
                                </View>
                            )}
                            ListFooterComponent={ () => {
                                return cartItems.length > 0?
                                <TouchableOpacity onPress={this.emptyShoppingCart} style={styles.btnList}>
                                    <Text style={{ color: lightpurple, alignSelf:'center'}}>Vaciar carrito</Text>
                                </TouchableOpacity> : null
                            }}
                        />
                        <View style={{borderBottomWidth: StyleSheet.hairlineWidth, marginHorizontal:20}} />
                        <View style={{ width:'100%', height:60, }}>
                                <TouchableOpacity 
                                disabled={!valid}
                                onPress={this.goToCheckout} 
                                style={styles.btnPay}>
                                    <Text style={{ color: 'white', alignSelf:'center'}}>Ir a pagar</Text>
                                </TouchableOpacity>
                        </View>
                    </View>
                    
                </View>
                
                </View>
                {/* { this.state.deliveryPrice &&
                    <View style={{backgroundColor:'orange'}}>
                        <Text style={styles.payText}>{`costo de envio : $${formatPrice(this.state.deliveryPrice)}`}</Text>
                    </View>
                }
                <View style={styles.footer}>
                    <TouchableOpacity 
                        disabled={!valid}
                        onPress={this.goToCheckout} 
                        style={styles.payBtn}>
                        <Text style={styles.payText}>Ir a Pagar</Text>
                    </TouchableOpacity>
                    <View style={styles.totalContainer}>
                        <Text style={styles.payText}>{`$${formatPrice(this.state.total)}`}</Text>
                    </View>
                </View> */}
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: 'white',
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: 'grey',
        marginBottom: 10
    },
    buttonHeaderContianer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonHeader: {
        height: 35,
        width: 35,
        borderRadius: 35 / 2,
        elevation: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    title: {
        fontSize: 18,
        marginTop:10,
        marginLeft: 10, 
        alignSelf:'flex-end'
    },
    body:{
        backgroundColor:'transparent',
        flex:1,
        paddingHorizontal:15,
        paddingTop:20
    },
    listContainer:{
        flex:1,
        marginTop:20, 
    },
    cartItem:{
        height:60,
        backgroundColor:'white',
        // flexDirection:'row',
        paddingVertical: 5,
        elevation:1,
        marginBottom:1,
        paddingHorizontal:10,
        justifyContent: 'space-between',
        marginTop:10
    },
    itemName:{
        color: pinkish,
        alignSelf:'flex-start',
        fontWeight:'bold'
    },
    itemPrice:{
        alignSelf:'center',
    },
    quantityContainer:{
        justifyContent:'center',
        width: '33%'
    },
    itemTextContainer:{
        width: '33%',
        justifyContent:'center'
    },
    footer: {
        backgroundColor: 'transparent',
        height:height * 0.10,
        flexDirection:'row',
    },
    payBtn:{
        height:"100%",
        width:width/1.5,
        backgroundColor: lightpurple , 
        elevation:5, 
        borderRightColor: StyleSheet.hairlineWidth,
        borderWidth:0.5,
        justifyContent: 'center',
        borderColor: secondaryColor
    },
    payText:{
        color:'white',
        fontWeight:'bold',
        fontSize:18, 
        alignSelf:'center'
    },
    totalContainer: {
        flex:1,
        backgroundColor:lightpurple, 
        borderWidth:0.5,
        justifyContent: 'center',
        borderColor: secondaryColor
    },
    storeTitle: {
        fontSize: 18,
        color: 'black'
    },
    cartContainer: {
        flex:1,
        minWidth: '100%',
        backgroundColor: 'white',
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        marginVertical: 20,
        borderRadius: 10
    },
    btnList: {
        backgroundColor: 'white',
        borderRadius: 30,
        width: '70%',
        paddingVertical: 5,
        borderColor: lightpurple,
        borderWidth:1,
        marginTop: 10,
        alignSelf: 'center',
        justifyContent: 'center'
    },
    btnPay: {
        backgroundColor: lightpurple,
        borderRadius: 30,
        width: '70%',
        paddingVertical: 5,
        marginTop: 10,
        alignSelf: 'center',
        justifyContent: 'center'
    },
    btnContainer: { 
        width: 50, 
        height: 50, 
        backgroundColor: lightpurple,
        borderRadius:25,
        justifyContent:'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.18,
        shadowRadius: 1.00,
        elevation: 1,
    },
    headerContainer: {
        width: width, 
        position: 'absolute', 
        flexDirection: 'row', 
        backgroundColor: 'transparent', 
        paddingTop: Platform.OS === 'ios' ? 40 : 15, 
        paddingHorizontal: 20, 
        justifyContent: 'space-between', 
        zIndex:1
    }, 
})
  
const mapStateToProps = state => ({
    cart: state.shoppingCart.cart,
    store: state.shoppingCart.store
})

const bindActions = dispatch => ({
    updateCartData: data => dispatch(updateCartData(data)),
    resetCartData: () => dispatch(resetCartData()),
    cleanStoreData: () => dispatch(cleanStoreData())
})

export default connect(mapStateToProps, bindActions)(ShoppingCart)
