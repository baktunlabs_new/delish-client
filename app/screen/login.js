import * as React from 'react';
import { 
  Text, 
  TextInput, 
  View, 
  Image, 
  ImageBackground, 
  StyleSheet, 
  Dimensions, 
  TouchableOpacity, 
  SafeAreaView, 
  StatusBar, 
  Platform, 
  Alert,
  Modal
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { userLogIn, resetPassword, getCurrentUser} from '../parse/parse';
import { connect } from 'react-redux'
import ProgressDialog from '../components/progressDialog'
import { primaryColor, lightpurple, pinkish } from '../utils/colors';
import OneSignal from 'react-native-onesignal'

const { height, width } = Dimensions.get('window');
const MyStatusBar = ({backgroundColor, ...props}) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

class LoginScreen extends React.Component{
  static navigationOptions = {
    header: null
  }

  state = {
    email: '',
    password: '',
    user: null,
    loggedIn: false,
    progressVisible:false,
    progressVisibleResetPW: false,
    showModal:false
  }
  
  handleResetPW = async () => {
    const { emailModal } = this.state
    const resp = await resetPassword({ email: emailModal })
    this.setState({ progressVisibleResetPW: true })

    if (resp.code === 200) {
      this.setState({ progressVisibleResetPW: false, showModal: false, emailModal: '' })
      Alert.alert('Cambiar Contraseña', resp.msg);
    } else {
      this.setState({ progressVisibleResetPW: false, showModal: false, emailModal: '' })
      Alert.alert('Cambiar Contraseña', 'Error al enviar el correo para cambiar la contraseña');
    }
  }

  handleLogIn = async () => {
    // const { updateSession } = this.props
    this.setState({progressVisible:true})
    if (this.state.email.trim() === '' || this.state.password.trim() === '') {
      Alert.alert('', 'Ingrese un correo o una contraseña')
      this.setState({progressVisible:false})
      return
    }
    const response = await userLogIn({ email: this.state.email , password: this.state.password })
    if (response.code === 200) {
      const currentUser = await getCurrentUser()
      setTimeout(() => {
        this.setState({ progressVisible: false })
          
          const email = currentUser.get('email') ? currentUser.get('email') : currentUser.get('username')
          OneSignal.sendTag('email', email)
          const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'App' })],
          })
          this.props.navigation.dispatch(resetAction);
        
        
      }, 1000)

    } else {
      if (response.code  === 101) {
        Alert.alert('Error', 'Correo o contraseña invalida.')
        this.setState({ progressVisible: false })
        return false
      } else {
        this.setState({ erroUser: true, loading: false, progressVisible: false })
        Alert.alert('Error al iniciar sesion','por favor cominiquese con soporte tecnico')
      }
    }
  }

  handleAppTour = () => {
    setTimeout(() => {
      this.setState({ progressVisible: false })
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'App' })],
      })
      this.props.navigation.dispatch(resetAction);
    }, 1000)
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: "#000000" }}>
        <ImageBackground source={require('../assets/bg_02.png')} style={{ width: '100%', height: '100%' }}>
          <View style={{ flex: 1, backgroundColor: "rgba(0, 0, 0, 0.5)" }}>
            <View>
              <MyStatusBar barStyle="light-content" />
            </View>
            <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center', marginBottom: 30 }}>
              <View style={{ flexDirection: 'row' }}>
                <Image
                  resizeMode='stretch'
                  style={{
                    backgroundColor: 'transparent',
                    width: 200,
                    height: 80,
                    justifyContent: 'center',
                    alignContent: 'center',
                  }}
                  source={require('../assets/text_01.png')} />
              </View>
              <Text style={{ fontSize: 23, fontWeight: 'bold', marginBottom: 15, color: lightpurple }}>Bienvenido</Text>
            </View>
            <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
              <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingHorizontal: 20, height: 45, width: "100%" }}>
                <Image source={require('../assets/icon_mail.png')} style={{ width: 20, height: 20, resizeMode: 'contain', marginRight: 5 }} />
                <TextInput
                  placeholder="Correo"
                  placeholderTextColor="white"
                  autoCapitalize={"none"}
                  onChangeText={email => this.setState({ email })}
                  keyboardType='email-address'
                  style={styles.textUsuario} />
              </View>

            </View>
            <View style={{ marginTop: 25, justifyContent: 'center', alignItems: 'center', marginBottom: 10 }}>
              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 20, height: 45, width: "100%" }}>
                <Image source={require('../assets/icon_pass.png')} style={{ width: 20, height: 20, resizeMode: 'contain', marginRight: 5 }} />
                <TextInput
                  placeholder="Contraseña"
                  placeholderTextColor="white"
                  secureTextEntry={true}
                  autoCapitalize={"none"}
                  onChangeText={password => this.setState({ password })}
                  style={styles.textUsuario}
                />
              </View>
            </View>
            <View style={{ marginTop: 10, marginBottom: 20 }}>
              <TouchableOpacity onPress={() => { this.setState({ showModal: true }) }} style={styles.containerRegister}>
                <Text style={{ color: 'white', fontSize: 10 }} >¿Olvidaste tu contraseña?
              <Text style={{ color: pinkish, fontWeight: 'bold', fontSize: 12 }}> Cámbiala aquí</Text>
                </Text>
              </TouchableOpacity>
            </View>
            <View style={{ flex: 1, alignItems: 'center', width: '100%', marginBottom: 70 }}>
              <View style={{ justifyContent: "center", width: '80%', alignContent: "center" }}>
                <TouchableOpacity style={styles.linearGradient} onPress={this.handleLogIn}>
                  <Text style={styles.buttonText}>INICIA SESIÓN</Text>
                </TouchableOpacity>
              </View>
              <View style={{ marginTop: 10, marginBottom: 20 }}>
                <TouchableOpacity onPress={() => { this.props.navigation.navigate('Register') }} style={styles.containerRegister}>
                  <Text style={{ color: pinkish, fontSize: 15, }}>  Regístrate en Delish</Text>
                </TouchableOpacity>
              </View>
            </View>
            <TouchableOpacity style={styles.linearGradient} onPress={this.handleAppTour}>
              <Text style={styles.buttonText}>EXPLORA DELISH AHORA</Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>

        <ProgressDialog
          visible={this.state.progressVisible}
          title="Iniciando sesion"
          message="Espere, porfavor ..."
          activityIndicatorColor={pinkish}
        />
        <ProgressDialog
          visible={this.state.progressVisibleResetPW}
          title="Enviado correo"
          message="Espere, porfavor ..."
          activityIndicatorColor={pinkish}
        />
        {
          /**
          MODAL ResetPassword
            */
        }
        <Modal onRequestClose={() => { this.setState({ showModal: false }) }} transparent={true} visible={this.state.showModal}>
          <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.40)', justifyContent: "center" }}>
            <View style={{ justifyContent: "space-evenly", width: width * 0.80, height: height * 0.40, borderRadius: 10, backgroundColor: "white", alignSelf: "center", padding: 10 }}>
              <Text style={{ textAlign: "center", fontWeight: "bold" }} >Proporcione el correo electronico correspondiente a la cuenta:</Text>
              <View style={{ flexDirection: 'row', justifyContent: "center", paddingHorizontal: 10, }} >

                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingHorizontal: 20, height: 45, width: "100%" }}>
                  <Image source={require('../assets/icon_mail.png')} style={{ tintColor: primaryColor, width: 20, height: 20, resizeMode: 'contain', marginRight: 5 }} />
                  {/* <View style={{borderRadius: 10, borderWidth: 1,  overflow: 'hidden'}} >  */}
                  <TextInput
                    placeholder="Correo"
                    placeholderTextColor="black"
                    // underlineColorAndroid="#ffa100"
                    autoCapitalize={"none"}
                    onChangeText={emailModal => this.setState({ emailModal })}
                    style={styles.inputModal} />
                  {/* </View> */}
                </View>
              </View>
              <View style={{ flexDirection: 'row', justifyContent: "space-evenly" }} >
                <TouchableOpacity onPress={() => { this.setState({ showModal: false }) }} style={{ marginTop: 20, backgroundColor: lightpurple, height: 40, width: '40%', alignItems: 'center', borderRadius: 5, justifyContent: "center", }} >
                  <Text style={{ fontSize: 18, fontFamily: 'Gill Sans', textAlign: 'center', margin: 4, color: 'white', backgroundColor: 'transparent', }}>
                    Cancelar
                        </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => { this.handleResetPW() }} style={{ marginTop: 20, backgroundColor: lightpurple, height: 40, width: '40%', alignItems: 'center', borderRadius: 5, justifyContent: "center", }} >
                  <Text style={{ fontSize: 18, fontFamily: 'Gill Sans', textAlign: 'center', margin: 4, color: 'white', backgroundColor: 'transparent', }}>
                    Enviar
                        </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </SafeAreaView>
    );
  }

}

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch'

  },
  containerSignIn: {
    width: '80%',
    height: 80,
    flexDirection: 'row',
    justifyContent: 'center',
    marginLeft: '10%',
    marginRight: '6%',
  },
  containerUserName: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    marginLeft: '6%',
    marginRight: '6%',
  },
  containerPassword: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    marginLeft: '6%',
    marginRight: '6%',
  },
  containerRegister: {
    marginLeft: '6%',
    marginRight: '6%',
    alignItems: 'center',
    marginTop: 5
  },
  icon: {
    flex: 1
  },
  textInput: {
    backgroundColor: 'transparent',
    flex: 5,
    color: 'black',
    paddingLeft: '25%'
  }, containerTitulos: {
    height: 60,
    marginLeft: '6%',
    marginRight: '6%',
    alignItems: 'center',
  },
  textUsuario: {
    //fontSize: 16,
    //marginLeft: 25,
    borderBottomColor: lightpurple,
    borderBottomWidth: 1,
    width: "70%",
    height: "90%",
    color: 'white',
    fontWeight: "bold",
    backgroundColor: 'transparent',
    borderRadius: 6,
    paddingHorizontal: 10

  }, textField: {
    fontSize: 16,
    //marginLeft: -25,
    borderWidth: 0,
    width: "70%",
    height: "100%",
    color: 'black',
    backgroundColor: 'white',
    borderRadius: 12
  },
  linearGradient: {
    marginTop: height * 0.12,
    backgroundColor: lightpurple,
    height: 50,
    width: '100%',
    alignItems: 'center',
    borderRadius: 5,
    justifyContent: "center"
  },
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 4,
    color: 'white',
    backgroundColor: 'transparent',
    // fontWeight: "bold"
  },
  imgContainer2: {
    backgroundColor: "transparent",
    width: width,
    height: 10
  },
  imgContainer: {
    backgroundColor: "transparent",
    marginTop: 10,
    width: 50,
    height: 50,
    marginBottom: 10
  },
  lineStyle: {
    width: '90%',
    borderWidth: 1,
    borderColor: '#E72A7B',
    margin: 10,
  },
  boton: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 23
  },

  statusBar: {
    height: STATUSBAR_HEIGHT,
  },
  appBar: {
    backgroundColor: '#79B45D',
    height: APPBAR_HEIGHT,
  },
  content: {
    flex: 1,
    backgroundColor: '#33373B',
  },
  inputModal: {
    //fontSize: 16,
    //marginLeft: 25,
    alignSelf: "center",
    borderWidth: 1,
    width: "90%",
    height: "90%",
    color: 'black',
    backgroundColor: 'white',
    borderRadius: 6,
    borderColor: 'black',
    paddingHorizontal: 10

  },


})

const bindActions = dispatch => ({
})

export default connect(null, bindActions)(LoginScreen)