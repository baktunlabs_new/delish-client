import React from 'react'
import {
  StyleSheet,
  Dimensions,
  SafeAreaView,
  View,
  Text,
  BackHandler,
  AppState,
  Platform
} from 'react-native'
import { StackActions, NavigationActions } from 'react-navigation';
import { connect } from 'react-redux'
import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions'
import { subscribeListOrders, getOrdersByBusiness } from '../../parse/parse'
import IconM from 'react-native-vector-icons/MaterialIcons'
import IconF from 'react-native-vector-icons/FontAwesome5'
import { lightpurple } from '../../utils/colors'
/** components */
import OrderList from '../../components/OrderList'
import  Menu  from "../../components/Menu";
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import ModalStatusPicker from "../../components/ModalStatus";

const { height, width } = Dimensions.get('window')
// const orders = [
//     { id: '1', status: 'accepted', address: 'Calle 34 251, San Ramón Nte, 97117 Mérida, Yuc., México', createdAt: '2021-01-23T00:27:43.683Z', client: 'Rodrigo Canto', total: 5000.00 },
//     { id: '2', status: 'finished', address: 'Calle 34 251, San Ramón Nte, 97117 Mérida, Yuc., México', createdAt: '2021-01-23T00:27:43.683Z', client: 'Rodrigo Canto', total: 500.00 },
//     { id: '3', status: 'canceled', address: 'Calle 34 251, San Ramón Nte, 97117 Mérida, Yuc., México', createdAt: '2021-01-23T00:27:43.683Z', client: 'Rodrigo Canto', total: 500.00 },
//     { id: '4', status: 'inDelivery', address: 'Calle 34 251, San Ramón Nte, 97117 Mérida, Yuc., México', createdAt: '2021-01-23T00:27:43.683Z', client: 'Rodrigo Canto', total: 500.00 },
//     { id: '5', status: 'created', address: 'Calle 34 251, San Ramón Nte, 97117 Mérida, Yuc., México', createdAt: '2021-01-23T00:27:43.683Z', client: 'Rodrigo Canto', total: 500.00 },
//     { id: '6', status: 'arrived', address: 'Calle 34 251, San Ramón Nte, 97117 Mérida, Yuc., México', createdAt: '2021-01-23T00:27:43.683Z', client: 'Rodrigo Canto', total: 500.00 },
// ]


class RestaurantOrdersScreen extends React.Component {
  _subscriptionOderList = null
  state = {
    orders: [],
    appState: AppState.currentState,
    showPicker: false,
    selectedStatus: null,
    selectedVal: null

  }

  async componentDidMount() {
    const { navigation, businessId } = this.props
    const { selectedVal } = this.state
    const storeId = businessId
    console.log('storeId', storeId)
    const resp = await getOrdersByBusiness({storeId, status: selectedVal})
    console.log('resp1', resp)
    if (resp.code === 200) {
        const orders = resp.data
        this.setState({ orders })
    }
    this._subscriptionOderList = await subscribeListOrders({storeId})
    this._subscriptionOderList.on('open', () => {})
    this._subscriptionOderList.on('update', async (object) => {
        // const stringObject = JSON.stringify(object)
        // const jsonResp = JSON.parse(stringObject)
        const { selectedVal } = this.state
        console.log('resp2', object)
        const resp = await getOrdersByBusiness({storeId, status:selectedVal})
        console.log('resp1', resp)
        if (resp.code === 200) {
            const orders = resp.data
            this.setState({ orders })
        }
        // const data = object.map((order)=> {
        //   return {
        //     id: order.id,
        //     createdAt: order.get('createdAt'),
        //     updatedAt: order.get('updatedAt'),
        //     total: order.get('total'),
        //     status: order.get('status'),
        //     paymentMethod: order.get('paymentMethod'),
        //     address: order.get('address'),
        //     client: `${order.get('user').get('name')} ${order.get('user').get('lastName')}`
        //   }
        // })
        // this.setState({ orders: data})
    })
    AppState.addEventListener("change", this._handleAppStateChange);
    if (Platform.OS == 'android') {
        BackHandler.addEventListener('hardwareBackPress', this.handlebackPress);
      }
}

async componentWillUnmount () {
    if (this._subscriptionOderList) {
        this._subscriptionOderList.on('close', () => {})
        this._subscriptionOderList.on('stop', () => {})
        this._subscriptionOderList.unsubscribe();
    }
    AppState.removeEventListener("change", this._handleAppStateChange);
    BackHandler.removeEventListener('hardwareBackPress', this.handlebackPress);

}

_handleAppStateChange = async (nextAppState) => {
    if (this.state.appState.match(/inactive|background/) && nextAppState === "active") {
      const { businessId } = this.props
      const { selectedVal } = this.state
      console.log('selectedStatus', selectedVal)
      const storeId = businessId
      const resp = await getOrdersByBusiness({ storeId, status: selectedVal})
      console.log('resp3', resp)

      if (resp.code === 200) {
          const orders = resp.data
          this.setState({ orders })
      }
    }
    this.setState({ appState: nextAppState });
}

onSelectedItem = async (item) => {
  const { businessId } = this.props
  const storeId = businessId
  const resp = await getOrdersByBusiness({storeId, status: item.value})
  console.log('resp1', resp)
  if (resp.code === 200) {
      const orders = resp.data
      this.setState({ orders, showPicker: false, selectedStatus: item.label, selectedVal: item.value })
      return
  } else {
      this.setState({showPicker:false})
   }

}

ShowPicker = () => {
  this.setState({ showPicker: true})
}

goToOrderDetail = orderId => {
  const { navigation,  } = this.props
  // const storeId = navigation.getParam('storeId')
  navigation.navigate('RestaurantOrderDetail', { orderId })
}

render() {
    const { orders, showPicker, selectedStatus } = this.state
    return (
      <SafeAreaView style={styles.container} >
        <View style={{flex:1, padding: 20}}>
            <View style={styles.elevatedContainer}>
                
                {/* SEARCH CONTAINER */}
                <View style={styles.searchContainer}>
                    {/* <View style={{justifyContent:'center'}}>
                        <Text style={styles.label}>Búsqueda</Text>
                        <View style={{borderBottomColor:'gray', borderBottomWidth:1, flexDirection:'row'}}>
                        <TextInput
                            placeholder='Búsqueda por nombre.'
                            placeholderTextColor='gray'
                            // style={{fontSize:12}}
                            style={{color:'black'}}
                        />
                        <IconF
                            name='search'
                            color={lightpurple}
                        />
                        </View>
                    </View> */}
                    <Text style={{fontSize:17, fontWeight:'bold', color:'black', alignSelf:'center'}}>Listado de Órdenes</Text>
                    <TouchableOpacity onPress={this.ShowPicker}  style={{justifyContent:'center'}}>
                        <Text style={styles.label}>Filtrar por</Text>
                        <View style={{borderBottomColor:'gray', borderBottomWidth:1, flexDirection:'row'}}>
                          <TextInput
                              value={selectedStatus}
                              placeholder='Elige el estatus.'
                              placeholderTextColor='gray'
                              editable={false}
                              style={{color:'black'}}
                          />
                          <IconM
                              name='keyboard-arrow-down'
                              color={lightpurple}
                              style={{alignSelf:'center'}}
                              size={20}
                          />
                        </View>
                    </TouchableOpacity>
                </View>
                {/* COLORS */}
                {/* <View style={styles.VisibleContainer}>
                    <Text style={styles.label}>Visilidad: </Text>

                </View> */}
                {/* LISTA DE ORDENES  */}
                <OrderList 
                    data={orders}
                    onPress={this.goToOrderDetail}
                />
            </View>
        </View>
       <Menu
            current='Orders'
            navigation={this.props.navigation}
        />
        <ModalStatusPicker
          modalVisible={showPicker}
          onSelect={this.onSelectedItem}
          onRequestClose={() => { this.setState({ showPicker:false })}}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: { 
    flex: 1,
    backgroundColor: 'white'
  },
  elevatedContainer: {
    padding:15,
    backgroundColor: 'white',
    width: '100%',
    height: '100%',
    alignSelf: 'center',
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    // marginVertical: 20,
    borderRadius: 10
  },
  searchContainer: {
    flexDirection: 'row',
    justifyContent:'space-between',
    // marginTop:15,
    marginBottom:40
  },
  label: {
      color:'black',
      fontSize:15
  },
  VisibleContainer: {
    flexDirection: 'row',
    justifyContent:'space-between',
    marginTop:15,
    marginBottom:15
  }
})

const mapStateToProps = state => ({
  businessId: state.business.businessId
})

const bindActions = dispatch => ({
 
})

export default connect(mapStateToProps, bindActions)(RestaurantOrdersScreen)