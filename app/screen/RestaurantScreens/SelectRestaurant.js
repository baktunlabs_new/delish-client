import React from 'react'
import {
  StyleSheet,
  Dimensions,
  SafeAreaView,
  Platform, 
  View,
  TouchableOpacity,
  Image,
  Text,
  Linking,
  Alert
} from 'react-native'
import { connect } from 'react-redux'

import { APIKEY } from '../../utils/global'
import { getBusinessByUser, } from '../../parse/parse'
import { primaryColor,secondaryColor, pinkish, lightpurple } from '../../utils/colors'
/** components */
import StoreList from '../../components/StoreList'
import { setBusinessId } from '../../reducers/business'
const { height, width } = Dimensions.get('window')
const categories = [
  { id: '1', name: 'Botana' },
  { id: '2', name: 'XX Large' },
  { id: '3', name: 'Hielo' },
  { id: '4', name: 'Cerveza' },
  { id: '5', name: 'Licores' },
  { id: '6', name: 'Refrescos' }
]

class SelectRestaurantScreen extends React.Component {
  state = {
    business: []
  }

  async componentDidMount() {
    const resp = await getBusinessByUser()
    console.log('resp', resp)
    if (resp.code === 200) {
        this.setState({business:resp.data})
    }
  }

  handleNavigateRestaurant = storeId => {
    const { navigation, setBusinessId} = this.props
    setBusinessId(storeId)
    console.log('props', this.props)
    navigation.navigate('RestaurantOrders')
  }

  handleGoBack = () => {
    const { navigation } = this.props
    navigation.goBack()
  }

  sendMessageToWhatsApp = async () => {
    const link = 'whatsapp://send?phone=+529993422523'
    const supported = await Linking.canOpenURL(link)
  
    console.log('supported', supported)
    if (!supported) {
      Alert.alert('¡Alerta!',
        'Es necesario installar WhatsApp para continuar'
      );
    } else {
      return Linking.openURL(link);
    }
    // Linking.openURL('whatsapp://send?text=&phone=+529993422523')
  }

  render() {
    const { business } = this.state
    console.log('state', this.state)
    return (
      <SafeAreaView style={styles.container} >
        <View style={styles.headerContainer}>
            <TouchableOpacity style={styles.btnContainer} onPress={this.handleGoBack}>
                <Image
                    source={require('../../assets/flecha_back_white.png')}
                    style={{
                        width:30, 
                        height:30,
                        alignSelf:'center'
                    }}
                />
            </TouchableOpacity>
            
        </View>
        <View style={{ paddingTop: Platform.OS === 'ios' ? 30 : 15 }}>
        <Text style={{fontSize:17, fontWeight:'bold', color:'black', alignSelf:'center'}}>Mis Tiendas</Text>
          <StoreList 
              data={business}
              onPress={this.handleNavigateRestaurant}
              showFooter={true}
              sendMsg={this.sendMessageToWhatsApp}
          />
        </View>
       
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: { 
    flex: 1,
    backgroundColor: 'white'
  },
  headerContainer: {
    width: width, 
    position: 'absolute', 
    flexDirection: 'row', 
    backgroundColor: 'transparent', 
    paddingTop: Platform.OS === 'ios' ? 40 : 15, 
    paddingHorizontal: 20, 
    justifyContent: 'space-between', 
    zIndex:1
}, 
btnContainer: { 
  width: 50, 
  height: 50, 
  backgroundColor: lightpurple,
  borderRadius:25,
  justifyContent:'center',
  shadowColor: "#000",
  shadowOffset: {
      width: 0,
      height: 1,
  },
  shadowOpacity: 0.18,
  shadowRadius: 1.00,
  elevation: 1,
},
})

const mapStateToProps = state => ({
  businessId: state.business.businessId
})

const bindActions = dispatch => ({
  setBusinessId: (data) => dispatch(setBusinessId(data))
})

export default connect(mapStateToProps, bindActions)(SelectRestaurantScreen)