import React from 'react'
import {
  Image,
  StyleSheet,
  Dimensions,
  SafeAreaView,
  View,
  Text,
  ActivityIndicator,
  ScrollView,
  AppState,
  Alert,
  BackHandler,
  Platform
} from 'react-native'
import * as Progress from 'react-native-progress'
import MapView, { Marker, AnimatedRegion, PROVIDER_GOOGLE }  from 'react-native-maps'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/MaterialIcons'
import IconC from 'react-native-vector-icons/MaterialCommunityIcons'
import { StackActions, NavigationActions } from 'react-navigation'
import {formatPrice} from '../../utils/format'
import ModalDelivered from '../../components/ModalDelivered'
import { 
    subscribeOrder, 
    getOrder, 
    getCurrentUser, 
    rateOrder, 
    cancelOrder, 
    acceptOrder, 
    toDeliver, 
    finishOrder,
    getAvailableDeliveries,
    setDeliveryMan
} from '../../parse/parse'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { lightpurple, pinkish, primaryColor, secondaryColor } from '../../utils/colors'
import ModalAproxTime from '../../components/ModalAproxTime'
import ModalDeliveryManPicker from '../../components/ModalDeliveryManPicker'

const { height, width } = Dimensions.get('window')
const waiting = require('../../assets/progressIcons/accepted.png')
const canceled = require('../../assets/ico_error.png')
const inDelivery = require('../../assets/ico_progress.png')
const finished = require('../../assets/ico_finished.png')
const accepted = require('../../assets/ico_accepted.png')

const createdOn = require('../../assets/progressIcons/stat_00_on.png')
const createdOff = require('../../assets/progressIcons/stat_00_off.png')
const acceptedOn = require('../../assets/progressIcons/stat_01_on.png')
const acceptedOff = require('../../assets/progressIcons/stat_01_off.png')
const cookingOn = require('../../assets/progressIcons/stat_02_on.png')
const cookingOff = require('../../assets/progressIcons/stat_02_off.png')
const inDeliveryOn = require('../../assets/progressIcons/stat_03_on.png')
const inDeliveryOff = require('../../assets/progressIcons/stat_03_off.png')
const finishedOn = require('../../assets/progressIcons/stat_04_on.png')
const finishedOff = require('../../assets/progressIcons/stat_04_off.png')
const checkImg = require('../../assets/progressIcons/ok_palomita.png')

const ASPECT_RATIO = (width * 0.90) / ( 150 * 0.70 )
const LATITUDE_DELTA = 0.1 //Very high zoom level
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO

class RestaurantOrderDetail extends React.Component {
    _subscription = null
    state = {
       cartItems: [],
        total: 0.00,
        locationString: "",
        storeName: "",
        modalVisible: false,
        status: 'created',
        appState: AppState.currentState,
        orderId: null,
        order: {},
        loading: true,
        progress: 0,
        paymentMethod:0,
        modalHelp: false,
        showDetails: true,
        showDeliveryTime: false,
        showPicker: false,
        driverList:[]
    }
  
    async componentDidMount() {
        const { navigation, businessId } = this.props
        const orderId = navigation.getParam('orderId')
       
        const resp = await getOrder({orderId})
        if (resp.code === 200) {
            const stringResp = JSON.stringify(resp.data)
            const jsonResp = JSON.parse(stringResp)
            console.log('jsonResp', jsonResp)
            this.setState({
                order:jsonResp,
                total: jsonResp.total,
                locationString: jsonResp.address,
                storeName: jsonResp.store.name,
                storeDeliveryPrice: jsonResp.store.deliveryPrice,
                paymentMethod: jsonResp.paymentMethod,
                status: jsonResp.status,
                cartItems: jsonResp.products,
                location: jsonResp.location,
                deliveryMan: jsonResp.deliveryMan? { 
                    name: `${jsonResp.deliveryMan.user.name} ${jsonResp.deliveryMan.user.lastName}`,
                    img: jsonResp.deliveryMan.user.profilePicture 
                }: null,
                deliveryLocation: jsonResp.driverLatitude && jsonResp.driverLongitude ?  { latitude: jsonResp.driverLatitude, longitude: jsonResp.driverLongitude } : null,
                storeLocation: jsonResp.store? jsonResp.store.location : null,
                orderId,
                loading: false
            })
            setTimeout(() => {
                this.setProgress(jsonResp.status)
            }, 1500)
        }
        const respDeliveries = await getAvailableDeliveries({ businessId})
        console.log('respDeliveries',respDeliveries)
        if (respDeliveries.code === 200) {
            
            this.setState({ driverList: respDeliveries.data})
        }
        
        this._subscription = await subscribeOrder({orderId})
        this._subscription.on('open', () => {})
        this._subscription.on('update', (object) => {
            const stringObject = JSON.stringify(object)
            const jsonResp = JSON.parse(stringObject)
            this.setState({   
                order:jsonResp,
                total: jsonResp.total,
                locationString: jsonResp.address,
                storeName: jsonResp.store.name,
                storeDeliveryPrice: jsonResp.store.deliveryPrice,
                paymentMethod: jsonResp.paymentMethod,
                status: jsonResp.status,
                cartItems: jsonResp.products,
                location: jsonResp.location,
                deliveryMan: jsonResp.deliveryMan? { 
                    name: `${jsonResp.deliveryMan.user.name} ${jsonResp.deliveryMan.user.lastName}`,
                    img: jsonResp.deliveryMan.user.profilePicture 
                }: null,
                deliveryLocation: jsonResp.driverLatitude && jsonResp.driverLongitude ? { latitude: jsonResp.driverLatitude, longitude: jsonResp.driverLongitude } : null,
                storeLocation: jsonResp.store? jsonResp.store.location : null,
            })
            this.setProgress(jsonResp.status)
           
        })
        AppState.addEventListener("change", this._handleAppStateChange);
        if (Platform.OS == 'android') {
            BackHandler.addEventListener('hardwareBackPress', this.handlebackPress);
          }
    }
 
    async componentWillUnmount () {
        if (this._subscription) {
            this._subscription.on('close', () => {})
            this._subscription.on('stop', () => {})
            this._subscription.unsubscribe();
        }
        AppState.removeEventListener("change", this._handleAppStateChange);
        BackHandler.removeEventListener('hardwareBackPress', this.handlebackPress);

    }

    _handleAppStateChange = async (nextAppState) => {
        if (this.state.appState.match(/inactive|background/) && nextAppState === "active") {
            const { navigation, businessId } = this.props
            const orderId = navigation.getParam('orderId')
            const resp = await getOrder({orderId})
            if (resp.code === 200) {
                const stringResp = JSON.stringify(resp.data)
                const jsonResp = JSON.parse(stringResp)
                this.setState({   
                    order:jsonResp,
                    total: jsonResp.total,
                    locationString: jsonResp.address,
                    storeName: jsonResp.store.name,
                    status: jsonResp.status,
                    cartItems: jsonResp.products,
                })
            }
            const respDeliveries = await getAvailableDeliveries({ businessId })
            console.log('respDeliveries',respDeliveries)
            if (respDeliveries.code === 200) {
                
                this.setState({ driverList: respDeliveries.data})
            }
        }
        this.setState({ appState: nextAppState });
    }

    setProgress = status => {
        if (status === 'created') {
            this.setState({ progress: 0.25 })
        }
        if (status === 'accepted') {
            this.setState({ progress: 0.50 })
        }
        if (status === 'inDelivery') {
            this.setState({ progress: 0.75 })
        }
        if (status === 'arrived') {
            this.setState({ progress: 1 })
        }
        if (status === 'finished') {
            this.setState({ progress: 1 })
        }
    }

    updateTotal = (items) => {
        var total = 0
        items.map(item => {
            const cost = item.price * item.quantity;
            total = total + cost;
        })
        return total
    }
    handlebackPress = () => {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'App' })],
        })
        this.props.navigation.dispatch(resetAction);
        return true
    }
    handleGoBack = () => {
        const { navigation } = this.props
       
        
        navigation.goBack()
    }


    callRestaurant = () => {
        // Linking.openURL(`tel:${phoneNumber}`)
        console.log('Entro')

    }

    renderStatus = status => {
        switch (status) {
            case 'created': 
                return 'Esperando confirmacion'
            case 'accepted': 
                return 'Orden aceptada. Preparando tus productos'
            case 'inDelivery': 
                return 'En reparto'
            case 'arrived': 
                return 'Tu pedido ha llegado'
            case 'finished': 
                return 'Finalizada'
            default:
                return 'Esperando confirmacion'
        }
    }

    renderImage = status => {
        switch (status) {
            case 'created': 
                return waiting
            case 'accepted': 
                return accepted
            case 'inDelivery': 
                return inDelivery
            case 'arrived': 
                return inDelivery
            case 'finished': 
                return finished
            case 'canceled': 
                return canceled
            default:
                return waiting
        }
    }

    nextStep = async () => {
        const { navigation } = this.props
        const { status, deliveryMan } = this.state
        const orderId = navigation.getParam('orderId')
        switch (status) {
            case 'accepted': 
                if (deliveryMan) {
                    const resp = await toDeliver({orderId})
                    console.log('resop',resp)
                    break 
                } else {
                    Alert.alert('Seleccione un repartidor', 'para poder continuar con la orden, seleccione a un repartidor')
                    break
                }
                
            case 'inDelivery': 
                const res = await finishOrder({orderId})
                console.log('res',res)
                break 

            default:
                break 
        }
    }

    handleCancel = async () => {
        const { navigation } = this.props
        const orderId = navigation.getParam('orderId')
        Alert.alert(
            'Cancelar order',
            'Esta seguro que desea cancelar la orden.',
            [
                {
                text: "Regresar",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
                },
                { text: "Cancelar orden", onPress: async () => { 
                    await cancelOrder({ orderId })
                    const { navigation } = this.props
                    navigation.goBack()
                }}
            ],
            { cancelable: false }
        )
    }
    
    onLayout = () => {
        const { location, deliveryLocation, storeLocation } = this.state
        if (deliveryLocation) {
            console.log('entro1')
            const coords = [location, deliveryLocation, storeLocation]
            if (this.map) {
                // this.map.fitToElements(true)
                this.map.fitToCoordinates(coords, {
                edgePadding: {
                    right: 10,
                    bottom: 10,
                    left: 10,
                    top: 280,
                },
                animated: true
                })
                
            }
        } else {
            const coords = [location, storeLocation]
            console.log('entro2')
            if (this.map) {
                // this.map.fitToElements(true)
                this.map.fitToCoordinates(coords, {
                edgePadding: {
                    right: 10,
                    bottom: 10,
                    left: 10,
                    top: 280,
                },
                animated: true
                })
            }
        }
    }

    handleShowDetails = () => {
        const { showDetails } = this.state
        this.setState({showDetails: !showDetails})
    }

    showAproxTime = () => {
        this.setState({ showDeliveryTime: true })
    }

    closeAproxTime = () => { 
        this.setState({ showDeliveryTime: false })
    }

    showPicker = () => {
        this.setState({ showPicker: true })
    }

    closePicker = () => {
        this.setState({ showPicker: false })
    }

    acceptOrder = async () => {
        const  { aproxTime } = this.state
        const { navigation } = this.props
        const orderId = navigation.getParam('orderId')
        if (aproxTime){

            const resp = await acceptOrder({ orderId, estimatedTime: Number(aproxTime) }) 
            console.log('resp', resp)
            this.closeAproxTime()
        } else {
            Alert.alert('debe llenar el campo con el tiempo de entrega estimada')
        }
    }
    
    setDeliveryMan = async ( deliveryManId ) => {
        const { navigation } = this.props
        const orderId = navigation.getParam('orderId')

        await setDeliveryMan({ orderId, deliveryManId }); 
        this.closePicker();
    }

    render() {
        const { 
            modalVisible,
            locationString,
            status,
            cartItems,
            loading,
            progress,
            storeLocation,
            deliveryLocation,
            deliveryMan,
            storeDeliveryPrice,
            paymentMethod,
            modalHelp,
            showDetails,
            showDeliveryTime,
            showPicker,
            driverList
        } = this.state
        console.log('props',this.props)
        const { location } = this.props
        console.log('state',this.state)
        // if (location) {
        // const { latitude , longitude } = location
        const initialRegion = {
            latitude: location? location.latitude : 20.9800083,
            longitude: location? location.longitude : -89.7730044,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }
       
        return (
            <SafeAreaView style={styles.container}>
                {/* <Button
                    style={{borderBottomWidth: StyleSheet.hairlineWidth}}
                    onPress={this.handleGoBack}>
                    <Icon
                        name='chevron-left'
                        color={lightpurple}
                        size={50}
                    />
                </Button> */}
                <View style={styles.headerContainer}>
                    <TouchableOpacity style={styles.btnContainer} onPress={this.handleGoBack}>
                        <Image
                            source={require('../../assets/flecha_back_white.png')}
                            style={{
                                width:30, 
                                height:30,
                                alignSelf:'center'
                            }}
                        />
                    </TouchableOpacity>
                    
                </View>
                <ScrollView 
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ flexGrow: 1 }}> 
                    {
                        !loading
                        ? <View style={styles.body}>
                        { status === 'inDelivery'? 
                            <View style={styles.statusContainer}>
                           <MapView
                            ref={ref => this.map = ref}
                            provider={PROVIDER_GOOGLE}
                            initialRegion={initialRegion}
                            style={StyleSheet.absoluteFillObject}
                            // customMapStyle={blueTheme}
                            scrollEnabled={false}
                            pitchEnabled={false}
                            rotateEnabled={false}
                            zoomTapEnabled={false}
                            zoomEnabled={false}
                        
                            onMapReady={this.onLayout}
                            >
                                {deliveryLocation && <Marker
                                zIndex={1}
                                coordinate={deliveryLocation}>
                                <Image  
                                    resizeMode='contain'
                                    source={require('../../assets/mrk_driver.png')}
                                    style={{ width: 50, height: 50 }}
                                     />
                                </Marker>}
                                {location && <Marker
                                coordinate={initialRegion}>
                                <Image  
                                    resizeMode='contain'
                                    source={require('../../assets/mrk_home.png')}
                                    style={{ width: 50, height: 50 }} />
                                </Marker>}
                                {storeLocation && <Marker
                                coordinate={storeLocation}>
                                <Image  
                                    resizeMode='contain'
                                    source={require('../../assets/mrk_store.png')}
                                    style={{ width: 50, height: 50 }} />
                                </Marker>}
                                
                            </MapView>
                            </View>
                            : null
                        }
                            <View style={styles.deliveryInfoContainer}>
                                <Text style={[styles.title, { fontWeight: 'bold', marginBottom: 10 }]}>Resumen de tu orden</Text>
                                <View style={styles.locationContainer}>
                                    <Text style={styles.locationTitle}>Servicio solicitado desde</Text>
                                    <Text style={styles.locationText}>{locationString ? locationString : 'Sin Ubicacion'}</Text>
                                </View>
                                <View style={styles.locationContainer}>
                                    <Text style={styles.locationTitle}>Le atiende</Text>
                                    <Text style={styles.locationText}>{this.state.storeName}</Text>
                                </View>
                            </View>
                                {/* PROGRESS BAR */}
                            <View style={styles.progressContainer}>
                                <View style={{ padding:10, backgroundColor:'#f2f2f2'}}>
                                
                                <View style={{justifyContent:'center', marginHorizontal:5}}>
                                    <Progress.Bar
                                        progress={progress}
                                        width={width*0.8}
                                        unfilledColor='#e4e4e4'
                                        borderWidth={0}
                                        height={15}
                                        borderRadius={10}
                                        color={lightpurple}//'#ffba3b'
                                        animated
                                        useNativeDriver
                                    />
                                </View>    
                                <View style={{flexDirection:'row', justifyContent:'space-between', width:'100%', paddingHorizontal:5}}>
                                    <View>
                                        <Image
                                            source={progress > 0? createdOn: createdOff}
                                            style={{ width:20, height:20, }}
                                            resizeMode='contain'
                                        />
                                       { progress > 0?
                                            <Image 
                                                source={checkImg}
                                                style={{ width:10, height:10, position:'absolute', marginLeft:10 }}
                                                resizeMode='contain'
                                            />:null
                                        }
                                    </View>
                                    <View>
                                        <Image
                                            source={progress >= 0.25? acceptedOn: acceptedOff}
                                            style={{ width:20, height:20}}
                                            resizeMode='contain'
                                        />
                                        { progress > 0.25?
                                            <Image 
                                                source={checkImg}
                                                style={{ width:10, height:10, position:'absolute', marginLeft:10 }}
                                                resizeMode='contain'
                                            />:null
                                        }
                                    </View>
                                    <View>
                                        <Image
                                            source={progress >= 0.50? cookingOn: cookingOff}
                                            style={{ width:20, height:20}}
                                            resizeMode='contain'
                                        />
                                        { progress > 0.50?
                                            <Image 
                                                source={checkImg}
                                                style={{ width:10, height:10, position:'absolute', marginLeft:10 }}
                                                resizeMode='contain'
                                            />:null
                                        }
                                    </View>
                                    
                                    <View>
                                        <Image
                                            source={progress >= 0.75? inDeliveryOn: inDeliveryOff}
                                            style={{ width:20, height:20}}
                                            resizeMode='contain'
                                        />
                                        { progress > 0.75?
                                            <Image 
                                                source={checkImg}
                                                style={{ width:10, height:10, position:'absolute', marginLeft:10 }}
                                                resizeMode='contain'
                                            />:null
                                        }
                                    </View>
                                   <View>
                                        <Image
                                            source={progress === 0.1? finishedOn : finishedOff}
                                            style={{ width:20, height:20}}
                                            resizeMode='contain'
                                        />
                                        { progress > 0.1 && status === 'finished'?
                                            <Image 
                                                source={checkImg}
                                                style={{ width:10, height:10, position:'absolute', marginLeft:10 }}
                                                resizeMode='contain'
                                            />:null
                                        }
                                   </View>
                                    
                                </View>                         
                                
                                </View>
                                <View style={{ paddingHorizontal: 10, marginVertical: 20, flexWrap: 'wrap', flexDirection:'row' }}>
                                    <Text style={{ color: 'black', fontWeight: 'bold' }}>Estado: </Text>
                                    <Text>{this.renderStatus(status)}</Text>
                                </View>
                            </View>
                            {/* RESUMEN DE PEDIDO  */}
                            <View style={styles.detailsContainer}>
                                <View style={{flexDirection:'row', justifyContent: 'space-between'}}>
                                    <Text style={[styles.title, { fontWeight: 'bold', marginBottom: 10 }]}>Resumen del Pedido</Text>
                                    <TouchableOpacity onPress={this.handleShowDetails} style={{ width:80, justifyContent:'center' }}>
                                       { showDetails?
                                        <Icon
                                            name='keyboard-arrow-up'
                                            size={40} 
                                            color={lightpurple} 
                                            style={{alignSelf:'center'}}      
                                        /> :
                                        <Icon
                                            name='keyboard-arrow-down'
                                            size={40} 
                                            color={lightpurple}   
                                            style={{alignSelf:'center'}}        
                                        />
                                        }
                                    </TouchableOpacity>
                                </View>
                                
                                    {
                                        cartItems && showDetails && cartItems.map(item => (
                                            <View
                                                key={item.id}
                                                style={styles.cartItem}>
                                                <View style={styles.itemTextContainer}> 
                                                    <Text style={styles.itemName}>{`x${item.quantity} ${item.name}`}</Text>
                                                    <Text style={styles.itemDesc}>{item.description}</Text>
                                                </View>
                                                <View style={{maxWidth:'20%'}} > 
                                                    <Text style={styles.itemPrice}>{
                                                        `$${(item.extraTotal?(item.price * item.quantity) 
                                                        + (item.extraTotal * item.quantity)
                                                        :(item.price * item.quantity) ).toFixed(2)}`}
                                                    </Text>
                                                </View>
                                            </View>
                                        ))
                                    }
                                    { showDetails && <View>
                                    <View style={{borderBottomWidth: StyleSheet.hairlineWidth, marginHorizontal:20}} />
                                    <View  style={styles.cartItem}>
                                        <Text style={styles.itemName}>Costo de envío</Text>
                                        <Text style={styles.itemPrice}>{`$${storeDeliveryPrice.toFixed(2)}`}</Text>
                                    </View>
                                    <View  style={styles.cartItem}>
                                        <Text style={[styles.itemName, {fontSize:20, fontWeight:'bold'}]}>Total</Text>
                                        <Text style={[styles.itemPrice, {fontSize:20}]}>{`$${formatPrice(this.state.total)}`}</Text>
                                    </View>
                                    <View style={{borderBottomWidth: StyleSheet.hairlineWidth, marginHorizontal:20}} />
                                    <View  style={styles.cartItem}>
                                        <View style={{flexDirection:'row', justifyContent:'center'}}>
                                            <IconC 
                                                name={'cash-multiple'}
                                                size={30}
                                                style={{alignSelf:'center'}}
                                                color={pinkish}
                                            />
                                            <Text style={[styles.itemName, {fontSize:20, fontWeight:'bold', alignSelf:'center', marginLeft:10}]}>
                                                {paymentMethod === 'cash'? 'Efectivo': 'Tarjeta'}
                                            </Text>
                                        </View>    
                                    </View>
                                    </View>
                                }
                                
                            </View>
                            { status != 'created'? <TouchableOpacity disabled={deliveryMan? true: false} onPress={this.showPicker} style={[styles.detailsContainer, {paddingLeft: 10}]}>
                                <View style={{flexDirection:'row', justifyContent: 'space-between'}}>
                                    <Image
                                        defaultSource={require('../../assets/icon_name.png')}
                                        source={deliveryMan && deliveryMan.img? {uri:deliveryMan.img }: require('../../assets/icon_name.png')}
                                        style={{
                                            width:50,
                                            height:50,
                                            backgroundColor:lightpurple,
                                            borderRadius:50/2,
                                            borderWidth:1,
                                            borderColor:lightpurple,
                                            marginRight:10
                                        }}
                                    />
                                    <Text style={[styles.locationText, {alignSelf:'center'}]}>{deliveryMan? deliveryMan.name: 'Asignar Conductor'}</Text>
                                    <View  style={{ width:80, justifyContent:'center' }}>
                                        { !deliveryMan && <Icon
                                            name='keyboard-arrow-right'
                                            size={40} 
                                            color={lightpurple} 
                                            style={{alignSelf:'center'}}      
                                        /> 
                                        }
                                    </View>
                                </View>
                            </TouchableOpacity>:null
                            }
                            {
                                status != 'created' && status != 'finished' && status != 'cancelled'  ?
                                <TouchableOpacity onPress={this.nextStep} style={styles.btnNext}>
                                    <Text style={{color:'white', alignSelf:'center', fontWeight:'bold'}}>{status ==='inDelivery' ? 'Finalizar':'Siguiente'}</Text>
                                </TouchableOpacity>:null
                            }
                            {
                                status != 'created' && status != 'finished'  && status != 'cancelled' ?
                                <TouchableOpacity onPress={this.handleCancel} style={styles.btnCancelOrder}>
                                    <Text style={{color:'white', alignSelf:'center', fontWeight:'bold'}}>Cancelar pedido</Text>
                                </TouchableOpacity>:null
                            }
                            
                            {   status === 'created' ?
                                <View style={styles.footer}>
                                    <TouchableOpacity onPress={this.showAproxTime} style={styles.btnAccept}>
                                        <Text style={{color:'white', alignSelf:'center', fontWeight:'bold'}}>Aceptar</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={()=>{ }} style={styles.btnReject}>
                                        <Text style={{color:'white', alignSelf:'center', fontWeight:'bold'}}>Rechazar</Text>
                                    </TouchableOpacity>
                                    
                                </View>:null
                            }
                        </View>
                        :
                        <View style={{ height:height, width:'100%', justifyContent:'center', alignItems:'center', backgroundColor:'white'}}>
                        <ActivityIndicator
                            color='black'
                            size='large'
                        />
                        </View>
                    }
                </ScrollView>
                {/* MODAL TIEMPO APROXIMADO */}
                <ModalAproxTime
                    modalVisible={showDeliveryTime}
                    onRequestClose={this.closeAproxTime}
                    onAccept={this.acceptOrder}
                    onChangeText={(aproxTime) => { this.setState({aproxTime})}}
                />
                {/* MODAL SELECCIONAR CONDUCTOR */}
                <ModalDeliveryManPicker
                    modalVisible={showPicker}
                    onRequestClose={this.closePicker}
                    data={driverList}
                    onSelect={this.setDeliveryMan}
                />
            </SafeAreaView>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: 'white'
    },
    header: {
        height: height*0.20,
        marginBottom: 20,
        paddingHorizontal:20
    },
    buttonHeaderContianer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonHeader: {
        height: 35,
        width: 35,
        borderRadius: 35 / 2,
        elevation: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    title: {
        fontSize: 20,
        marginTop:10,
        marginLeft: 10
    },
    body:{
        flex: 1,
        paddingTop:40,
        paddingHorizontal:20
    },
    listContainer:{
        // height:height/2.7,
        backgroundColor:'transparent'
    },
    cartItem:{
        height:60,
        backgroundColor:'white',
        flexDirection:'row',
        paddingVertical: 5,
        elevation:1,
        marginBottom:1,
        paddingHorizontal:10,
        justifyContent: 'space-between',
        marginTop:10
    },
    itemName:{
        color: pinkish,
        alignSelf:'flex-start',
        fontSize: 18
        
    },
    itemDesc:{
        color: 'gray',
        marginLeft:5
        
    },
    itemPrice:{
        // alignSelf:'center',
        color:'black',
        fontWeight:'bold'
    },
    quantityContainer:{
        justifyContent:'center',
        width: width/3.1
    },
    itemTextContainer:{
        // justifyContent:'center',
        maxWidth: '80%'
    },
    footer: {
        justifyContent: 'space-between',
        flexDirection:'row',
        flex:1,
        marginBottom: 20

    },
    payBtn:{
        height:"100%",
        width:width/1.5,
        backgroundColor:lightpurple, 
        elevation:5, 
        borderRightColor: StyleSheet.hairlineWidth,
        borderWidth:0.5,
        justifyContent: 'center',
        borderColor: secondaryColor
    },
    payText:{
        color:'white',
        fontWeight:'bold',
        fontSize:18, 
        alignSelf:'center'
    },
    totalContainer: {
        flex:1,
        backgroundColor:lightpurple, 
        borderWidth:0.5,
        justifyContent: 'center',
        borderColor: secondaryColor
    },
    locationContainer: {
        height: height*0.08, 
        backgroundColor:'transparent',
        paddingHorizontal:20,
        justifyContent:'center',
        marginTop:5,
        marginBottom: 10
    },
    locationTitle: {
        fontSize:12,
        color:pinkish
    },
    locationText: {
        fontSize: 18,
        // fontWeight:'bold'
    },
    paymentMethodContainer: {
        backgroundColor:'transparent',
        flex:1, 
        paddingVertical:10
    },
    paymentMethodBtn: { 
        backgroundColor:lightpurple, 
        height:50, justifyContent:'center'
    },
    storeTitle: {
        fontSize: 18,
        color: 'black'
    },
    statusContainer: {
        backgroundColor: 'white',
        height:200,
        width: '100%',
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        marginVertical:20,
        borderRadius:10,
        overflow:'hidden'
    },
    progressContainer: {
        width: '100%',
        backgroundColor: 'white',
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        borderRadius: 10, 
        marginBottom: 20
    },
    deliveryInfoContainer: {
        backgroundColor: 'white',
        width: '100%',
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        marginVertical: 20,
        borderRadius: 10
    },
    detailsContainer: {
        width: '100%',
        backgroundColor: 'white',
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        borderRadius: 10,
        paddingVertical: 20,
        marginBottom: 20
    }, 
    btnContainer: { 
        width: 50, 
        height: 50, 
        backgroundColor: lightpurple,
        borderRadius:25,
        justifyContent:'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.18,
        shadowRadius: 1.00,
        elevation: 1,
    },
    headerContainer: {
        width: width, 
        position: 'absolute', 
        flexDirection: 'row', 
        backgroundColor: 'transparent', 
        paddingTop: Platform.OS === 'ios' ? 40 : 15, 
        paddingHorizontal: 20, 
        justifyContent: 'space-between', 
        zIndex:1
    }, 
    btnCancelOrder: { 
        backgroundColor: '#EB110E', 
        height:50, 
        justifyContent:'center', 
        borderRadius:30, 
        marginHorizontal:20,
        marginBottom: 30
    },
    btnAccept: {
        backgroundColor: '#00B61C', 
        paddingVertical:10,
        justifyContent:'center', 
        borderRadius:30, 
        width: width * 0.35
    },
    btnReject: {
        backgroundColor: '#EB110E', 
        paddingVertical:10,
        justifyContent:'center', 
        borderRadius:30, 
        width: width * 0.35
    },
    btnNext:  { 
        backgroundColor: lightpurple, 
        height:50, 
        justifyContent:'center', 
        borderRadius:30, 
        marginHorizontal:20,
        marginBottom:20
    },
})
  
const mapStateToProps = state => ({
    location: state.location.location,
    cart: state.shoppingCart.cart,
    store: state.shoppingCart.store,
    businessId: state.business.businessId
})

const bindActions = dispatch => ({
})

export default connect(mapStateToProps, bindActions)(RestaurantOrderDetail)
