import React from 'react'
import {
  StyleSheet,
  Dimensions,
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
} from 'react-native'
import { StackActions, NavigationActions,} from 'react-navigation';
import { connect } from 'react-redux'
import { lightpurple } from '../../utils/colors'
/** components */
import  Menu  from "../../components/Menu";
import { cleanBusinessId } from '../../reducers/business'

const { height, width } = Dimensions.get('window')


class RestaurantSettingsScreen extends React.Component {
  state = {

  }

  goToClient = async () => {
    const { cleanBusinessId } = this.props
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Main' })],
    })
    this.props.navigation.dispatch(resetAction);
    cleanBusinessId()
  }



  render() {

    return (
      <SafeAreaView style={styles.container} >
        <View style={{flex:1, padding: 20, justifyContent:'flex-end'}}>
          {/* PERFIL */}
          {/* <View style={styles.elevatedContainer}>
            <Text style={styles.title}>Perfil</Text>
            <View style={{justifyContent:'center', paddingHorizontal:20, paddingVertical:10}}>
              <TouchableOpacity style={styles.lightBtn}>
                <Text style={styles.lightBtnText}>Editar información del perfil de negocio</Text>
              </TouchableOpacity>
            </View>
          </View> */}
          {/* STAFF */}
          {/* <View style={styles.elevatedContainer}>
            <Text style={styles.title}>Staff</Text>
            <View style={{justifyContent:'center', paddingHorizontal:20, paddingVertical:10}}>
              <TouchableOpacity style={[styles.lightBtn, { marginBottom: 20}]}>
                <Text style={styles.lightBtnText}>Sección de repartidores</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.lightBtn}>
                <Text style={styles.lightBtnText}>Sección de vehículos de entrega</Text>
              </TouchableOpacity>
            </View>
          </View> */}
          {/* MEDIOS DE PAGO */}
          {/* <View style={styles.elevatedContainer}>
            <Text style={styles.title}>Medios de pago</Text>
            <View style={{justifyContent:'center', paddingHorizontal:20, paddingVertical:10}}>
              <TouchableOpacity style={styles.lightBtn}>
                <Text style={styles.lightBtnText}>Métodos de pago disponibles</Text>
              </TouchableOpacity>
            </View>
          </View> */}
          {/* VOLVER A CLIENTE */}
          <TouchableOpacity onPress={this.goToClient} style={styles.darkbtn}>
            <Text style={styles.darkBtnText}>Volver a sección de clientes</Text>
          </TouchableOpacity>
        </View>
       <Menu
            current='Opciones'
            navigation={this.props.navigation}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: { 
    flex: 1,
    backgroundColor: 'white'
  },
  elevatedContainer: {
    padding:15,
    backgroundColor: 'white',
    width: '100%',
    // height: '100%',
    alignSelf: 'center',
    shadowColor: "#000",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    // marginVertical: 20,
    borderRadius: 10,
    marginBottom: 20
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black'
  },
  lightBtn: {
    borderRadius: 30,
    borderColor: lightpurple,
    borderWidth: 1,
    // backgroundColor: 'white',
    justifyContent:'center',
    paddingHorizontal: 10,
    paddingVertical:5
  }, 
  lightBtnText : {
    color: lightpurple,
    fontSize: 12,
    alignSelf:'center'
  },
  darkbtn: {
    borderRadius: 30,
    backgroundColor: lightpurple,
    justifyContent:'center',
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  darkBtnText: {
    color: 'white',
    fontSize: 12,
    alignSelf:'center'
  }
})

const mapStateToProps = state => ({

})

const bindActions = dispatch => ({
  cleanBusinessId: () => dispatch(cleanBusinessId())

})

export default connect(mapStateToProps, bindActions)(RestaurantSettingsScreen)