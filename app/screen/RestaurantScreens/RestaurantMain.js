import React from 'react'
import {
  Image,
  StyleSheet,
  Dimensions,
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Platform
} from 'react-native'
import { StackActions, NavigationActions,} from 'react-navigation';
import { connect } from 'react-redux'
import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions'

import IconM from 'react-native-vector-icons/MaterialIcons'
import IconF from 'react-native-vector-icons/FontAwesome5'
import { APIKEY } from '../../utils/global'
import { logUserOutParse, } from '../../parse/parse'
import LinearGradient from 'react-native-linear-gradient'
import { udpateLocationData } from '../../reducers/location'
import { primaryColor,secondaryColor, pinkish, lightpurple } from '../../utils/colors'
/** components */
import StoreList from '../../components/StoreList'
import  Menu  from "../../components/Menu";

const { height, width } = Dimensions.get('window')
const categories = [
  { id: '1', name: 'Botana' },
  { id: '2', name: 'XX Large' },
  { id: '3', name: 'Hielo' },
  { id: '4', name: 'Cerveza' },
  { id: '5', name: 'Licores' },
  { id: '6', name: 'Refrescos' }
]

class RestaurantMainScreen extends React.Component {
  drawer = React.createRef()
  _unsuscribe = null
  state = {

  }

  async componentDidMount() {
    
  }

  async componentWillUnmount () {
      
  }




  handleLogout = async () => {
    await logUserOutParse()
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Login' })],
    })
    this.props.navigation.dispatch(resetAction);
  }



  render() {
    console.log('this.props', this.props)
    return (
      <SafeAreaView style={styles.container} >
        <View style={{flex:1}}>

        </View>
       <Menu
            current='Inicio'
            navigation={this.props.navigation}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: { 
    flex: 1,
    backgroundColor: 'white'
  },
})

const mapStateToProps = state => ({
  businessId: state.business.businessId
})

const bindActions = dispatch => ({
})

export default connect(mapStateToProps, bindActions)(RestaurantMainScreen)