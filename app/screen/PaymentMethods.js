import React from 'react'
import {
  Image,
  StyleSheet,
  Dimensions,
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  FlatList
} from 'react-native'
import { connect } from 'react-redux'
import IconIon from 'react-native-vector-icons/Ionicons'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Button from '../components/Button'
import {formatPrice} from '../utils/format'
import { updatePaymentMethod} from '../reducers/paymentMethod'
import { lightpurple, pinkish, primaryColor } from '../utils/colors'

const { height, width } = Dimensions.get('window')

class PaymentMethods extends React.Component {

    state = {
       cartItems: [
            { id: '1', name: 'tarjeta1 ', ccnumber: '4242424242424242' },
            { id: '2', name: 'tarjeta2 ', ccnumber: '4242424242424242' },
            { id: '3', name: 'tarjeta3 ', ccnumber: '4242424242424242' },
            { id: '4', name: 'tarjeta4 ', ccnumber: '4242424242424242' },
        ],
        paymentMethod: "",
        selectedCard: {}
    }
  
    async componentDidMount() {
        
    }

    goBack = () => {
        const { navigation } =this.props
        navigation.goBack()
    } 

    selectPaymentMethod = ({ type, card }) => {
        this.setState({ paymentMethod: type, selectedCard: card? card:{}})
    }

    goToCheckout =  () => {
        const { paymentMethod, selectedCard} = this.state
        const { navigation, updatePaymentMethod } = this.props
        const data = {  type:paymentMethod, selectedCard }
        updatePaymentMethod(data)
        console.log('props', this.props)
        navigation.navigate('Checkout')
    }

    goToAddPaymentMethod = () => {
        const { navigation } = this.props
        navigation.navigate('AddPaymentMethod')
    }

    render () {
        const {source} = this.props.navigation.state.params
        console.log(source)
        return (
            <SafeAreaView style={styles.container} >
                <View style={styles.header}>
                    <TouchableOpacity
                    onPress={this.goBack}>
                        <Icon
                        name='chevron-left'
                        color={lightpurple}
                        size={50}
                    />
                    </TouchableOpacity>
                    <Text style={styles.storeTitle}>Métodos de pago</Text>
                </View>
                <View style={styles.body} >
                    <Text style={[styles.title, {fontWeight:'bold', marginBottom:10}]}>Tarjetas registradas</Text>
                    <View style={ source === "Main"?styles.listContainerMain:styles.listContainer}>
                        <FlatList
                        data={this.state.cartItems}
                        showsVerticalScrollIndicator={true}
                        keyExtractor={item => item.id}
                        contentContainerStyle={{
                        
                        }}
                        renderItem={({ item, index }) => {
                            return <Button onPress={()=>{source !== "Main"?this.selectPaymentMethod({type: "creditCard", card:item }):null}} style={item === this.state.selectedCard?styles.selectedCartItem : styles.cartItem}>
                                <View style={styles.itemTextContainer}> 
                                   <Icon
                                    name="credit-card"
                                    color={item === this.state.selectedCard?'white':lightpurple}
                                    size={40}
                                   />
                                </View>
                                <View style={styles.itemTextContainer}> 
                                    <Text style={item === this.state.selectedCard?styles.selectedItemText :styles.itemPrice}>{`${item.ccnumber.substr(12,16)}`}</Text>
                                </View>
                            </Button>
                        }}
                        />
                    </View>
                    <View style={styles.paymentMethodContainer} >
                        <Button onPress={this.goToAddPaymentMethod} style={styles.paymentMethodBtn}>
                            <Text style={styles.payText}>Agregar nuevo método de pago</Text>
                        </Button>
                        { source !== "Main"&&<Text style={[styles.title, {fontWeight:'bold', marginBottom:10}]}>Otros métodos de pago</Text>}
                        {source !== "Main"?
                        <Button onPress={()=>{source !== "Main"?this.selectPaymentMethod({type: "cash"}):null}}  style={"cash" === this.state.paymentMethod? styles.selectedCartItem : styles.cartItem}>
                            <View style={styles.itemTextContainer}>
                                <IconIon
                                name="ios-cash"
                                color={"cash" === this.state.paymentMethod?'white': lightpurple}
                                size={40}
                                />
                            </View>
                            <View style={styles.itemTextContainer}> 
                                <Text style={"cash" === this.state.paymentMethod? styles.selectedItemText : styles.itemPrice}>Efectivo</Text>
                            </View>
                        </Button>:null
                        }
                    </View>
                </View>
                { source !== "Main"&&
                <View style={styles.footer}>
                    <TouchableOpacity onPress={this.goToCheckout} style={styles.payBtn}>
                        <Text style={styles.payText}>Proceder al pago</Text>
                    </TouchableOpacity>
                </View>
                }
            </SafeAreaView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: 'white',
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: 'grey',
        marginBottom: 10
    },
    buttonHeaderContianer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonHeader: {
        height: 35,
        width: 35,
        borderRadius: 35 / 2,
        elevation: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    title: {
        fontSize: 18,
        marginTop:10,
        marginLeft: 10
    },
    body:{
        backgroundColor:'transparent',
        height: height/1.3
    },
    listContainer:{
        height:height/2.5,
        backgroundColor:'transparent',
        // marginTop:10
    },
    cartItem:{
        height:50,
        backgroundColor:'gainsboro',
        flexDirection:'row',
        paddingVertical: 5,
        elevation:1,
        marginBottom:2,
        paddingHorizontal:10,
    },
    selectedCartItem:{
        height:50,
        backgroundColor: primaryColor,
        flexDirection:'row',
        paddingVertical: 5,
        elevation:1,
        marginBottom:1,
        paddingHorizontal:10,
      

    },
    itemName:{
        color: pinkish,
        alignSelf:'flex-start',
        fontWeight:'bold'
    },
    itemPrice:{
        alignSelf:'center',
    },
    selectedItemText : {
        color:"white",
        alignSelf:'center',
        fontWeight:'bold'
    },
    quantityContainer:{
        justifyContent:'center',
        width: width/3.1
    },
    itemTextContainer:{
        paddingHorizontal:10,
        justifyContent:'center'
    },
    footer: {
        backgroundColor: 'transparent',
        flex:1,
        flexDirection:'row'
    },
    payBtn:{
        height:"100%",
        width:width,
        backgroundColor:lightpurple, 
        elevation:5, 
        borderRightColor: StyleSheet.hairlineWidth,
        borderWidth:0.5,
        justifyContent: 'center',
        borderColor:'green'
    },
    payText:{
        color:'white',
        fontWeight:'bold',
        fontSize:18, 
        alignSelf:'center'
    },
    totalContainer: {
        flex:1,
        backgroundColor:lightpurple, 
        borderWidth:0.5,
        justifyContent: 'center',
        borderColor:'green'
    },
    locationContainer: {
        height: height*0.08, 
        backgroundColor:'transparent',
        paddingHorizontal:20,
       justifyContent:'center'
    },
    locationTitle: {
        fontSize:12
    },
    locationText: {
        fontSize: 20,
      
    },
    paymentMethodContainer: {
        backgroundColor:'transparent',
        flex:1, 
        paddingVertical:10
    },
    paymentMethodBtn: { 
        backgroundColor:lightpurple, 
        height:50, 
        paddingHorizontal: 20,
        flexDirection:'row',
        justifyContent:'center'
    },
    storeTitle: {
        fontSize: 18,
        color: 'black'
    },
    listContainerMain: {
        height:height/1.6,
        backgroundColor:'transparent',
    },
    
    
});
  
const mapStateToProps = state => ({
    paymentMethod: state.paymentMethod.paymentMethod,

})

const bindActions = dispatch => ({
    updatePaymentMethod: data => dispatch(updatePaymentMethod(data))

})

export default connect(mapStateToProps, bindActions)(PaymentMethods)