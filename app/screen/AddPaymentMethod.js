import React from 'react'
import {
  Image,
  StyleSheet,
  Dimensions,
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  TextInput,
  Platform,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard
} from 'react-native'
import { connect } from 'react-redux'
import IconIon from 'react-native-vector-icons/Ionicons'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Button from '../components/Button'
import {formatPrice} from '../utils/format'
import { lightpurple } from '../utils/colors'

const { height, width } = Dimensions.get('window')

class AddPaymentMethod extends React.Component {

    state = {
     
    }
  
    async componentDidMount() {

    }
    goBack = () => {
        const { navigation } =this.props
        navigation.goBack()
    } 

    saveCard = () => {
        const  { ccNumber, cvv, expM, expY } = this.state
        const { navigation } =this.props


        navigation.goBack()
    }


    render () {
        return (
        <KeyboardAvoidingView
        behavior={Platform.OS == "ios" ? "padding" : "height"}
        style={styles.container}
        >
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <SafeAreaView style={styles.container} >
            <View style={styles.header}>
                <TouchableOpacity
                onPress={this.goBack}>
                    <Icon
                    name='chevron-left'
                    color={lightpurple}
                    size={50}
                    />
                </TouchableOpacity>
                <Text style={styles.storeTitle}>Agregar Tarjeta</Text>
            </View>
            <View style={styles.body}>
                <Text style= {{fontWeight:'bold'}}>Número de tarjeta</Text>
                <View style={styles.inputContainer}>
                    <View style={{  paddingVertical:10 }}>
                        <Icon 
                        name="credit-card"
                        size={30}
                        color={lightpurple} />
                    </View>
                    <View removeClippedSubviews={true} style={{  paddingHorizontal:10,  }}>
                        <TextInput
                            maxLength={16}
                            placeholder="xxxx-xxxx-xxxx-xxxx"
                            placeholderTextColor="gray"
                            autoCapitalize={"none"}
                            keyboardType={'numeric'}
                            contextMenuHidden={true}
                            onChangeText={ccNumber => this.setState({ ccNumber })}
                            style={styles.textInput} />
                    </View>
                </View>
                <Text style= {{fontWeight:'bold'}}>CVV</Text>
                <View style={styles.inputContainer}>
                    <View style={{  paddingHorizontal:10 }}>
                        <IconIon 
                        name="md-lock"
                        size={30}
                        color={lightpurple} />
                    </View>
                    <View removeClippedSubviews={true}>
                        <TextInput
                            maxLength={3}
                            placeholder="xxx"
                            placeholderTextColor="gray"
                            autoCapitalize={"none"}
                            keyboardType={'numeric'}
                            contextMenuHidden={true}
                            onChangeText={cvv => this.setState({ cvv })}
                            style={styles.textInputSmall} />
                    </View>
                </View>
                <Text style= {{fontWeight:'bold'}}>Fecha de vencimiento</Text>
                <View style={styles.inputContainer}>
                    <View style={{  paddingHorizontal:10 }}>
                        <IconIon 
                        name="md-lock"
                        size={30}
                        color={lightpurple} />
                    </View>
                    <View removeClippedSubviews={true}>
                        <TextInput
                            placeholder="MM"
                            maxLength={2}
                            placeholderTextColor="gray"
                            autoCapitalize={"none"}
                            keyboardType={'numeric'}
                            contextMenuHidden={true}
                            onChangeText={expM => this.setState({ expM })}
                            style={styles.textInputSmall} />
                    </View>
                    <Text style= {{fontWeight:'bold'}}>/</Text>
                    <View removeClippedSubviews={true}>
                        <TextInput
                            maxLength={2}
                            placeholder="YY"
                            placeholderTextColor="gray"
                            autoCapitalize={"none"}
                            keyboardType={'numeric'}
                            contextMenuHidden={true}
                            onChangeText={expY => this.setState({ expY })}
                            style={styles.textInputSmall} />
                    </View>
                </View>
                <View style={{flex:1, justifyContent:'center'}}>
                <Image
                    source={require('../assets/logo_openpay1.png')}
                    resizeMode="contain"
                    style={{width:width, height:height/8, alignSelf:"center", opacity:0.20}}
                />
                </View>
                
                
                
            </View>
            <View style={styles.footer}>
                <TouchableOpacity onPress={this.saveCard} style={styles.payBtn}>
                    <Text style={styles.payText}>Guardar</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
        </TouchableWithoutFeedback>
        </KeyboardAvoidingView>  
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: 'white',
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: 'grey',
        marginBottom: 10
    },
    buttonHeaderContianer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonHeader: {
        height: 35,
        width: 35,
        borderRadius: 35 / 2,
        elevation: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    title: {
        fontSize: 18,
        marginTop:10,
        marginLeft: 10
    },
    body:{
        backgroundColor:'transparent',
        height: height/1.3,
        paddingHorizontal:20
    },
    listContainer:{
        height:height/2.5,
        backgroundColor:'transparent',
        // marginTop:10
    },
    cartItem:{
        height:50,
        backgroundColor:'gainsboro',
        // borderBottomWidth: StyleSheet.hairlineWidth,
        flexDirection:'row',
        paddingVertical: 5,
        elevation:1,
        // borderRadius:3,
        // borderWidth:0.5,
        marginBottom:1,
        paddingHorizontal:10,
        // justifyContent: 'space-between'

    },
    selectedCartItem:{
        height:50,
        backgroundColor:'#E7D005',
        // borderBottomWidth: StyleSheet.hairlineWidth,
        flexDirection:'row',
        paddingVertical: 5,
        elevation:1,
        // borderRadius:3,
        // borderWidth:0.5,
        marginBottom:1,
        paddingHorizontal:10,
        // justifyContent: 'space-between'

    },
    itemName:{
        color:lightpurple,
        alignSelf:'flex-start',
        fontWeight:'bold'
    },
    itemPrice:{
        alignSelf:'center',
    },
    quantityContainer:{
        justifyContent:'center',
        width: width/3.1
    },
    itemTextContainer:{
        paddingHorizontal:10,
        justifyContent:'center'
    },
    footer: {
        backgroundColor: 'transparent',
        flex:1,
        flexDirection:'row'
    },
    payBtn:{
        height:"100%",
        width:width,
        backgroundColor:lightpurple, 
        elevation:5, 
        borderRightColor: StyleSheet.hairlineWidth,
        borderWidth:0.5,
        justifyContent: 'center',
        borderColor:'green'
    },
    payText:{
        color:'white',
        fontWeight:'bold',
        fontSize:18, 
        alignSelf:'center'
    },
    totalContainer: {
        flex:1,
        backgroundColor:'#05CD2F', 
        borderWidth:0.5,
        justifyContent: 'center',
        borderColor:'green'
    },
    locationContainer: {
        height: height*0.08, 
        backgroundColor:'transparent',
        paddingHorizontal:20,
       justifyContent:'center'
    },
    locationTitle: {
        fontSize:12
    },
    locationText: {
        fontSize: 20,
      
    },
    paymentMethodContainer: {
        backgroundColor:'transparent',
        flex:1, 
        paddingVertical:10
    },
    paymentMethodBtn: { 
        backgroundColor:lightpurple, 
        height:50, 
        paddingHorizontal: 20,
        flexDirection:'row',
        justifyContent:'center'
    },
    storeTitle: {
        fontSize: 18,
        color: 'black'
    },
    textInput: {
        borderBottomColor:lightpurple,
        borderBottomWidth:1,
        color: 'black',
        fontWeight:"bold",
        backgroundColor:'transparent',
        borderRadius:6,
        fontSize: 16,
        //marginLeft: 25,
        borderWidth: 0,
        width:width/2,
        height: Platform.OS === "ios" ? 30 : 40,
        paddingHorizontal: 10
    },
    textInputSmall: {
        borderBottomColor:lightpurple,
        borderBottomWidth:1,
        color: 'black',
        fontWeight:"bold",
        backgroundColor:'transparent',
        borderRadius:6,
        fontSize: 16,
        //marginLeft: 25,
        borderWidth: 0,
        width:width/5,
        height: Platform.OS === "ios" ? 30 : 40,
        paddingHorizontal: 10
    },
    inputContainer:{ 
        flexDirection: 'row', 
        alignItems: 'center',  
        marginBottom: 10 
    }

    
});
  
const mapStateToProps = state => ({
   
})

const bindActions = dispatch => ({
    
})

export default connect(mapStateToProps, bindActions)(AddPaymentMethod)