import React from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  SafeAreaView,
  FlatList,
  SectionList,
  Alert,
  Animated,
  Image
} from 'react-native'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/MaterialIcons'
import IconF from 'react-native-vector-icons/FontAwesome5'
import ModalProduct from '../components/ModalProduct'
import { updateCartData, updateStoreData, resetCartData } from '../reducers/shoppingCart'
import { storeData, productData, getCurrentUser } from '../parse/parse'
import sectionListGetItemLayout from 'react-native-section-list-get-item-layout'
import { formatPrice } from '../utils/format'
import { lightpurple, pinkish, primaryColor, secondaryColor } from '../utils/colors'
import ModalToRegister from '../components/ModalToRegister'
import { StackActions, NavigationActions } from 'react-navigation';


const placeHolder = require('../assets/placeholderStore.png')
const preloadedImage = Image.resolveAssetSource(placeHolder).uri
const categories = [
  { id: '1', name: 'Botana' },
  { id: '2', name: 'XX Large' },
  { id: '3', name: 'Hielo' },
  { id: '4', name: 'Cerveza' },
  { id: '5', name: 'Licores' },
  { id: '6', name: 'Refrescos' }
]

class Store extends React.Component {

  sectionRef = React.createRef();
  getItemLayout = sectionListGetItemLayout({
    // The height of the row with rowData at the given sectionIndex and rowIndex
    getItemHeight: (rowData, sectionIndex, rowIndex) => sectionIndex === 0 ? 100 : 50,

    // // These three properties are optional
    // getSeparatorHeight: () => 1 / PixelRatio.get(), // The height of your separators
    // getSectionHeaderHeight: () => 20, // The height of your section headers
    // getSectionFooterHeight: () => 10, // The height of your section footers
  })
  _unsuscribe = null
  state = {
    showModal: false,
    product: null,
    store: {},
    total: 0.00,
    animLabel: new Animated.Value(0),
    // animIcon: new Animated.Value(0),
    animationOver: false,
    showCategoryModal: false,
    showModalLogin: false
  }

  async componentDidMount() {
    const { navigation, cart } = this.props
    const storeId = navigation.getParam('storeId')
    const store = await storeData({ storeId })
    console.log('storedata', store)
    this.setState({ store })
    if (cart.length > 0) {
      const total = this.updateTotal(this.props.cart)
      Animated.timing(
        this.state.animLabel,
        {
          toValue: 100,
          duration: 200,
          useNativeDriver: false,
        }
      ).start();
      setTimeout( ()=> {this.setState({ animationOver: true, total})}, 250)
      // Animated.timing(this.state.animIcon,{toValue:50, duration:500}).start();
    }
    this._unsuscribe = navigation.addListener(
      'didFocus',
      payload => {
        this.forceUpdate();
      }
    );

  }

  async componentWillUnmount () {
    // const { navigation } = this.props
    // navigation.removeListener('didFocus', payload => {
    //   this.forceUpdate();
    // })
    this._unsuscribe.remove()
  }

 
  goToShoppingCart = async () => {
    const { navigation } = this.props
    const loggedIn = await getCurrentUser()
    if (!loggedIn) {
      this.setState({showModalLogin:true})
      return
    }
    navigation.navigate('ShoppingCart')
  }
  
  handleGoBack = () => {
    const { navigation } = this.props
    navigation.goBack()
  }

  handleOpenModal = async product => { 
    console.log('productdata ', product.id)
    const productdata  = await productData({productId:product.id})
    console.log('product', productdata)
    if (productdata.code === 200 ) {
      product.addOns = productdata.data
    } 
    product.comment = ''
    console.log('updated product', product)
    this.setState({ showModal: true, product })
  }
  handleOpenCatModal = () => { 
    this.setState({ showCategoryModal: true})
  }
  
  handleCloseCatModal = () => this.setState({ showCategoryModal: false,})

  handleCheck = (quantity, type, catIndex, topIndex) => {
    // console.log('Params: ', quantity, type, catIndex, topIndex)
    if (quantity === 0 ) {
      return <TouchableOpacity key={`${catIndex}${topIndex}`}  onPress={ async() => await this.onPressCheckbox(type,catIndex, topIndex)}>
      <Icon
      name='radio-button-unchecked'
      color={lightpurple}
      size={25}
      />
    </TouchableOpacity>
    } else {
      return <TouchableOpacity key={`${catIndex}${topIndex}`} onPress={async () => await this.onPressCheckbox(type ,catIndex, topIndex)}>
      <Icon
        name='radio-button-checked'
        color={lightpurple}
        size={25}
      />
      </TouchableOpacity> 
    }                      
  }


  handleCloseModal = () => this.setState({ showModal: false, product: null })

  handleChangeQuantity = value => {
    const { product: oldProduct } = this.state
    if (oldProduct.quantity === 1 && value === '-') return
    if (value === '-') {
      const product = {
        ...oldProduct,
        quantity: oldProduct.quantity - 1
      }
      this.setState({ product })
      return
    }
    const product = {
      ...oldProduct,
      quantity: oldProduct.quantity + 1
    }
    this.setState({ product })
  }

  handleChangeQuantityExtras = async ( value, catIndex, topIndex ) => {
    console.log(value, catIndex, topIndex )
    const { product: oldProduct, } = this.state
    if (oldProduct.addOns.categories[catIndex].toppings[topIndex].quantity === 0 && value === '-') return
    if (value === '-') {
      var product = oldProduct
      product.addOns.categories[catIndex].toppings[topIndex].quantity = oldProduct.addOns.categories[catIndex].toppings[topIndex].quantity  - 1
      console.log('product-', product)
      const extraTotal = oldProduct.extraTotal? oldProduct.extraTotal : 0
      if (extraTotal > 0){
        product['extraTotal'] = extraTotal - product.addOns.categories[catIndex].toppings[topIndex].price
      }
      this.setState({ product })
      return
    }
    var product = oldProduct
    var counter = 0
    await Promise.all( product.addOns.categories[catIndex].toppings.map( (topping , index) => {
      if ( topping.quantity > 0 ) {
        counter = counter + 1
      } 
    }))
    if (oldProduct.addOns.categories[catIndex].max_toppings_for_categories > counter || oldProduct.addOns.categories[catIndex].toppings[topIndex].quantity > 0) {
      if ( product.addOns.categories[catIndex].toppings[topIndex].max_limit > oldProduct.addOns.categories[catIndex].toppings[topIndex].quantity ) {
        product.addOns.categories[catIndex].toppings[topIndex].quantity = oldProduct.addOns.categories[catIndex].toppings[topIndex].quantity + 1
        const extraTotal = oldProduct.extraTotal? oldProduct.extraTotal : 0
        product['extraTotal'] = extraTotal + product.addOns.categories[catIndex].toppings[topIndex].price
        console.log('product+', product)
        this.setState({ product})
      } else {
        Alert.alert('', `Solo se pueden seleccionar ${product.addOns.categories[catIndex].toppings[topIndex].max_limit} de este tipo`)
      }
    } else {
      Alert.alert('', `Solo se pueden seleccionar ${product.addOns.categories[catIndex].toppings[topIndex].max_limit} opciones diferentes`)

    }
    return
  }
  onPressCheckbox = async (type ,catIndex, topIndex) => {
    const { product: oldProduct } = this.state
    console.log('params on Press Check', type ,catIndex, topIndex)
    if ( type === 'exclusive' ) {
      console.log('Exclusive')
      var product = oldProduct
       await Promise.all( oldProduct.addOns.categories[catIndex].toppings.map( (topping , index) => {
        if (index != topIndex ) {
          console.log('Entro ')
          product.addOns.categories[catIndex].toppings[index].quantity = 0
        }
      }))
        console.log('toppings before Changing Value', product.addOns.categories[catIndex].toppings)
      product.addOns.categories[catIndex].toppings[topIndex].quantity = oldProduct.addOns.categories[catIndex].toppings[topIndex].quantity === 0 ? 1 : 0
      this.setState({ product })
    } else {
      console.log('Inclusive')
      var counter = 0
      var product = oldProduct
      const newVal = oldProduct.addOns.categories[catIndex].toppings[topIndex].quantity === 0 ? 1 : 0
       await Promise.all( product.addOns.categories[catIndex].toppings.map( (topping , index) => {
        if ( topping.quantity > 0 ) {
          counter = counter + 1
        } 
      }))
      console.log('old Product ', oldProduct)
      console.log('Counter', counter)
     if (oldProduct.addOns.categories[catIndex].max_toppings_for_categories > counter || newVal === 0) {
      product.addOns.categories[catIndex].toppings[topIndex].quantity = newVal
      console.log('Product ', product)
      console.log('Counter', counter)
      this.setState({ product })
     } else { 
       Alert.alert('Maximo de productos Seleccionados', `Solo se pueden seleccionar ${oldProduct.addOns.categories[catIndex].max_toppings_for_categories}`)
     }
    }
    return
  }
  updateTotal = items => {
    var total = 0
    items.map( item => {
        const cost = item.extraTotal ? (item.price * item.quantity) + (item.extraTotal * item.quantity) : (item.price * item.quantity)  ;
        total = total + cost;
    })
    return total
  }

  handleAddProductCart = async () => {
    const { updateCartData, cart, store, resetCartData, updateStoreData } = this.props
    const { storeId } = this.props.navigation.state.params
    const { product } = this.state
    const loggedIn = await getCurrentUser()
    if (!loggedIn) {
      this.setState({
        showModal:false,
        showModalLogin: true
      })
      return
    }
    if (store !== null && store !== storeId) {
      Alert.alert(
        'Solo se puede realizar compras de un establecimiento a la vez',
        '¿Esta seguro que desea, remplazar los articulos ya seleccionados y empezar la compra de nuevo?',
        [
          {
            text: "Cancelar",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          { text: "Si, deseo agregar de este establecimiento", onPress: () => { 
            resetCartData();
            updateStoreData(storeId);
            this.handleAddProductCart()
          }}
        ],
        { cancelable: false }
      );
      return
    }

    if (store === null) {
      updateStoreData(storeId)
      
    }

    const found = cart.find(x => x.id === product.id)
    if (!found) {
      const data = [
        ...cart,
        product
      ]
      updateCartData(data)
      this.setState({ showModal: false, product: null })
      const total = this.updateTotal(data)
      
        Animated.timing(
          this.state.animLabel,
          {
            toValue: 100,
            duration: 200,
            useNativeDriver: false,
          }
        ).start();
        setTimeout( ()=> {this.setState({ animationOver: true, total })}, 250)
        // Animated.timing(this.state.animIcon,{toValue:50, duration:500}).start();
      
      return
    }
    const objIndex = cart.findIndex(obj => obj.id === product.id)
    const updateProduct = {
      ...found,
      quantity: found.quantity + product.quantity
    }
    const updatedList = [
      ...cart.slice(0, objIndex),
      updateProduct,
      ...cart.slice(objIndex + 1),
    ]
    
    updateCartData(updatedList)
    this.setState({ showModal: false, product: null })
    this.updateTotal(updatedList)
    
      Animated.timing(
        this.state.animLabel,
        {
          toValue: 100,
          duration: 200,
          useNativeDriver: false,
        }
      ).start();
      setTimeout( ()=> {this.setState({ animationOver: true })}, 250)
      // Animated.timing(this.state.animIcon,{toValue:50, duration:500}).start();
    
  }

  _scrollToSection = index => {
    console.log('index', index)
    setTimeout(() => {
      if (this.sectionRef) {
        this.sectionRef.scrollToLocation({
            animated: true,
            itemIndex:0,
            sectionIndex: index,
            viewPosition: 0
        });
      }
    }, 150);
  };
  onLogin = () => {
    setTimeout(() => {
      // this.setState({ progressVisible: false })
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Login' })],
      })
      this.props.navigation.dispatch(resetAction);
    }, 1000)
  }

  onClose = () => {
    this.setState({showModalLogin: false, showModal:true})
  }

  render() {
    const { showModal, product, store, total, showCategoryModal, showModalLogin } = this.state
    const { cart } = this.props

    return <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity
          onPress={this.handleGoBack}>
          <Icon
            name='chevron-left'
            color={primaryColor}
            size={50}
          />
        </TouchableOpacity>
        <Text style={styles.storeTitle}>{store ?store.name:`Nombre de la tienda`}</Text>
      </View>
      <View style={styles.body}>
        <Text style={styles.hallways}>Pasillos</Text>
        <View>
          <FlatList
            data={store.data}
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={item => item.id}
            contentContainerStyle={styles.categoriesContainerList}
            renderItem={({ item, index }) => {
              return <TouchableOpacity onPress={()=>{this._scrollToSection(index)}} style={styles.categoryItem}>
                <Text style={styles.categoryTitle}>{item.title}</Text>
              </TouchableOpacity>
            }}
          />
        </View>
        <Text style={{ ...styles.hallways, marginBottom: 10 }}>Nuestros productos</Text>
        <View style={{ flex: 1 }}>
          {
            store &&
            store.data &&
            store.data.length > 0 && <SectionList
              // ref={this.sectionRef}
              ref={ref => {
                  if (ref) {
                      this.sectionRef = ref;
                  }
              }}
              sections={store.data}
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{
                paddingVertical: 10
              }}
              keyExtractor={item => item.id}
              getItemLayout={this.getItemLayout}
              renderItem={({ item }) => (
                <View style={styles.item}>
                  <View style={{backgroundColor:'transparent', width:'28%',height:100, borderRadius:10}}>
                    <Image 
                      source={{uri:item.image? item.image : preloadedImage}}
                      style={{flex:1, borderRadius:10}}
                      resizeMode='stretch'
                    />
                  </View>
                  <View style={{width:'52%', padding:5 , alignSelf: "flex-start"}}>
                    <Text style={styles.itemTitle}>{item.name}</Text>
                    <Text style={styles.itemDescription}>{item.description}</Text>
                    <Text style={styles.itemPrice}>{`$${item.price.toFixed(2)}`}</Text>
                  </View>
                  <TouchableOpacity
                    onPress={() => this.handleOpenModal(item)}
                    style={styles.buttonAdd}>
                    <IconF
                      name='shopping-basket'
                      color='white'
                      size={30}
                      style={{alignSelf:'center'}}
                    />
                  </TouchableOpacity>
                </View>
              )}
              renderSectionHeader={({ section: { title } }) => (
                <Text style={styles.headerSection}>{title}</Text>
              )}
            />
          }
        </View>
        {
          cart.length > 0 && <Animated.View style={{ height: this.state.animLabel, borderTopWidth: StyleSheet.hairlineWidth, borderColor: 'grey', paddingHorizontal:30, paddingVertical:20}}>
            <TouchableOpacity onPress={this.goToShoppingCart} style={{ backgroundColor:lightpurple, flex:1, borderRadius:10, justifyContent:'space-evenly', flexDirection:'row'}}>
              {this.state.animationOver && <View style={{borderRadius:10, width:30, height:30, backgroundColor:'red',justifyContent:'center', alignSelf:'center'}}>
                  <Text style={{color:'white', alignSelf:'center', fontSize:20}}>{cart.length}</Text>
              </View>}
              {this.state.animationOver && <Text style={{fontSize:20, color:'white',alignSelf:'center', fontWeight:'bold'}}>Ver canasta</Text>}
              {this.state.animationOver && <Text style={{fontSize:20, color:'white',alignSelf:'center', fontWeight:'bold'}}>{`$${formatPrice(total)}`}</Text>}
            </TouchableOpacity>
          </Animated.View>
        }
      </View>
      {
        showModal
        ? <ModalProduct
          modalVisible={showModal}
          onRequestClose={this.handleCloseModal}
          product={product}
          onPress={this.handleChangeQuantity}
          onAdd={this.handleAddProductCart}
          // comment={this.state.comment}
          changeText={(comment) => {
            const { product: oldProduct } = this.state
            const product = {
                ...oldProduct,
                comment: comment
              }
             
             this.setState({product})
             }}
          handleOpenCatModal={this.handleOpenCatModal}
          showCategoryModal={showCategoryModal}
          onRequestCloseCategory={this.handleCloseCatModal}
          renderCheck={this.handleCheck}
          onPressAddon={this.handleChangeQuantityExtras}
        /> : null
      }
      <ModalToRegister
      modalVisible={showModalLogin}
      onLogin={this.onLogin}
      onRequestClose={this.onClose}
      />
    </SafeAreaView>
  } 
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: 'grey',
    marginBottom: 10
  },
  storeTitle: {
    fontSize: 18,
    color: 'black'
  },
  body: {
    flex: 1,
    // paddingHorizontal: 10
  },
  hallways: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'black',
    paddingHorizontal: 10
  },
  categoriesContainerList: {
    paddingLeft: 5,
    marginVertical: 10
  },
  categoryItem: {
    marginLeft: 10,
    paddingVertical:5,
    paddingHorizontal:10,
    borderRadius:15,
    backgroundColor:primaryColor,

  },
  categoryTitle: {
    color: 'white'
    
  },
  headerSection: {
    fontSize: 18,
    fontWeight: 'bold',
    color: lightpurple,
    paddingHorizontal: 10
  },
  item: {
    backgroundColor: "#f1f1f1",
    padding: 10,
    marginVertical: 8,
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'space-between'
  },
  itemTitle: {
    fontWeight: 'bold',
    color: pinkish,
    fontSize: 18
  },
  itemDescription: {
    color: 'grey',
    fontSize: 12
  },
  itemPrice: {
    fontWeight: 'bold',
    color: 'black',
    fontSize: 14,
    
  },
  buttonAdd: {
    backgroundColor: primaryColor,
    height:100, 
    borderRadius:10,
    width: '20%',
    justifyContent:'center'

  }
})

const mapStateToProps = state => ({
  cart: state.shoppingCart.cart,
  store: state.shoppingCart.store
})

const bindActions = dispatch => ({
  updateCartData: data => dispatch(updateCartData(data)),
  updateStoreData: data => dispatch(updateStoreData(data)),
  resetCartData: () => dispatch(resetCartData())

})

export default connect(mapStateToProps, bindActions)(Store)
