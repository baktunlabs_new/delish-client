import React from 'react'
import {
  Image,
  StyleSheet,
  Dimensions,
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Platform
} from 'react-native'
import { StackActions, NavigationActions } from 'react-navigation';
import { connect } from 'react-redux'
import DrawerLayout from 'react-native-gesture-handler/DrawerLayout'
import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions'
import Geolocation from 'react-native-geolocation-service'
import IconM from 'react-native-vector-icons/MaterialIcons'
import IconF from 'react-native-vector-icons/FontAwesome5'
import { APIKEY } from '../utils/global'
import { logUserOutParse, nearBusiness, getCurrentOrder, getCurrentUser } from '../parse/parse'
import LinearGradient from 'react-native-linear-gradient'
import { udpateLocationData } from '../reducers/location'
import { primaryColor,secondaryColor, pinkish, lightpurple } from '../utils/colors'
/** components */
import StoreList from '../components/StoreList'
import ModalToRegister from '../components/ModalToRegister';


const { height, width } = Dimensions.get('window')
const categories = [
  { id: '1', name: 'Botana' },
  { id: '2', name: 'XX Large' },
  { id: '3', name: 'Hielo' },
  { id: '4', name: 'Cerveza' },
  { id: '5', name: 'Licores' },
  { id: '6', name: 'Refrescos' }
]

class MainScreen extends React.Component {
  drawer = React.createRef()
  _unsuscribe = null
  state = {
    location: 'Ubicación actual',
    business: [],
    currentOrder: null,
    showModalLogin: false,
    loggedIn: true
    
  }

  async componentDidMount() {
    const loggedIn = await getCurrentUser()
    if (!loggedIn) {
      this.setState({loggedIn: false})
    }
    const result = await check(Platform.select({
      android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
      ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
    }))
    switch (result) {
      case RESULTS.UNAVAILABLE:
        this.requestPermission()
        break;
      case RESULTS.DENIED:
        this.requestPermission()
        break;
      case RESULTS.GRANTED:
        await this.getCurrentPosition()
        break;
      case RESULTS.BLOCKED:
        this.requestPermission()
        break;
    }
    const { navigation } = this.props
    const order = await getCurrentOrder()
    if (order.code === 200) {
      
      this.setState({ currentOrder: order.data })
      setTimeout(() => {
        navigation.navigate('OrderTracking')
      }, 2000)
    }

    this._unsuscribe = navigation.addListener(
      'didFocus',
      payload => {
        this.forceUpdate();
      }
    );

  }

  componentDidUpdate = async(prevProps) => {
    if (prevProps.location !== this.props.location) {
      const { location } = this.props
      if (location && location.editableAddress) {
        const { latitude, longitude } = location
        const business = await nearBusiness({ latitude, longitude })
        this.setState({ location: location.editableAddress, business })
      }
    }
  }

  async componentWillUnmount () {
    this._unsuscribe.remove()
  }

  requestPermission = () => {
    request(
      Platform.select({
        android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
        ios: PERMISSIONS.IOS.LOCATION_ALWAYS,
      })
    ).then(result => {
      if (result !== 'granted') return
      this.getCurrentPosition()
    })
    .catch(err => {
      return
    })
  }

  getCurrentPosition = async () => { 
    const { udpateLocationData } = this.props
    Geolocation.getCurrentPosition(async position => {
      console.log('position', position)
      const { coords: { latitude, longitude } } = position
      const response = await fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=${APIKEY}&sensor=true`)
      console.log('response',response)
      const result = await response.json()
      if (result.status !== 'OK') {
        return
      }
      const { results } = result
      const { formatted_address, placeId} = results[0]
      const data = {
        position: formatted_address ,
        latitude,
        longitude,
        editableAddress: formatted_address,
        placeReference:'',
        placeTitle:'',
        placeId,
      } 
      console.log('data', data)
      const business = await nearBusiness({ latitude, longitude })
      udpateLocationData(data)
      this.setState({ location: formatted_address, business })
    })
  }

  handleLogout = async () => {
    await logUserOutParse()
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Login' })],
    })
    this.props.navigation.dispatch(resetAction);
  }

  goToShoppingCart = () => {
    const { loggedIn } = this.state
    const { navigation } = this.props
    if (loggedIn) {
      navigation.navigate('ShoppingCart')
    } else {
      this.handleShowModalLogin()
    }
  }

  handleNavigateMap = () => {
    const { navigation } = this.props
    navigation.navigate('Location')
  }

  handleNavigateCurrentOrder = () => {
    const { navigation } = this.props
    navigation.navigate('OrderTracking')
  }

  handleNavigateStore = storeId => {
    const { navigation } = this.props
    navigation.navigate('Store', { storeId })
  }

  handleNavigateMenu = route => {
    const { navigation } = this.props
    this.drawer.current.closeDrawer()
    if(route !== 'PaymentMethods'){
      navigation.navigate(route)
    } else {
      navigation.navigate(route, { source: 'Main' } )
    }
  }


  handleShowModalLogin = () => {
    this.setState({
      showModalLogin: true
    })
  }

  onLogin = () => {
    setTimeout(() => {
      this.setState({ progressVisible: false })
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Login' })],
      })
      this.props.navigation.dispatch(resetAction);
    }, 1000)
  }

  onClose = () => {
    this.setState({showModalLogin: false})
  }

  renderDrawer = () => {
    return <View style={{ flex: 1 }}>
      <View style={styles.headerMenu}>
        <Text style={styles.menu}>Tu menú</Text>
      </View>
      <View style={styles.menuSection}>
        <TouchableOpacity
         onPress={() => this.handleNavigateMenu('ServiceHistory')}
         style={styles.menuItem}>
          <IconM
            name='history'
            color={lightpurple}
            size={25}
          />
          <Text style={styles.menuItemTitle}>Historial de servicios</Text>
        </TouchableOpacity>
        {/* 
        <TouchableOpacity
          onPress={() => this.handleNavigateMenu('PaymentMethods')}
          style={styles.menuItem}>
          <IconM
            name='account-balance-wallet'
            color={lightpurple}
            size={25}
          />
          <Text style={styles.menuItemTitle}>Métodos de pago</Text>
        </TouchableOpacity> 
        */}
        <TouchableOpacity
          onPress={() => this.handleNavigateMenu('Profile')}
          style={styles.menuItem}>
          <IconM
            name='person'
            color={lightpurple}
            size={25}
          />
          <Text style={styles.menuItemTitle}>Perfil</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => this.handleNavigateMenu('SelectRestaurant')}
          style={styles.menuItem}>
          <IconM
            name='business'
            color={lightpurple}
            size={25}
          />
          <Text style={styles.menuItemTitle}>Mi Restaurant</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.menuSectionLogOut}>
        <TouchableOpacity onPress={this.handleLogout} style={styles.menuItem}>
          <IconF
            name='sign-out-alt'
            color={lightpurple}
            size={25}
          />
          <Text style={styles.menuItemTitle}>Cerrar sesión</Text>
        </TouchableOpacity>
      </View>
    </View>
  }

  render() {
    const { location, business, currentOrder, showModalLogin,loggedIn } = this.state
    console.log('props Main', this.props)
    const { cart } = this.props
    console.log('business', business )
    return (
      <SafeAreaView style={styles.container} >
        <DrawerLayout
          ref={this.drawer}
          drawerWidth={width * 0.80}
          drawerPosition={DrawerLayout.positions.Left}
          drawerType='front'
          drawerBackgroundColor='white'
          renderNavigationView={this.renderDrawer}>
          <View style={styles.header}>
            <View style={styles.buttonHeaderContianer}>
            { loggedIn && <TouchableOpacity
                onPress={() => this.drawer.current.openDrawer()}
                style={styles.buttonHeader,{ justifyContent:'center',alignItems:'center'}}>
                <IconM
                  name='menu'
                  color={lightpurple}
                  size={25}
                />
              </TouchableOpacity>
            }
            </View>
            <TouchableOpacity
              onPress={this.handleNavigateMap}
              style={{ flex: 1 }}>
              <Text style={styles.title}>Tu ubicación para entrega es</Text>
              <View style={styles.locationContainer}>
                <View style={{ flex: 1 }}>
                  <Text
                    numberOfLines={1}
                    ellipsizeMode='tail'
                    style={styles.actualLocation}>
                    {location}
                  </Text>
                </View>
                <IconM
                  name='place'
                  color={lightpurple}
                  size={22}
                />
              </View>
            </TouchableOpacity>
            <View style={styles.buttonHeaderContianer}>
              <TouchableOpacity style={{
                  ...styles.buttonHeader,
                  backgroundColor: primaryColor,
                  justifyContent:'center'
                }}
                onPress={this.goToShoppingCart}>
                <IconF
                  name='shopping-basket'
                  color='white'
                  size={20}
                  style={{alignSelf:'center'}}
                />
                {cart.length > 0 && <View style={styles.cartNotification}>
                <Text style={{color:'white', alignSelf:'center', fontSize:8}}>{cart.length}</Text>
                </View>}
              </TouchableOpacity>
              
            </View>
          </View>
          <View style={styles.container}>
            {
              currentOrder && <TouchableOpacity
                onPress={this.handleNavigateCurrentOrder}
                style={styles.currentOrderButton}>
                  <View style={styles.locationContainer}>
                    <Image
                      source={require('../assets/cheveman_silouette.png')}
                      style={styles.logoCheveMan}
                      resizeMode='contain'
                    />
                    <Text style={styles.currentOrderTitle}>Pedido en servicio</Text>
                  </View>
                  <Text style={styles.goCurrentOrder}>Ir a pedido</Text>
              </TouchableOpacity>
            }
            {/* <Text style={styles.mostWanted}>Lo más solicitado</Text>
            <View style={{ marginBottom: 15 }}>
              <FlatList
                data={categories}
                horizontal
                showsHorizontalScrollIndicator={false}
                keyExtractor={item => item.id}
                contentContainerStyle={{
                  paddingLeft: 15
                }}
                renderItem={({ item }) => {
                  return <TouchableOpacity style={styles.categoryItem}>
                    <Text style={styles.categoryTitle}>{item.name}</Text>
                  </TouchableOpacity>
                }}
              />
            </View> */}
            <StoreList
              data={business}
              onPress={this.handleNavigateStore}
            />
          </View>
          <ModalToRegister
          modalVisible={showModalLogin}
          onLogin={this.onLogin}
          onRequestClose={this.onClose}
          />
        </DrawerLayout>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: { 
    flex: 1,
    backgroundColor: 'white'
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: 'grey',
    paddingVertical: 5
  },
  buttonHeaderContianer: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonHeader: {
    height: 35,
    width: 35,
    borderRadius: 35 / 2,
    elevation: 5,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'white'
  },
  title: {
    fontSize: 12
  },
  locationContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  actualLocation: {
    fontSize: 20,
    color: 'black'
  },
  mostWanted: {
    padding: 10,
    fontSize: 12
  },
  categoryItem: {
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    paddingHorizontal: 10,
    paddingVertical: 3,
    marginRight: 15,
  },
  categoryTitle: {
    color: 'white'
  },
  itemStore: {
    height: 200,
    backgroundColor: 'grey',
    borderRadius: 10,
    marginBottom: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  imgStore: {
    height: 200,
    width: '100%'
  },
  titleStore: {
    color: 'white',
    position: 'absolute',
    left: 10,
    bottom: 15,
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {
      width: -1,
      height: 1
    },
    textShadowRadius: 10,
    fontSize: 18
  },
  headerMenu: {
    marginHorizontal: 10,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: 'grey',
    paddingHorizontal: 30,
    paddingVertical: 10,
    marginTop: 20
  },
  menu: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 20
  },
  menuSection: {
    marginHorizontal: 10,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: 'grey',
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginTop: 20
  },
  menuItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 30
  },
  menuItemTitle: {
    color: 'black',
    fontSize: 20,
    marginLeft: 10
  },
  menuSectionLogOut: {
    marginHorizontal: 10,
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginTop: 20
  },
  currentOrderButton: {
    backgroundColor: '#f2f2f2',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 15,
    marginVertical: 10
  },
  logoCheveMan: {
    height: 20,
    width: 30,
    tintColor: 'grey'
  },
  currentOrderTitle: {
    fontSize: 18,
    marginLeft: 5
  },
  goCurrentOrder: {
    color: lightpurple,
    fontSize: 12
  },
  cartNotification: {
    width:20,
    height:20,
    backgroundColor:'red',
    position:'absolute',
    zIndex:10,
    borderRadius:10,
    justifyContent:'center',
    marginLeft:20,
    padding:3
  }
})

const mapStateToProps = state => ({
  location: state.location.location,
  storeId: state.shoppingCart.store,
  cart: state.shoppingCart.cart,

})

const bindActions = dispatch => ({
  udpateLocationData: data => dispatch(udpateLocationData(data))
})

export default connect(mapStateToProps, bindActions)(MainScreen)