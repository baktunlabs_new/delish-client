import React from 'react'
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
  Image,
  ActivityIndicator,
  Alert
} from 'react-native'
import ImagePicker from 'react-native-image-picker'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Button from '../components/Button'
import { getUserData, updateUserProfile } from '../parse/parse'
import { lightpurple, pinkish, primaryColor, secondaryColor } from '../utils/colors'

const options = {
  title: 'Seleccione una imagen',
  takePhotoButtonTitle: 'Tomar una foto...',
  chooseFromLibraryButtonTitle: 'Seleccionar una imagen de la libreria...',
  cancelButtonTitle: 'Cancelar',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
}
const placeholder = 'https://www.pngitem.com/pimgs/m/30-307416_profile-icon-png-image-free-download-searchpng-employee.png'

class Profile extends React.Component {
  state = {
    name: '',
    lastName: '',
    phone: '',
    email: '',
    profilePicture: null,
    saving: false
  }

  async componentDidMount() {
    const res = await getUserData()
    console.log(res)
    const { data } = res
    this.setState({
      name: data.name,
      lastName: data.lastName,
      phone: data.phone,
      email: data.email,
      profilePicture: data.profilePicture ? { uri: data.profilePicture } : null
    })
  }

  handleGoBack = () => {
    const { navigation } = this.props
    navigation.goBack()
  }

  handleSelectPicture = () => {
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker')
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error)
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton)
      } else {
        const data = {
          fileName: response.fileName,
          type: response.type,
          uri: 'data:image/jpeg;base64,' + response.data
        }
        this.setState({
          profilePicture: data
        })
      }
    })
  }

  handleChangeText = ({ index, value }) => {
    this.setState({
      [index]: value
    })
  }

  handleUpdateProfile = async () => {
    const {
      email,
      lastName,
      name,
      phone,
      profilePicture
    } = this.state

    Keyboard.dismiss()
    this.setState({ saving: true })

    const user = await updateUserProfile({
      email,
      lastName,
      name,
      phone,
      profilePicture
    })

    if (user.code !== 200) {
      this.setState({ saving: false })
      Alert.alert(
        '',
        'Ocurrio un problema al intentar actualizar el usuario.'
      )
      return
    }

    this.setState({ saving: false })
    Alert.alert(
      '',
      'Usuario actualizado'
    )
  }

  render() {
    const {
      email,
      lastName,
      name,
      phone,
      profilePicture,
      saving
    } = this.state

    return <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      style={{ flex: 1 }}>
      <SafeAreaView style={styles.container}>
        <TouchableWithoutFeedback
          touchSoundDisabled
          onPress={Keyboard.dismiss}>
          <View style={styles.inner}>
            <View style={styles.header}>
              <TouchableOpacity
                onPress={this.handleGoBack}>
                <Icon
                  name='chevron-left'
                  color={lightpurple}
                  size={50}
                />
              </TouchableOpacity>
              <Text style={styles.title}>Perfil de usuario</Text>
            </View>
            <View style={styles.infoContainer}>
              <View style={styles.infoTextContainer}>
                <Text style={styles.textInfo}>Edita o actualiza la información de tu perfil</Text>
              </View>
              <TouchableOpacity
                onPress={this.handleSelectPicture}
                style={styles.imgProfileContainer}>
                  {
                    profilePicture
                    ? <Image
                      source={{ uri: profilePicture.uri }}
                      style={{
                        height: 60,
                        width: 60
                      }}
                      borderRadius={30}
                    />
                    : <Image
                      source={{ uri: placeholder }}
                      style={{
                        height: 60,
                        width: 60
                      }}
                      borderRadius={30}
                    />
                  }
              </TouchableOpacity>
            </View>
            <View style={{ padding: 20 }}>
              <Text style={styles.titleSection}>Datos personales</Text>
              <View style={styles.fieldContainer}>
                <TextInput
                  value={name}
                  placeholder='Nombre'
                  placeholderTextColor='grey'
                  underlineColorAndroid='transparent'
                  style={{
                    padding: 5
                  }}
                  onChangeText={value => this.handleChangeText({ index: 'name', value })}
                />
              </View>
              <View style={styles.fieldContainer}>
                <TextInput
                  value={lastName}
                  placeholder='Apellido'
                  placeholderTextColor='grey'
                  underlineColorAndroid='transparent'
                  style={{
                    padding: 5
                  }}
                  onChangeText={value => this.handleChangeText({ index: 'lastName', value })}
                />
              </View>
              <Text style={{
                ...styles.titleSection,
                marginVertical: 10
              }}>Información de contacto</Text>
              <View style={styles.fieldContainer}>
                <TextInput
                  value={email}
                  placeholder='Correo electronico'
                  placeholderTextColor='grey'
                  underlineColorAndroid='transparent'
                  keyboardType='email-address'
                  style={{
                    padding: 5
                  }}
                  onChangeText={value => this.handleChangeText({ index: 'email', value })}
                />
              </View>
              <View style={styles.fieldContainer}>
                <TextInput
                  value={phone}
                  placeholder='Numero de telefono'
                  placeholderTextColor='grey'
                  underlineColorAndroid='transparent'
                  keyboardType='phone-pad'
                  style={{
                    padding: 5
                  }}
                  onChangeText={value => this.handleChangeText({ index: 'phone', value })}
                />
              </View>
              <Button
                disabled={saving}
                onPress={this.handleUpdateProfile}
                style={styles.buttonSave}>
                  {
                    saving
                    ? <ActivityIndicator color='white' size='large'/>
                    : <Text style={styles.buttonSaveText}>Guardar cambios</Text>
                  }
              </Button>
            </View>
            <View style={{ flex : 1 }} />
          </View>
        </TouchableWithoutFeedback>
      </SafeAreaView>
    </KeyboardAvoidingView>
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: 'grey'
  },
  title: {
    fontSize: 18,
    color: 'black'
  },
  inner: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  infoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 20,
    justifyContent: 'space-between',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderColor: 'grey'
  },
  infoTextContainer: {
    width: '70%'
  },
  textInfo: {
    fontSize: 18,
    color: 'black'
  },
  imgProfileContainer: {
    height: 60,
    width: 60,
    borderRadius: 60 / 2,
    backgroundColor: 'grey'
  },
  titleSection: {
    fontSize: 20,
    color: primaryColor,
    marginBottom: 10
  },
  fieldContainer: {
    width: '100%',
    borderBottomWidth: 2,
    borderColor: pinkish,
    marginBottom: 8
  },
  buttonSave: {
    padding: 10,
    backgroundColor: lightpurple,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    marginTop: 20
  },
  buttonSaveText: {
    color: 'white',
    fontWeight: 'bold'
  }
})

export default Profile
