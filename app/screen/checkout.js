import React from 'react'
import {
  Image,
  StyleSheet,
  Dimensions,
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Alert,
  ScrollView
} from 'react-native'
import { connect } from 'react-redux'
import IconIon from 'react-native-vector-icons/Ionicons'
import Icon from 'react-native-vector-icons/MaterialIcons'
import IconC from 'react-native-vector-icons/MaterialCommunityIcons'
import {updateCartData, resetCartData} from '../reducers/shoppingCart'
import {formatPrice} from '../utils/format'
import {createOrder, storeData } from '../parse/parse'
import { lightpurple, pinkish, secondaryColor } from '../utils/colors'
import { TextInput } from 'react-native-gesture-handler'

const { height, width } = Dimensions.get('window')

class Checkout extends React.Component {

    state = {
       cartItems: null,
        total: 0.00,
        locationString: "calle 45, chuburna",
        storeName: "",
        deliveryPrice:0.00,
        modalVisible: false,
        type:null,
        paymentMethod:0,
    }
  
    async componentDidMount() {
        const {cartItems, storeName} = this.state;
        const { cart, paymentMethod, store: storeId } = this.props;
        const data = await storeData({ storeId })
        
        if (storeName != data.name) {
            this.setState({ storeName: data.name })
        }
        
        if (cartItems !== cart) {
            console.log('cart', cart)
            const total = this.updateTotal(cart)
            this.setState({
                cartItems: cart,
                total: data.deliveryPrice ? total + data.deliveryPrice : total,
                deliveryPrice: data.deliveryPrice || null
            })
        }
        
        if (paymentMethod) {
            console.log('paymentmenthod', paymentMethod)
            const { type, selectedCard } = paymentMethod
            this.setState({
                type,
                selectedCard,
            })
        }
    }

    componentDidUpdate() {
        const { paymentMethod } = this.props
        
        if (paymentMethod) {
            if (paymentMethod.type !== this.state.type ) {
                const { type, selectedCard } = paymentMethod
                this.setState({
                    type,
                    selectedCard,
                })
            }
        }
    }

    goBack = () => {
        const { navigation } =this.props
        navigation.goBack()
    } 

    updateTotal = items => {
        var total = 0
        items.map( item => {
            const cost = item.extraTotal ? (item.price * item.quantity) + (item.extraTotal * item.quantity) : (item.price * item.quantity);
            total = total + cost;
        })
        return total
    }
    
    openModal = () => this.setState({ modalVisible: true })

    closeModal = () => this.setState({ modalVisible: false })

    goToPaymentMethods = () => {
        const { navigation } = this.props
        navigation.navigate('PaymentMethods', { source: 'Checkout' })
    }

    onCreateOrder = async () => {
        const { cartItems, total, type } = this.state
        const { navigation, store: storeId, location: { latitude, longitude, position: address }, resetCartData } = this.props
        const location = { latitude, longitude }

        if (!type) {
            Alert.alert('Metodo de pago','Seleccionar un metodo de pago valido')
            return
        }

        const response = await createOrder({
            storeId,
            total,
            paymentMethod: type,
            products: cartItems,
            address,
            location
        })

        if ( response.code !== 200 ) {
            Alert.alert('Alerta', 'Error al realizar el pedido')
            return
        }
        
        resetCartData()  
        navigation.navigate('SuccessfulOrder')
    }

    getPaymentMethod = () => {
        const { type, selectedCard } = this.state
        if ( type !== 'creditCard' ) return 'Efectivo'

        const name = `${selectedCard.ccnumber.substr(12,16)}`
        return name
    }

    render() {
        const { location } = this.props
        const { cartItems, deliveryPrice, total, paymentMethod } = this.state
        console.log('state  ', this.state)
        console.log('props', this.props)
        return (
            <SafeAreaView style={styles.container} >
                {/* <View style={styles.header}>
                    <TouchableOpacity
                    onPress={this.goBack}>
                        <Icon
                        name='chevron-left'
                        color={lightpurple}
                        size={50}
                    />
                    </TouchableOpacity>
                    <Text style={styles.storeTitle}>Pasar a pagar</Text>
                </View> */}
                <View style={styles.headerContainer}>
                    <TouchableOpacity style={styles.btnContainer} onPress={this.goBack}>
                        <Image
                            source={require('../assets/flecha_back_white.png')}
                            style={{
                                width:30, 
                                height:30,
                                alignSelf:'center'
                            }}
                        />
                    </TouchableOpacity>
                </View>
                
                <ScrollView 
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ flexGrow: 1 }}> 
                <View style={styles.body} >
                    <View style={styles.deliveryInfoContainer}>
                        <Text style={[styles.title, { fontWeight: 'bold', marginBottom: 10 }]}>Resumen de tu orden</Text>
                        <View style={styles.locationContainer}>
                            <Text style={styles.locationTitle}>Servicio solicitado desde</Text>
                            <Text style={styles.locationText}>{location?location.position:'Sin Ubicacion'}</Text>
                        </View>
                        <View style={styles.locationContainer}>
                            <Text style={styles.locationTitle}>Le atiende</Text>
                            <Text style={styles.locationText}>{this.state.storeName}</Text>
                        </View>
                    </View>
                    <View style={styles.detailsContainer}>
                                
                        <Text style={[styles.title, { fontWeight: 'bold', marginBottom: 10 }]}>Tu carrito</Text>

                        {
                            cartItems && cartItems.map(item => (
                                <View
                                    key={item.id}
                                    style={styles.cartItem}>
                                    <View style={styles.itemTextContainer}> 
                                        <Text style={styles.itemName}>{item.name}</Text>
                                        <Text style={styles.itemDesc}>{item.description}</Text>
                                    </View>
                                    <View style={{maxWidth:'20%'}} > 
                                        <Text style={styles.itemPrice}>{
                                            `$${(item.extraTotal?(item.price * item.quantity) 
                                            + (item.extraTotal * item.quantity)
                                            :(item.price * item.quantity) ).toFixed(2)}`}
                                        </Text>
                                    </View>
                                </View>
                            ))
                        }
                        <View style={{borderBottomWidth: StyleSheet.hairlineWidth, marginHorizontal:20}} />
                        <View  style={styles.cartItem}>
                            <Text style={styles.itemName}>Costo de envío</Text>
                            <Text style={styles.itemPrice}>{`$${deliveryPrice.toFixed(2)}`}</Text>
                        </View>
                        <View  style={styles.cartItem}>
                            <Text style={[styles.itemName, {fontSize:20, fontWeight:'bold'}]}>Total</Text>
                            <Text style={[styles.itemPrice, {fontSize:20}]}>{`$${formatPrice(this.state.total)}`}</Text>
                        </View>
                        <View style={{borderBottomWidth: StyleSheet.hairlineWidth, marginHorizontal:20}} />
                        <View  style={styles.cartItem}>
                            <View style={{flexDirection:'row', justifyContent:'center'}}>
                                <IconC 
                                    name={'cash-multiple'}
                                    size={30}
                                    style={{alignSelf:'center'}}
                                    color={pinkish}
                                />
                                <Text style={[styles.itemName, {fontSize:20, fontWeight:'bold', alignSelf:'center', marginLeft:10}]}>
                                    {this.state.type?this.getPaymentMethod():'Elige un método de pago'}
                                </Text>
                            </View>    
                            <View style={{flexDirection: 'row', justifyContent:'center'}}>
                                <Text style={{alignSelf:'center', color:'gray'}}>Cambio de </Text>
                                <TextInput
                                    placeholder='0.00'
                                    value={this.state.change}
                                    placeholderTextColor='black'
                                    style={{backgroundColor:'#e9e9e9',color:'black', width:80, justifyContent:'center', textAlign:'center'}}
                                    keyboardType='numeric'
                                    onChangeText={change => {
                                        this.setState({ change })
                                    }}
                                />
                            </View>
                        </View>
                        {/* 
                            <TouchableOpacity onPress={this.goToPaymentMethods} style={styles.paymentMethodBtn}>
                                <Text style={styles.payText}>{this.state.type?this.getPaymentMethod():'Elige un método de pago'}</Text>
                            </TouchableOpacity> 
                        */}
                        <View style={{borderBottomWidth: StyleSheet.hairlineWidth, marginHorizontal:20}} />
                        
                        <TouchableOpacity 
                        onPress={this.onCreateOrder} 
                        style={styles.payBtn}>
                            <Text style={{ color: 'white', alignSelf:'center'}}>Realizar pedido</Text>
                        </TouchableOpacity>
                        
                    </View>
                    {/* <View style={styles.deliveryInfoContainer}>
                    <View style={styles.listContainer}>
                         <FlatList
                        data={this.state.cartItems}
                        showsVerticalScrollIndicator={true}
                        keyExtractor={item => item.id}
                        renderItem={({ item, index }) => {
                            return <View style={styles.cartItem}>
                                <View style={styles.itemTextContainer}> 
                                    <Text style={styles.itemName}>{item.name}</Text>
                                </View>
                                <View style={styles.itemTextContainer}> 
                                    <Text style={styles.itemPrice}>{`$${(item.extraTotal?(item.price * item.quantity) + (item.extraTotal * item.quantity):(item.price * item.quantity) ).toFixed(2)}`}</Text>
                                </View>
                            </View>
                        }}
                        /> 
                    </View>
                    </View> */}
                    
                </View>
                </ScrollView>
                {/* { this.state.deliveryPrice &&
                    <View style={{backgroundColor:'orange'}}>
                        <Text style={styles.payText}>{`costo de envio : $${formatPrice(this.state.deliveryPrice)}`}</Text>
                    </View>
                }
                <View style={styles.footer}>
                    <TouchableOpacity  onPress={this.onCreateOrder} style={styles.payBtn}>
                        <Text style={styles.payText}>Realizar pedido</Text>
                    </TouchableOpacity>
                    <View style={styles.totalContainer}>
                        <Text style={styles.payText}>{`$${formatPrice(this.state.total)}`}</Text>
                    </View>
                </View> */}
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: 'white',
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: 'grey',
        marginBottom: 10
    },
    buttonHeaderContianer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonHeader: {
        height: 35,
        width: 35,
        borderRadius: 35 / 2,
        elevation: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    title: {
        fontSize: 20,
        marginTop:10,
        marginLeft: 10
    },
    body:{
        backgroundColor:'transparent',
        // height: height/1.4
        flex:1,
        paddingHorizontal:20,
        marginTop:30
    },
    listContainer:{
        height: '45%',
        backgroundColor:'transparent',
        // marginTop:10
    },
    cartItem:{
        height:60,
        backgroundColor:'white',
        flexDirection:'row',
        paddingVertical: 5,
        elevation:1,
        marginBottom:1,
        paddingHorizontal:10,
        justifyContent: 'space-between',
        marginTop:10
    },
    itemName:{
        color: pinkish,
        alignSelf:'flex-start',
        fontSize: 18
        
    },
    itemDesc:{
        color: 'gray',
        marginLeft:5
        
    },
    itemPrice:{
        // alignSelf:'center',
        color:'black',
        fontWeight:'bold'
    },
    quantityContainer:{
        justifyContent:'center',
        width: width/3.1
    },
    itemTextContainer:{
        // justifyContent:'center',
        maxWidth: '80%'
    },
    footer: {
        backgroundColor: 'transparent',
        height: height * 0.10,
        flexDirection:'row'
    },
    payBtn:{
        backgroundColor: lightpurple,
        borderRadius: 30,
        width: '70%',
        paddingVertical: 5,
        marginTop: 10,
        alignSelf: 'center',
        justifyContent: 'center'
    },
    payText:{
        color: lightpurple,
        // fontWeight:'bold',
        fontSize:18, 
        alignSelf:'center'
    },
    totalContainer: {
        flex:1,
        backgroundColor:lightpurple, 
        borderWidth:0.5,
        justifyContent: 'center',
        borderColor:secondaryColor
    },
    locationContainer: {
        height: height* 0.08, 
        backgroundColor:'transparent',
        paddingHorizontal:20,
        justifyContent:'center',
        marginTop:5,
        marginBottom: 10
    },
    locationTitle: {
        fontSize:12,
        color:pinkish
    },
    locationText: {
        fontSize: 18,
        // fontWeight:'bold'
    },
    paymentMethodContainer: {
        backgroundColor:'transparent',
        flex:1, 
        paddingVertical:10
    },
    paymentMethodBtn: { 
        backgroundColor: 'white',
        borderRadius: 30,
        width: '70%',
        paddingVertical: 5,
        borderColor: lightpurple,
        borderWidth:1,
        marginTop: 10,
        alignSelf: 'center',
        justifyContent: 'center',
        marginBottom: 20
    },
    storeTitle: {
        fontSize: 18,
        color: 'black'
    },
    deliveryInfoContainer: {
        backgroundColor: 'white',
        width: '100%',
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        marginVertical: 20,
        borderRadius: 10
    },
    detailsContainer: {
        width: '100%',
        backgroundColor: 'white',
        alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        borderRadius: 10,
        paddingVertical: 20
    }, 
    btnContainer: { 
        width: 50, 
        height: 50, 
        backgroundColor: lightpurple,
        borderRadius:25,
        justifyContent:'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.18,
        shadowRadius: 1.00,
        elevation: 1,
    },
    headerContainer: {
        width: width, 
        position: 'absolute', 
        flexDirection: 'row', 
        backgroundColor: 'transparent', 
        paddingTop: Platform.OS === 'ios' ? 40 : 15,  
        paddingHorizontal: 20, 
        justifyContent: 'space-between', 
        zIndex:1
    }, 
});
  
const mapStateToProps = state => ({
    location: state.location.location,
    cart: state.shoppingCart.cart,
    store: state.shoppingCart.store,
    paymentMethod: state.paymentMethod.paymentMethod,
})

const bindActions = dispatch => ({
    updateCartData: data => dispatch(updateCartData(data)),
    resetCartData: () => dispatch(resetCartData())
})

export default connect(mapStateToProps, bindActions)(Checkout)
