import * as React from 'react'
import { Text, TextInput, View, Image, ImageBackground, StyleSheet, Dimensions, TouchableOpacity, SafeAreaView, ScrollView, Platform, Alert } from 'react-native'
import { StackActions, NavigationActions } from 'react-navigation'
import { singUpUser } from '../parse/parse'
import { connect } from 'react-redux'
import { updateTokenSession, updateAccountData } from '../reducers/accounts'
import { validateRegister, validatePassword, validateEmail, validateRegisterIOS } from '../utils/validates'
import ProgressDialog from '../components/progressDialog'
import { lightpurple, pinkish } from '../utils/colors'


const { width } = Dimensions.get('window');


class RegistroScreen extends React.Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            name: '',
            lastName: '',
            phone: '',
            birthday: '',
            address: '',
            user: null,
            loggedIn: false,
            formError: false,
            emailError: false,
            passwordError: false,
            progressVisible: false,

        };
    }

    handleRegistro = async () => {
        // const { updateSession } = this.props
        const { name, lastName, phone, email, password,} = this.state
        const validateAll = validateRegisterIOS({ name, lastName, phone, email, password }) 
        const validateE = validateEmail({ email })
        const validatePass = validatePassword({ password })
        this.setState({ progressVisible: true })
        if (!validateAll) {
            this.setState({ formError: true , progressVisible: false})
            Alert.alert('Alerta','Ingrese todos los campos')
            return
        }
        if (!validateE) {
            this.setState({ emailError: true, progressVisible: false })
            Alert.alert('Alerta','Ingrese un correo electronico válido')
            return
        }
        if (!validatePass) {
            this.setState({ passwordError: true, progressVisible: false })
            Alert.alert('Alerta','Ingrese la contraseña válida mínimo de 6 carácteres')
            return
        }
        if (validateAll && validateE && validatePass ) {
            const user = await singUpUser({
                name,
                lastName,
                email,
                phone,
                password,
            })
            console.log('registro ', user)
            // this.setState({progressVisible:false})  
            if (user) {
                setTimeout(() => {
                    const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'App',})],
                    })
                    this.setState({progressVisible:false})  
                    this.props.navigation.dispatch(resetAction);
                }, 2000)
                this.setState({progressVisible:false})   
            } else {
                this.setState({progressVisible:false})  
                Alert.alert('Registro','La cuenta ya fue registrada anteriormente.')
            }
        } else {
            this.setState({ progressVisible: false }) 
            return
        }
    }


    render() {
    
        console.log("state",this.state)
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "#000000" }}>
                <ImageBackground source={require('../assets/bg_02.png')} style={{ width: '100%', height: '100%' }}>
                    <View style={{ flex: 1, backgroundColor: "rgba(0, 0, 0, 0.5)" }}>
                    <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center', marginBottom: 30 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Image
                            resizeMode='stretch'
                            style={{
                                backgroundColor: 'transparent',
                                width: 200,
                                height: 80,
                                justifyContent: 'center',
                                alignContent: 'center',
                            }}
                            source={require('../assets/text_01.png')} />
                        </View>
                        <Text style={{ fontSize: 23, marginBottom: 15, color: lightpurple }}>Registra tus datos</Text>
                    </View>
                    <ScrollView>
                        <View style={{ justifyContent: 'center', alignItems: 'center', width: '100%', marginTop: 20 }}>
                            <View style={{ marginTop: 20, justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 30, marginBottom: 20 }}>
                                    <View style={{ flex: 0.1 }}>
                                        <Image source={require('../assets/icon_name.png')} style={{ tintColor: "white", width: 20, height: 20, resizeMode: 'contain', marginRight: 10 }} />
                                    </View>
                                    <View style={{ flex: 0.9 }}>
                                        <TextInput
                                            placeholder="Nombre"
                                            placeholderTextColor="white"
                                            autoCapitalize={"none"}
                                            onChangeText={name => this.setState({ name })}
                                            style={styles.textUsuario} />
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 30, marginBottom: 20 }}>
                                    <View style={{ flex: 0.1 }}>
                                        <Image source={require('../assets/icon_name.png')} style={{ tintColor: "white", width: 20, height: 20, resizeMode: 'contain', marginRight: 10 }} />
                                    </View>
                                    <View style={{ flex: 0.9 }}>
                                        <TextInput
                                            placeholder="Apellido"
                                            placeholderTextColor="white"
                                            autoCapitalize={"none"}
                                            onChangeText={lastName => this.setState({ lastName })}
                                            style={styles.textUsuario} />
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 30, marginBottom: 20 }}>
                                    <View style={{ flex: 0.1 }}>
                                        <Image source={require('../assets/icon_cellphone.png')} style={{ tintColor: "white", width: 20, height: 20, resizeMode: 'contain', marginRight: 10 }} />
                                    </View>
                                    <View style={{ flex: 0.9 }}>
                                        <TextInput
                                            placeholder="Teléfono"
                                            placeholderTextColor="white"
                                            autoCapitalize={"none"}
                                            keyboardType='phone-pad'
                                            onChangeText={phone => this.setState({ phone })}
                                            style={styles.textUsuario}
                                        />
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 30, marginBottom: 20 }}>
                                    <View style={{ flex: 0.1 }}>
                                        <Image source={require('../assets/icon_name.png')} style={{ tintColor: "white", width: 20, height: 20, resizeMode: 'contain', marginRight: 10 }} />
                                    </View>
                                    <View style={{ flex: 0.9 }}>
                                        <TextInput
                                            placeholder="Correo"
                                            placeholderTextColor="white"
                                            autoCapitalize={"none"}
                                            onChangeText={email => this.setState({ email })}
                                            style={styles.textUsuario} />
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', paddingHorizontal: 30, marginBottom: 20 }}>
                                    <View style={{ flex: 0.1 }}>
                                        <Image source={require('../assets/icon_pass.png')}  style={{ tintColor: "white", width: 20, height: 20, resizeMode: 'contain', marginRight: 10 }} />
                                    </View>
                                    <View style={{ flex: 0.9 }}>
                                        <TextInput
                                            placeholder="Contraseña"
                                            placeholderTextColor="white"
                                            secureTextEntry={true}
                                            autoCapitalize={"none"}
                                            onChangeText={password => this.setState({ password })}
                                            style={styles.textUsuario}/>
                                    </View>
                                </View>
                            </View>
                            <TouchableOpacity style={styles.linearGradient} onPress={this.handleRegistro} >
                                <Text style={styles.buttonText}>REGISTRAR TU CUENTA</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.linearGradient} onPress={()=>{this.props.navigation.goBack()}} >
                                <Text style={styles.buttonText}>REGRESAR</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                    <ProgressDialog
                        visible={this.state.progressVisible}
                        title="Registrando"
                        message="Espere, porfavor ..."
                        activityIndicatorColor={pinkish}
                    />
                    </View>
                </ImageBackground>
            </SafeAreaView>

        );
    }




}
//AppRegistry.registerComponent('App', () => App)
//export default LoginScreen;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'stretch'

    },
    containerSignIn: {
        width: '80%',
        height: 80,
        flexDirection: 'row',
        justifyContent: 'center',
        marginLeft: '10%',
        marginRight: '6%',
    },
    containerUserName: {
        height: 60,
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: '#ffffff',
        marginLeft: '6%',
        marginRight: '6%',
    },
    containerPassword: {
        height: 60,
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: '#ffffff',
        marginLeft: '6%',
        marginRight: '6%',
    },
    containerRegister: {
        marginLeft: '6%',
        marginRight: '6%',
        alignItems: 'center',
    },
    icon: {
        flex: 1
    },
    textInput: {
        backgroundColor: 'transparent',
        flex: 5,
        color: 'black',
        paddingLeft: '25%'
    }, containerTitulos: {
        height: 60,
        marginLeft: '6%',
        marginRight: '6%',
        alignItems: 'center',
    },
    textUsuario: {
        borderBottomColor:pinkish,
        borderBottomWidth:1,
        color: 'white',
        fontWeight:"bold",
        backgroundColor:'transparent',
        borderRadius:6,
        fontSize: 16,
        //marginLeft: 25,
        borderWidth: 0,
        width: "90%",
        height: Platform.OS === "ios" ? 30 : 40,
        paddingHorizontal: 10
    },
    dateText: {
        //fontSize: 16,
        //marginLeft: 25,
        borderWidth: 0,
        width: "70%",
        height: Platform.OS === "ios" ? 30 : 40,
        color: 'white',
        backgroundColor: 'white',
        borderRadius: 8,
        paddingHorizontal: 10
    },
    textField: {
        fontSize: 16,
        //marginLeft: -25,
        borderWidth: 0,
        width: "70%",
        height: Platform.OS === "ios" ? 30 : 40,
        color: 'black',
        backgroundColor: 'white',
        borderRadius: 12
    },
    linearGradient: {
        marginTop: 20,
        backgroundColor: lightpurple,
        height: 40,
        width: '70%',
        alignItems: 'center',
        borderRadius: 9,
        justifyContent: "center",
        marginBottom: 20
    },
    buttonText: {
        fontSize: 18,
        fontFamily: 'Gill Sans',
        textAlign: 'center',
        margin: 4,
        color: 'white',
        backgroundColor: 'transparent',
    },
    imgContainer2: {
        backgroundColor: "transparent",
        width: width,
        height: 10
    },
    imgContainer: {
        backgroundColor: "transparent",
        marginTop: 10,
        width: 50,
        height: 50,
        marginBottom: 10
    },
    lineStyle: {
        width: '90%',
        borderWidth: 1,
        borderColor: '#E72A7B',
        margin: 10,
    },
    boton: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 23
    },
})

const bindActions = dispatch => ({
    updateSession: ({ token, account }) => {
        dispatch(updateTokenSession(token))
        dispatch(updateAccountData(account))
    }
})

export default connect(null, bindActions)(RegistroScreen)