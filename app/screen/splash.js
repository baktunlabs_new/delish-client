import React from 'react'
import { connect } from 'react-redux'
import { NavigationActions, StackActions } from 'react-navigation'
import { View, Image, StyleSheet, ImageBackground, Alert, SafeAreaView } from 'react-native'
import { checkSession, getCurrentUser } from '../parse/parse'
import { logOutUser } from '../reducers/accounts'
import { primaryColor } from '../utils/colors'

class SplashScreen extends React.Component {
    static navigationOptions = {
        header: null
    }

    async componentDidMount() {
        const { navigation} = this.props
        console.log('props', this.props)
        const user = await getCurrentUser();
        console.log('user', user)
        if (!user) {
            setTimeout(() => {
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'Login' })],
                })
                navigation.dispatch(resetAction);
            }, 3500)
            return
        }
       
        setTimeout(() => {
            const { businessId } = this.props 
            console.log('thisprops', this.props)
            if (businessId) {
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'RestaurantMain' })],
                })
                navigation.dispatch(resetAction);
            } else {
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'App' })],
                })
                navigation.dispatch(resetAction);
            }
        }, 1500)
    }

    render() {
        return <ImageBackground
            source={require('../assets/bg_001.png')}
            style={{ width: '100%', height: '100%', }}
            resizeMode='cover'
            >
            <View  style={styles.container}>
                {/* <Image
                    resizeMode='stretch'
                    style={{
                        backgroundColor: 'transparent',
                        width:250,
                        height:250,
                        justifyContent: 'center',
                        alignContent: 'center',
                    }}
                    source={require('../assets/logo.png')}/> */}
            </View>
        </ImageBackground>
        // </SafeAreaView>
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
})

const mapStateToProps = state => ({
    token: state.accounts.token,
    businessId: state.business.businessId
})

const bindActions = dispatch => ({
    logOutUser: () => dispatch(logOutUser())
})

export default connect(mapStateToProps, bindActions)(SplashScreen)
