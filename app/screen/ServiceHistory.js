import React from 'react'
import {
  Image,
  StyleSheet,
  Dimensions,
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Platform,
  Alert
} from 'react-native'
import { connect } from 'react-redux'
import Icon from 'react-native-vector-icons/MaterialIcons'
import ModalServiceDetail from '../components/ModalServiceDetail'
import Button from '../components/Button'
import moment from 'moment'
import { getOrdersHistory } from '../parse/parse'
import { lightpurple, pinkish } from '../utils/colors'

const { width } = Dimensions.get('window')

class ServiceHistory extends React.Component {

    state = {
        modalVisible: false,
        selectedService: {},
        orders: []
    }
  
    async componentDidMount() {
        const response = await getOrdersHistory()
        this.setState({ orders: response.data })
    }

    goBack = () => {
        const { navigation } =this.props
        navigation.goBack()
    }
    
    handleShowModal = (service) => {
        this.setState({ modalVisible: true, selectedService: service })
    }
    handleCloseModal = () => {
        this.setState({ modalVisible: false, selectedService: {} })
    }
   
    render() {
        const { modalVisible, selectedService, orders } = this.state
        return (
            <SafeAreaView style={styles.container} >
                <View style={styles.header}>
                    <TouchableOpacity
                        onPress={this.goBack}>
                        <Icon
                            name='chevron-left'
                            color={lightpurple}
                            size={50}
                        />
                    </TouchableOpacity>
                    <Text style={styles.storeTitle}>Historial de servicios</Text>
                </View>
                <View style={styles.body} >
                    <View style={styles.listContainer}>
                        <FlatList
                        data={orders}
                        showsVerticalScrollIndicator={true}
                        keyExtractor={item => item.id}
                        renderItem={({ item }) => {
                            return <Button onPress={()=>{this.handleShowModal(item)}} style={styles.cartItem}>
                                <View style={styles.itemTextContainer}> 
                                    <Text style={styles.itemName}>{`Pedido ${moment(item.date).format('DD/MM/YYYY')}`}</Text>
                                </View>
                                <View style={styles.itemTextContainer}> 
                                    <Text style={styles.itemValue}>{`$${item.total}`}</Text>
                                </View>
                            </Button>
                        }}/>
                    </View>
                </View>
                {
                    modalVisible
                    ? <ModalServiceDetail
                        modalVisible={modalVisible}
                        onRequestClose={this.handleCloseModal}
                        service={selectedService}
                    /> : null
                }
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: 'white',
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderColor: 'grey',
        marginBottom: 10
    },
    buttonHeaderContianer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonHeader: {
        height: 35,
        width: 35,
        borderRadius: 35 / 2,
        elevation: 5,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white'
    },
    title: {
        fontSize: 18,
        marginTop:10,
        marginLeft: 10
    },
    body:{
        backgroundColor:'transparent',
        flex:1
    },
    listContainer:{
        flex:1,
        marginTop:20
    },
    cartItem:{
        height:50,
        backgroundColor:'gainsboro',
        // borderBottomWidth: StyleSheet.hairlineWidth,
        flexDirection:'row',
        paddingVertical: 5,
        elevation:1,
        // borderRadius:3,
        // borderWidth:0.5,
        marginBottom:1,
        paddingHorizontal:10,
        justifyContent: 'space-between'

    },
    itemName:{
        color:pinkish,
        alignSelf:'flex-start',
        fontWeight:'bold'
    },
    itemValue:{
        alignSelf:'flex-end',
    },
    quantityContainer:{
        justifyContent:'center',
        width: width/3.1
    },
    itemTextContainer:{
        justifyContent:'center'
    },
    
});
  
const mapStateToProps = state => ({
})

const bindActions = dispatch => ({
})

export default connect(mapStateToProps, bindActions)(ServiceHistory)