import React from 'react'
import { View, Image, StyleSheet, Text, Dimensions,ImageBackground } from 'react-native'
import { lightpurple } from '../utils/colors'
const { height } = Dimensions.get('window')

class SuccessfulOrder extends React.Component {
    static navigationOptions = {
        header: null
    }

    componentDidMount() {
        const { navigation } = this.props
        setTimeout(() => {
            navigation.navigate('OrderTracking')
        }, 3000)
    }

    render() {
        return <View style={{ width: '100%', height: '100%', backgroundColor:lightpurple}}>
            <View  style={styles.container}>
                <ImageBackground
                    resizeMode='center'
                    style={{
                        backgroundColor: 'transparent',
                        flex:1,
                        justifyContent: 'flex-end',
                        alignContent: 'center',
                    }}
                    source={require('../assets/bg_03.png')}
                >
                    <Text style={{fontSize:40, color:'white', alignSelf:'center'}}>¡Pedido exitoso!</Text>

                </ImageBackground>
                
                <View style={{padding:60, justifyContent:'space-between', height:height/2.8, backgroundColor:lightpurple}}> 
                    <Text style={{fontSize:20, color:'white'}}>Su pedido ha sido realizado exitosamente.</Text>
                    <Text style={{fontSize:20, color:'white'}}>Sigue el estado de tu pedido en la siguiente pantalla.</Text>
                </View>
            </View>
        </View>
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center',
    }
})

export default SuccessfulOrder
