import React, {Component} from 'react'
/** store */
import {store, persistor} from './app/stores/index';
import {Provider} from 'react-redux';
import Routes from './app/routers';
import initializeStore from './app/stores';
import {PersistGate} from 'redux-persist/integration/react'
import {View, Text, AsyncStorage, StyleSheet} from 'react-native';
import {createStackNavigator, createAppContainer} from 'react-navigation'
import OneSignal from 'react-native-onesignal'
import { getCurrentUser } from "./app/parse/parse";

class App extends React.Component {

  async componentDidMount () {
    
    
      OneSignal.setAppId("a0e6c3d9-a444-41f3-ba4a-fd96ac916f69");
      OneSignal.setLogLevel(6, 0);
      OneSignal.setRequiresUserPrivacyConsent(false);
      OneSignal.promptForPushNotificationsWithUserResponse(response => {
        console.log("Prompt response:", response);
      });

      /* O N E S I G N A L  H A N D L E R S */
      OneSignal.setNotificationWillShowInForegroundHandler(notifReceivedEvent => {
          console.log("OneSignal: notification will show in foreground:", notifReceivedEvent);
          // let notif = notifReceivedEvent.getNotification();

          // const button1 = {
          //     text: "Cancel",
          //     onPress: () => { notifReceivedEvent.complete(); },
          //     style: "cancel"
          // };

          // const button2 = { text: "Complete", onPress: () => { notifReceivedEvent.complete(notif); }};

          // Alert.alert("Complete notification?", "Test", [ button1, button2], { cancelable: true });
      });
      OneSignal.setNotificationOpenedHandler(notification => {
         console.log("OneSignal: notification opened:", notification);
      });
      OneSignal.setInAppMessageClickHandler(event => {
        console.log("OneSignal IAM clicked:", event);
      });
      OneSignal.addEmailSubscriptionObserver((event) => {
        console.log("OneSignal: email subscription changed: ", event);
      });
      OneSignal.addSubscriptionObserver(event => {
        console.log("OneSignal: subscription changed:", event);
          this.setState({ isSubscribed: event.to.isSubscribed})
      });
      OneSignal.addPermissionObserver(event => {
        console.log("OneSignal: permission changed:", event);
      });

      const deviceState = await OneSignal.getDeviceState();

      this.setState({
          isSubscribed : deviceState.isSubscribed
      });
      const currentUser = await getCurrentUser()
      if (currentUser) {
        const email = currentUser.get('email') ? currentUser.get('email') : currentUser.get('username')
       OneSignal.sendTag('email', email)
     }
  }




  render() {
    return <Provider store={store}>
      <PersistGate persistor={persistor}>
        <View style={styles.container}>
          <Routes/>
        </View>
      </PersistGate>
    </Provider>
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  }
})

export default App; 
